<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('product_images', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('product_id');
        $table->enum('type', ['other', 'main']);
        $table->string('url');
        $table->string('resize_small_url');
        $table->longText('description');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('product_images');
    }
}
