<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sellers', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name');
        $table->string('company');
        $table->longText('address');
        $table->string('phone', 30);
        $table->longText('description');
        $table->string('company_type');
        $table->string('year_established', 4);
        $table->integer('level');
        $table->integer('deleted');
        $table->remembertoken();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('sellers');
    }
}
