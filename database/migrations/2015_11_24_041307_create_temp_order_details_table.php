<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('temp_order_details', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('product_id');
        $table->bigInteger('buyer_id');
        $table->integer('quantity');
        $table->double('sub_total', 15, 2);
        $table->double('sub_weight', 15, 2);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('temp_order_details');
    }
}
