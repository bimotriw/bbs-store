<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('orders', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('code', 12);
        $table->bigInteger('buyer_id');
        $table->enum('status', ['0', '1', '2', '3']);
        $table->double('shipping_cost', 15, 2);
        $table->double('total', 15, 2);
        $table->enum('notification_status', ['0', '1']);
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('orders');
    }
}
