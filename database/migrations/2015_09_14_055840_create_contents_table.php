<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('contents', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('title');
        $table->longText('content');
        $table->enum('type', ['slider', 'promo', 'banner']);
        $table->string('url');
        $table->string('resize_small_url');
        $table->remembertoken();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('contents');
    }
}
