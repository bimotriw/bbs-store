<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('wishlists', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->integer('total');
        $table->bigInteger('product_id');
        $table->bigInteger('wishlistable_id');
        $table->string('wishlistable_type');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('wishlists');
    }
}
