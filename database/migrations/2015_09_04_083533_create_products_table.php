<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('products', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('seller_id');
        $table->bigInteger('category_id');
        $table->string('code')->unique();
        $table->string('name');
        $table->double('price', 15, 2);
        $table->double('weight', 6, 2);
        $table->longText('description');
        $table->longText('specification');
        $table->string('additional_document');
        $table->string('additional_document_resize');
        $table->enum('availability', ['Y', 'N']);
        $table->integer('rating');
        $table->integer('rater');
        $table->integer('sold');
        $table->integer('deleted');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('products');
    }
}
