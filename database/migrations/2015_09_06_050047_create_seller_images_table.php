<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('seller_images', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('seller_id');
        $table->enum('type', ['other', 'main']);
        $table->string('url');
        $table->string('resize_small_url');
        $table->longText('description');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('seller_images');
    }
}
