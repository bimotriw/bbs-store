<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('order_details', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('product_id');
        $table->bigInteger('orderdetailable_id');
        $table->string('orderdetailable_type');
        $table->integer('quantity');
        $table->double('sub_total', 15, 2);
        $table->double('sub_weight', 15, 2);
        $table->enum('status', ['0', '1', '2', '3', '4', '5']);
        $table->enum('notification_status', ['0', '1']);
        $table->enum('notification_status_seller', ['0', '1']);
        $table->string('resi');
        $table->string('url');
        $table->string('resize_small_url');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('order_details');
    }
}
