<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSellerAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('seller_accounts', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('seller_id');
        $table->enum('account_type', ['BCA', 'BRI', 'Mandiri']);
        $table->string('account_number');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('seller_accounts');
    }
}
