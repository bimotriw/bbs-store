<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('buyers', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name');
        $table->string('company');
        $table->longText('address');
        $table->string('phone', 30);
        $table->remembertoken();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('buyers');
    }
}
