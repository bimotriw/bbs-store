<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('complaints', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->bigInteger('buyer_id');
        $table->string('code', 12);
        $table->longText('comment');
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('complaints');
    }
}
