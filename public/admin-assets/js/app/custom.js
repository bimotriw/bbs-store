



/********************************
Preloader
********************************/
$(window).load(function() {
  $('.loading-container').fadeOut(1000, function() {
	$(this).remove();
  });
});






$(function(){


	/*$('.dropdown-menu').click(function(event){
	  event.stopPropagation();
	});*/




	/********************************
	Toggle Aside Menu
	********************************/

	$(document).on('click', '.navbar-toggle', function(){

		$('aside.left-panel').toggleClass('collapsed');

	});





	/********************************
	Aside Navigation Menu
	********************************/

	$("aside.left-panel nav.navigation > ul > li:has(ul) > a").click(function(){

		if( $("aside.left-panel").hasClass('collapsed') == false || $(window).width() < 768 ){



		$("aside.left-panel nav.navigation > ul > li > ul").slideUp(300);
		$("aside.left-panel nav.navigation > ul > li").removeClass('active');

		if(!$(this).next().is(":visible"))
		{

			$(this).next().slideToggle(300,function(){ $("aside.left-panel:not(.collapsed)").getNiceScroll().resize(); });
			$(this).closest('li').addClass('active');
		}

		return false;

		}

	});



	/********************************
	popover
	********************************/
	if( $.isFunction($.fn.popover) ){
	$('.popover-btn').popover();
	}



	/********************************
	tooltip
	********************************/
	if( $.isFunction($.fn.tooltip) ){
	$('.tooltip-btn').tooltip()
	}



	/********************************
	NanoScroll - fancy scroll bar
	********************************/
	if( $.isFunction($.fn.niceScroll) ){
	$(".nicescroll").niceScroll({

		cursorcolor: '#9d9ea5',
		cursorborderradius : '0px'

	});
	}


	if( $.isFunction($.fn.niceScroll) ){
	$("aside.left-panel:not(.collapsed)").niceScroll({
		cursorcolor: '#8e909a',
		cursorborder: '0px solid #fff',
		cursoropacitymax: '0.5',
		cursorborderradius : '0px'
	});
	}





	/********************************
	Input Mask
	********************************/
	if( $.isFunction($.fn.inputmask) ){
		$(".inputmask").inputmask();
	}





	/********************************
	TagsInput
	********************************/
	if( $.isFunction($.fn.tagsinput) ){
		$('.tagsinput').tagsinput();
	}





	/********************************
	Chosen Select
	********************************/
	if( $.isFunction($.fn.chosen) ){
		$('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
	}




	/********************************
	DateTime Picker
	********************************/
	if( $.isFunction($.fn.datetimepicker) ){
		$('#datetimepicker').datetimepicker();
		$('#datepicker').datetimepicker({pickTime: false});
		$('#timepicker').datetimepicker({pickDate: false});

		$('#datetimerangepicker1').datetimepicker();
		$('#datetimerangepicker2').datetimepicker();
		$("#datetimerangepicker1").on("dp.change",function (e) {
		   $('#datetimerangepicker2').data("DateTimePicker").setMinDate(e.date);
		});
		$("#datetimerangepicker2").on("dp.change",function (e) {
		   $('#datetimerangepicker1').data("DateTimePicker").setMaxDate(e.date);
		});
	}


	/********************************
	wysihtml5
	********************************/
	if( $.isFunction($.fn.wysihtml5) ){
		$('.wysihtml').wysihtml5();
	}



	/********************************
	wysihtml5
	********************************/
	if( $.isFunction($.fn.ckeditor) ){
	CKEDITOR.disableAutoInline = true;
	$('#ckeditor').ckeditor();
	$('.inlineckeditor').ckeditor();
	}









	/********************************
	Scroll To Top
	********************************/
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});




});








/********************************
Toggle Full Screen
********************************/

function toggleFullScreen() {
	if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
		if (document.documentElement.requestFullScreen) {
			document.documentElement.requestFullScreen();
		} else if (document.documentElement.mozRequestFullScreen) {
			document.documentElement.mozRequestFullScreen();
		} else if (document.documentElement.webkitRequestFullScreen) {
			document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
		}
	} else {
		if (document.cancelFullScreen) {
			document.cancelFullScreen();
		} else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	}
}


$(document).on("click", ".btnModalSellerImage", function () {
   $("#fileText").empty();
   var id_seller = $(this).data('par');
   var id_image = $(this).data('id');
   var type_image = $(this).data('type');
   var url_image = $(this).data("url");
   var description_image = $(this).data("description");
   if(type_image == 'main') {
    var value = "<button class='btn btn-purple btn-sm'>main</button>";
   } else {
    var value = "<button class='btn btn-success btn-sm'>other</button>";
   }
   document.getElementById("title_edit").innerHTML = "Edit Image";
   document.getElementById("type_edit").innerHTML = value;
   $("#id_edit").val( id_image );
   var res = url_image.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
   var temp = "uploadsseller_images" + id_seller;
   var y = res.replace(temp, "");
   document.getElementById("fileText").innerHTML = y;
   $("#description_edit").val( description_image );
});

$(document).on("click", ".btnModalSellerImageNew", function () {
   document.getElementById("title_edit1").innerHTML = "Add Image";
   $("#fileText1").empty();
   $("#file1").val("");
   $("#description_edit1").val("");
});

$(document).on("click", ".btnModalSellerImageDelete", function () {
   var id_image = $(this).data('id');
   document.getElementById("modal-footer").innerHTML = "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> <a href='deleteprofileimage/"+id_image+"'><button type='button' class='btn btn-danger'>Delete</button></a>";
});

$(document).on("click", ".btnModalProductDelete", function () {
   var id = $(this).data('id');
   document.getElementById("modal-footer").innerHTML = "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> <a href='deleteproduct/"+id+"'><button type='button' class='btn btn-danger'>Delete</button></a>";
});

$(document).on("click", ".btnModalProductImage", function () {
   $("#fileText").empty();
   var id_seller = $(this).data('par');
   var id_image = $(this).data('id');
   var type_image = $(this).data('type');
   var url_image = $(this).data("url");
   var description_image = $(this).data("description");
   if(type_image == 'main') {
    var value = "<button class='btn btn-purple btn-sm'>main</button>";
   } else {
    var value = "<button class='btn btn-success btn-sm'>other</button>";
   }
   document.getElementById("title_edit").innerHTML = "Edit Image";
   document.getElementById("type_edit").innerHTML = value;
   $("#id_edit").val( id_image );
   var res = url_image.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
   var temp = "uploadsproduct_images" + id_seller;
   var y = res.replace(temp, "");
   document.getElementById("fileText").innerHTML = y;
   $("#description_edit").val( description_image );
});

$(document).on("click", ".btnModalProductImageNew", function () {
   document.getElementById("title_edit1").innerHTML = "Add Image";
   var id_prod = $(this).data('id_prod');
   $("#id_prod_edit").val( id_prod );
   $("#fileText1").empty();
   $("#file1").val("");
   $("#description_edit1").val("");
});

$(document).on("click", ".btnModalProductImageDelete", function () {
   var id_image = $(this).data('id');
   var id_prod = $(this).data('id_prod');
   document.getElementById("modal-footer").innerHTML = "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> <a href='deleteproductimage/"+id_prod+"/"+id_image+"'><button type='button' class='btn btn-danger'>Delete</button></a>";
});

$(document).on("click", ".confirmChangePassword", function () {
   document.getElementById("change_password").value = "";
   document.getElementById("change_password").value = document.getElementById("password").value;
});

$(document).on("click", ".btnModalSellerDeactive", function () {
   var id = $(this).data('id');
   document.getElementById("modal-footer-block").innerHTML = "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> <a href='inactiveseller/"+id+"'><button type='button' class='btn btn-danger'>Block</button></a>";
});

$(document).on("click", ".btnModalSellerActive", function () {
   var id = $(this).data('id');
   document.getElementById("modal-footer-unblock").innerHTML = "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> <a href='activeseller/"+id+"'><button type='button' class='btn btn-warning'>Unblock</button></a>";
});

$(document).on("click", ".btnModalCategoryDelete", function () {
   var id = $(this).data('id');
   document.getElementById("modal-footer").innerHTML = "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button> <a href='categories/delete/"+id+"'><button type='button' class='btn btn-danger'>Delete</button></a>";
});


function myFunctionVerificationResi() {
  var resi = document.getElementById("verification_resi").value;
  var resi1 = resi.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
  var resi2 = resi1.replace("Cfakepath", "");
  document.getElementById("verification_resiText").innerHTML = "<i class='fa fa-arrow-right'></i> " + resi2;
}
function myFunctionProductFile() {
  var x2 = document.getElementById("file2").value;
  var res2 = x2.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
  var y2 = res2.replace("Cfakepath", "");
  document.getElementById("fileText2").innerHTML = "<i class='fa fa-arrow-right'></i> " + y2;
}
function myFunction() {
  var x = document.getElementById("file").value;
  var x1 = document.getElementById("file1").value;
  var res = x.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
  var res1 = x1.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
  var y = res.replace("Cfakepath", "");
  var y1 = res1.replace("Cfakepath", "");
  document.getElementById("fileText").innerHTML = y;
  document.getElementById("fileText1").innerHTML = "<i class='fa fa-arrow-right'></i> " + y1;
}
function myFunctionImage() {
  var x = document.getElementById("image").value;
  var res = x.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
  var y = res.replace("Cfakepath", "");
  document.getElementById("fileTextImage").innerHTML = y;
}
function myFunctionProductFile1() {
  var x3 = document.getElementById("prod_file").value;
  var res3 = x3.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
  var y3 = res3.replace("Cfakepath", "");
  document.getElementById("prod_fileText").innerHTML = "<i class='fa fa-arrow-right'></i> " + y3;
}
function myFunctionMainImage() {
  var x4 = document.getElementById("main_image").value;
  var res4 = x4.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
  var y4 = res4.replace("Cfakepath", "");
  document.getElementById("main_imageText").innerHTML = "<i class='fa fa-arrow-right'></i> " + y4;
}


function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'Invoice', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Invoice</title>');
        mywindow.document.write('<link rel="stylesheet" href="http://localhost/bbs/public/admin-assets/css/plugins/datatables/jquery.dataTables.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="http://localhost/bbs/public/admin-assets/css/bootstrap/bootstrap.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="http://localhost/bbs/public/admin-assets/css/app/app.v1.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
