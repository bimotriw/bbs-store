<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Buyer extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
  use Authenticatable, Authorizable, CanResetPassword;

  protected $fillable = ['name','email','company','address','phone'];

  public function wishlists()
  {
    return $this->morphMany('App\Wishlist', 'wishlistable');
  }

  public function user()
  {
    return $this->hasOne('App\User');
  }

  public function orders()
  {
    return $this->hasMany('App\Order');
  }

  public function temp_order_details()
  {
    return $this->hasMany('App\TempOrderDetail');
  }
}
