<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
  public function buyer()
  {
    return $this->belongsTo('App\Buyer');
  }
}
