<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  protected $fillable = ['seller_id','category_id','name','price','description','specification','additional_document','deleted'];

  public function wishlists()
  {
    return $this->morphMany('App\Wishlist', 'wishlistable');
  }

  public function seller()
  {
    return $this->belongsTo('App\Seller');
  }

  public function category()
  {
    return $this->belongsTo('App\Category');
  }

  public function product_images()
  {
    return $this->hasMany('App\ProductImage');
  }

  public function order_detailOrders()
  {
    return $this->hasMany('App\OrderDetail');
  }

  public function order_details()
  {
    return $this->morphMany('App\OrderDetail', 'orderdetailable');
  }
}
