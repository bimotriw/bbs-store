<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use Hash;
use Validator;
use App\Category;
use App\User;
use App\Buyer;

use App\Http\Requests;
use App\Http\Requests\AuthRequest;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function store(AuthRequest $request)
    {
      if(Auth::attempt(array('email' => $request->input('email'), 'password' => $request->input('password'))))
      {
        if(Auth::user()->role == 0)
          return Redirect::to('/adminbbs');
        else if(Auth::user()->role == 2)
          return Redirect::to('/sellerbbs');
      }
      return Redirect::to('/');
    }

    public function logout()
    {
      Auth::logout();
      return Redirect::to('/');
    }

    public function createAccount()
    {
      $categories = Category::all();
      return view('createNew', compact('categories'));
    }

    public function storeAccount(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'email' => 'unique:users',
      ]);
      if ($validator->fails()) {
        $request->session()->flash('warning', 'The email has already been used');
        return redirect('create/account')
          ->withErrors($validator)
          ->withInput();
      }
      $user = new User;
      $buyer = new Buyer;
      $user->email = $request->input('email');
      $user->password = Hash::make($request->input('password'));
      $user->role = 1;
      $user->seller_id = 0;
      $buyer->name = $request->input('full_name');
      $buyer->company = $request->input('company');
      $buyer->address = $request->input('address');
      $buyer->city_id = $request->input('city');
      $buyer->phone = $request->input('phone');
      $buyer->save();
      $user->buyer_id = $buyer->id;
      if ($user->save()) {
        $request->session()->flash('success', 'Account Has Been Created');
        return Redirect::to('/');
      } else {
        $request->session()->flash('denger', 'Failed. Try Again');
        return Redirect::to('create/account');
      }
    }
}
