<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Category;
use App\Product;
use App\TempOrderDetail;
use App\OrderDetail;
use App\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CheckOutController extends Controller
{
  public function index()
  {
    $categories = Category::all();
    $orderReal = Order::all();
    return view('checkout', compact('categories', 'orderReal'));
  }

  public function actionUpdateQuantity(Request $request)
  {
    $temp_order_detail = TempOrderDetail::find($request->input('id_temp'));
    $product = Product::find($temp_order_detail->product_id);
    $temp_order_detail->quantity = $request->input('quantity');
    $temp_order_detail->sub_total = $product->price * $request->input('quantity');
    $temp_order_detail->sub_weight = $product->weight * $request->input('quantity');
    $temp_order_detail->save();
    return Redirect::to('checkout');
  }

  public function actionDelete($id)
  {
    $temp_order_detail = TempOrderDetail::find($id);
    $temp_order_detail->delete();
    return Redirect::to('checkout');
  }

  public function actionStore(Request $request)
  {
    $order = new Order;
    $order->buyer_id = Auth::user()->buyer->id;
    $order->status = 2;
    $order->shipping_cost = $request->input('shipment_input');
    $order->total = $request->input('total_input');
    $order->notification_status = 0;
    $order->save();
    $order->code = date("dmY")."00".$order->id;
    $order->save();

    foreach (Auth::user()->buyer->temp_order_details as $temp_order_detail) {
      if ($temp_order_detail->product->deleted != 1) {
        $order_detail = new OrderDetail;
        $order_detail->product_id = $temp_order_detail->product_id;
        $order_detail->orderdetailable_id = $order->id;
        $order_detail->orderdetailable_type = "App\Order";
        $order_detail->quantity = $temp_order_detail->quantity;
        $order_detail->sub_total = $temp_order_detail->sub_total;
        $order_detail->sub_weight = $temp_order_detail->sub_weight;
        $order_detail->status = 3;
        $order_detail->notification_status = 0;
        $order_detail->notification_status_seller = 0;
        $order_detail->save();
        $temp_order_detail->delete();
      }
    }
    return Redirect::to('order');
  }
}
