<?php

namespace App\Http\Controllers\Adm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Product;
use App\Category;
use App\Order;
use App\OrderDetail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductAdmController extends Controller
{
  public function actionList()
  {
    $products = Product::all();
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/product', compact('products', 'orders', 'order_details'));
  }

  public function actionInactive(Request $request, $id)
  {
    $product = Product::find($id);
    $product->deleted = 1;
    if ($product->save())
      $request->session()->flash('success', 'Inactive Product Was Successfull!');
    else
      $request->session()->flash('error', 'Inactive Product Was Not Successfull!');
    return Redirect::to('adminbbs/catalogs/products');
  }

  public function actionActive(Request $request, $id)
  {
    $product = Product::find($id);
    $product->deleted = 0;
    if ($product->save())
      $request->session()->flash('success', 'Active Product Was Successfull!');
    else
      $request->session()->flash('error', 'Active Product Was Not Successfull!');
    return Redirect::to('adminbbs/catalogs/products');
  }
}
