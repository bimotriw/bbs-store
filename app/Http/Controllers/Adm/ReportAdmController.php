<?php

namespace App\Http\Controllers\Adm;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportAdmController extends Controller
{
  public function listOrder()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/report', compact('orders', 'order_details'));
  }
}
