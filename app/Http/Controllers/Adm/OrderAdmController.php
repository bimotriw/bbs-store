<?php

namespace App\Http\Controllers\Adm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Hash;
use Response;
use Validator;
use App\Order;
use App\OrderDetail;
use App\Buyer;
use App\Seller;
use App\Product;
use App\Complaint;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderAdmController extends Controller
{
  public function actionList()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/order', compact('orders', 'order_details'));
  }

  public function complaints()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    $complaints = Complaint::all();
    return view('adm/complaint', compact('complaints', 'orders', 'order_details'));
  }

  public function actionView($id)
  {
    $order = Order::find($id);
    if($order->status==0) {
      $order->notification_status = 1;
      $order->save();
    }
    foreach($order->order_details as $detail) {
      if($detail->status==0) {
        $detail->notification_status = 1;
        $detail->save();
      }
    }
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/orderView', compact('orders', 'order', 'order_details'));
  }

  public function actionSuccess(Request $request, $id)
  {
    $order = Order::find($id);
    $order->status = 1;
    foreach($order->order_details as $detail) {
      $detail->status = 2;
      if ($detail->save())
        $request->session()->flash('success', 'Purchase Invoice Has Been Paid!');
      else
        $request->session()->flash('error', 'Purchase Invoice Has Not Been Paid!');
    }
    if ($order->save())
      $request->session()->flash('success', 'Purchase Invoice Had Been Paid!');
    else
      $request->session()->flash('error', 'Purchase Invoice Had Not Been Paid!');
    return Redirect::to('adminbbs/orders/');
  }

  public function actionFail(Request $request, $id)
  {
    $order = Order::find($id);
    $order->status = 3;
    foreach($order->order_details as $detail) {
      $detail->status = 5;
      if ($detail->save())
        $request->session()->flash('success', 'Purchase Invoice Has Been Fail!');
      else
        $request->session()->flash('error', 'Purchase Invoice Has Not Been Fail!');
    }
    if ($order->save())
      $request->session()->flash('success', 'Purchase Invoice Has Been Fail!');
    else
      $request->session()->flash('error', 'Purchase Invoice Has Not Been Fail!');
    return Redirect::to('adminbbs/orders/');
  }

  public function actionDetailSuccess(Request $request, $id, $id_seller)
  {
    $order = Order::find($id);
    foreach($order->order_details as $detail) {
      if($detail->product->seller->id == $id_seller) {
        $detail->status = 1;
        if ($detail->save())
          $request->session()->flash('success', 'Seller Invoice Has Been Success!');
        else
          $request->session()->flash('error', 'Seller Invoice Has Not Been Success!');
      }
    }
    return Redirect::to('adminbbs/orders/view/'.$id);
  }

  public function actionDetailFail(Request $request, $id, $id_seller)
  {
    $order = Order::find($id);
    foreach($order->order_details as $detail) {
      if($detail->product->seller->id == $id_seller) {
        $detail->status = 4;
        if ($detail->save())
          $request->session()->flash('success', 'Seller Invoice Has Been Fail!');
        else
          $request->session()->flash('error', 'Seller Invoice Has Not Been Fail!');
      }
    }
    return Redirect::to('adminbbs/orders/view/'.$id);
  }

  public function actionListResponded(Request $request)
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/orderStatus', compact('orders', 'order_details'));
  }

  public function actionListPaid(Request $request)
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/orderStatus', compact('orders', 'order_details'));
  }

  public function actionListInvalid(Request $request)
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/orderStatus', compact('orders', 'order_details'));
  }
}
