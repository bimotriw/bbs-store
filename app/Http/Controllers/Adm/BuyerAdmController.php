<?php

namespace App\Http\Controllers\Adm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Hash;
use Response;
use Validator;
use App\User;
use App\Buyer;
use App\Wishlist;
use App\Product;
use App\Order;
use App\OrderDetail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BuyerAdmController extends Controller
{
  public function actionList()
  {
    $buyers = Buyer::all();
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/buyer', compact('buyers', 'orders', 'order_details'));
  }

  public function actionNew()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/buyerNew', compact('orders', 'order_details'));
  }

  public function actionStore(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email' => 'unique:users',
    ]);
    if ($validator->fails()) {
      $request->session()->flash('warning', 'The email has already been used');
      return redirect('adminbbs/buyers/new')
        ->withErrors($validator)
        ->withInput();
    }
    $buyer = new Buyer;
		$buyer->name = $request->input('name');
    $buyer->company = $request->input('company');
    $buyer->address = $request->input('address');
    $buyer->phone = $request->input('phone');
    if ($buyer->save())
    {
      $user = new User;
      $user->email = $request->input('email');
      $user->password = Hash::make($request->input('password'));
      $user->role = 1;
      $user->buyer_id = $buyer->id;
      if ($user->save())
        $request->session()->flash('success', 'Add Buyer Was Successfull!');
      else
        $request->session()->flash('error', 'Add Buyer1 Was Not Successfull!');
    }
    else
      $request->session()->flash('error', 'Add Buyer2 Was Not Successfull!');
    return Redirect::to('adminbbs/buyers');
  }

  public function actionView($id)
  {
    $buyer = Buyer::find($id);
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/buyerView', compact('buyer', 'orders', 'order_details'));
  }

  public function actionUpdate(Request $request)
  {
    $id = $request->input('id');
    $buyer = Buyer::find($id);
    $buyer->user->password = Hash::make($request->input('password'));
    if ($buyer->user->save())
      $request->session()->flash('success', 'Update Password Was Successfull!');
    else
      $request->session()->flash('error', 'Update Password Was Not Successfull!');
    return Redirect::to('adminbbs/buyers');
  }
}
