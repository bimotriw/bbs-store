<?php

namespace App\Http\Controllers\Adm;

use Illuminate\Http\Request;
use App\Order;
use App\OrderDetail;
use App\Buyer;
use App\Seller;
use App\Product;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeAdmController extends Controller
{
  public function index()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    $buyers = Buyer::all();
    $sellers = Seller::all();
    $products = Product::all();
    return view('adm/home', compact('orders', 'order_details', 'buyers', 'sellers', 'products'));
  }

  public function help()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/help', compact('orders', 'order_details'));
  }

  public function backup()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/backup');
  }
}
