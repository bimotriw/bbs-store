<?php

namespace App\Http\Controllers\Adm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Hash;
use Validator;
use App\User;
use App\Seller;
use App\Wishlist;
use App\Product;
use App\ProductImage;
use App\SellerImage;
use App\SellerAccount;
use App\Order;
use App\OrderDetail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SellerAdmController extends Controller
{
  public function actionList()
  {
    $sellers = Seller::all();
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/seller', compact('sellers', 'orders', 'order_details'));
  }

  public function actionNew()
  {
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/sellerNew', compact('orders', 'order_details'));
  }

  public function actionStore(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email' => 'unique:users',
    ]);
    if ($validator->fails()) {
      $request->session()->flash('warning', 'The email has already been used');
      return redirect('adminbbs/seller/new')
        ->withErrors($validator)
        ->withInput();
    }
    $seller = new Seller;
		$seller->name = $request->input('name');
    $seller->company = $request->input('company_sel');
    $seller->address = $request->input('address');
    $seller->phone = $request->input('phone');
    $seller->description = $request->input('description');
    $seller->company_type = $request->input('company_type');
    $seller->year_established = $request->input('year_established');
    if ($seller->save())
    {
      $user = new User;
      $user->email = $request->input('email');
      $user->password = Hash::make($request->input('password'));
      $user->role = 2;
      $user->seller_id = $seller->id;
      $seller_account = new SellerAccount;
      $seller_account->account_number = $request->input('bca');
      $seller_account->seller_id = $seller->id;
      $seller_account->account_type = "BCA";
      $seller_account->save();
      $seller_account = new SellerAccount;
      $seller_account->account_number = $request->input('bri');
      $seller_account->seller_id = $seller->id;
      $seller_account->account_type = "BRI";
      $seller_account->save();
      $seller_account = new SellerAccount;
      $seller_account->account_number = $request->input('mandiri');
      $seller_account->seller_id = $seller->id;
      $seller_account->account_type = "Mandiri";
      $seller_account->save();
      if ($user->save())
        $request->session()->flash('success', 'Add Seller Was Successfull!');
      else
        $request->session()->flash('error', 'Add Seller Was Not Successfull!');
    }
    else
      $request->session()->flash('error', 'Add Seller Was Not Successfull!');
    return Redirect::to('adminbbs/sellers');
  }

  public function actionView($id)
  {
    $seller = Seller::find($id);
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/sellerView', compact('seller', 'orders', 'order_details'));
  }

  public function actionInactive(Request $request, $id)
  {
    $seller = Seller::find($id);
    $seller->deleted = 1;
    if ($seller->save())
      $request->session()->flash('success', 'Inactive Seller Was Successfull!');
    else
      $request->session()->flash('error', 'Inactive Seller Was Not Successfull!');
    return Redirect::to('adminbbs/sellers/');
  }

  public function actionActive(Request $request, $id)
  {
    $seller = Seller::find($id);
    $seller->deleted = 0;
    if ($seller->save())
      $request->session()->flash('success', 'Active Seller Was Successfull!');
    else
      $request->session()->flash('error', 'Active Seller Was Not Successfull!');
    return Redirect::to('adminbbs/sellers/');
  }

  public function actionUpdate(Request $request)
  {
    $id = $request->input('id');
    $seller = Seller::find($id);
    $seller->user->password = Hash::make($request->input('password'));
    if ($seller->user->save())
      $request->session()->flash('success', 'Update Password Was Successfull!');
    else
      $request->session()->flash('error', 'Update Password Was Not Successfull!');
    return Redirect::to('adminbbs/sellers');
  }

  //Seller->product
  public function actionProductInactive(Request $request, $id, $id_prod)
  {
    $product = Product::find($id_prod);
    $product->deleted = 1;
    if ($product->save())
      $request->session()->flash('success', 'Inactive Product Was Successfull!');
    else
      $request->session()->flash('error', 'Inactive Product Was Not Successfull!');
    return Redirect::to('adminbbs/sellers/view/'.$id);
  }

  public function actionProductActive(Request $request, $id, $id_prod)
  {
    $product = Product::find($id_prod);
    $product->deleted = 0;
    if ($product->save())
      $request->session()->flash('success', 'Active Product Was Successfull!');
    else
      $request->session()->flash('error', 'Active Product Was Not Successfull!');
    return Redirect::to('adminbbs/sellers/view/'.$id);
  }

  public function actionProductView($id, $id_prod)
  {
    $seller = Seller::find($id);
    $product = Product::find($id_prod);
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/sellerViewProduct', compact('seller', 'product', 'orders', 'order_details'));
  }

  public function actionProductSwap(Request $request, $id, $id_prod, $id_img)
  {
    $product_img = ProductImage::find($id_img);
    if($product_img->type == "main")
    {
      $request->session()->flash('warning', 'The Image Has Become Main Image');
      return Redirect::to('adminbbs/sellers/'.$id.'/products/'.$id_prod);
    }
    $affected = DB::update('update product_images set type = "other" where product_id = ?', [$id_prod]);
    $product_img->type = "main";
    if ($product_img->save())
      $request->session()->flash('success', 'Main Image Has Been Changed');
    else
      $request->session()->flash('error', 'Main Image Has Not Been Changed');
    return Redirect::to('adminbbs/sellers/'.$id.'/products/'.$id_prod);
  }

  public function actionProductDelete(Request $request, $id, $id_prod, $id_img)
  {
    $product_img = ProductImage::find($id_img);
    if ($product_img->delete())
      $request->session()->flash('success', 'The Image Has Been Deleted!');
    else
      $request->session()->flash('success', 'The Image Has Not Been Deleted!');
    return Redirect::to('adminbbs/sellers/'.$id.'/products/'.$id_prod);
  }
}
