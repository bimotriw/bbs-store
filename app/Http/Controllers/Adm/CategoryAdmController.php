<?php

namespace App\Http\Controllers\Adm;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;
use App\Category;
use App\Order;
use App\OrderDetail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryAdmController extends Controller
{
  public function actionList()
  {
    $categories = Category::all();
    $orders = Order::all();
    $order_details = OrderDetail::all();
    return view('adm/category', compact('categories', 'orders', 'order_details'));
  }

  public function actionStore(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'unique:categories',
    ]);
    if ($validator->fails()) {
      $request->session()->flash('warning', 'The category has already been used');
      return redirect('adminbbs/catalogs/categories')
        ->withErrors($validator)
        ->withInput();
    }
    $category = new Category;
		$category->name = $request->input('name');
    $category->parent_id = $request->input('parent');
    if ($category->save())
      $request->session()->flash('success', 'Add Category Was Successfull!');
    else
      $request->session()->flash('error', 'Add Category Was Not Successfull!');
    return Redirect::to('adminbbs/catalogs/categories');
  }

  public function actionDelete(Request $request, $id)
  {
    $category = Category::find($id);
    if ($category->delete())
      $request->session()->flash('success', 'The Category Has Been Deleted!');
    else
      $request->session()->flash('success', 'The Category Has Not Been Deleted!');
    return Redirect::to('adminbbs/catalogs/categories');
  }

  public function actionUpdate(Request $request)
  {
    $id = $request->input('id');
    $category = Category::find($id);
    $category->name = $request->input('name');
    if($request->input('check')=="on")
    {
      $category->parent_id = 0;
      if ($category->save())
        $request->session()->flash('success', 'Update Category Was Successfull!');
      else
        $request->session()->flash('error', 'Update Category Was Not Successfull!');
    }
    if($request->input('parent_edit')==0)
    {
      if ($category->save())
        $request->session()->flash('success', 'Update Category Was Successfull!');
      else
        $request->session()->flash('error', 'Update Category Was Not Successfull!');
    }
    else
    {
      $category->parent_id = $request->input('parent_edit');
      if ($category->save())
        $request->session()->flash('success', 'Update Category Was Successfull!');
      else
        $request->session()->flash('error', 'Update Category Was Not Successfull!');
    }
    return Redirect::to('adminbbs/catalogs/categories');
  }
}
