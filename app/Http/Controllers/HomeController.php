<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;
use Hash;
use DB;
use App\Buyer;
use App\Wishlist;
use App\TempOrderDetail;
use App\Product;
use App\Category;
use App\ProductImage;
use App\Content;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
  public function index()
  {
    $contents = Content::all();
    $categories = Category::all();
    $product_images = ProductImage::all();
    //$products = DB::table('products')->orderByRaw('rand()')->take(6)->get();
    $products = Product::all();
    $new_products = Product::all();
    //$new_products = DB::table('products')->orderBy('id', 'desc')->take(10)->get();
    //$best_products = DB::table('products')->orderBy('sold', 'desc')->take(4)->get();
    $promos1 = DB::table('contents')->where('type', 'promo')->orderByRaw('rand()')->take(2)->get();
    $promos2 = DB::table('contents')->where('type', 'promo')->orderByRaw('rand()')->take(1)->get();
    return view('home', compact('contents', 'products', 'product_images', 'categories', 'new_products','promos1', 'promos2'));
  }

  public function actionStoreOrder($id)
  {
    $temp_order_detail = new TempOrderDetail;
    $temp_order_detail->buyer_id = Auth::user()->buyer->id;
    $temp_order_detail->product_id = $id;
    $product = Product::find($id);
    $temp_order_detail->quantity = 1;
    $temp_order_detail->sub_total = $product->price;
    $temp_order_detail->sub_weight = $product->weight;
    $temp_order_detail->save();
    return Redirect::to('/');
  }

  public function actionStoreWishlist($id)
  {
    $product = Product::find($id);
    $wishlist = new Wishlist;
    $wishlist->product_id = $id;
    $wishlist->wishlistable_id = Auth::user()->buyer->id;
    $wishlist->total = 1;
    $wishlist->wishlistable_type = "App\Buyer";
    $wishlist->save();
    return Redirect::to('/');
  }

  public function actionDeleteWishlist($id)
  {
    $wishlist = Wishlist::find($id);
    $wishlist->delete();
    return Redirect::to('/');
  }

  public function accountView()
  {
    if (Auth::check()) {
      $categories = Category::all();
      return view('account', compact('categories'));
    } else {
      return Redirect::to('/');
    }
  }

  public function editAccount(Request $request)
  {
    Auth::user()->buyer->name = $request->input('full_name');
    Auth::user()->buyer->company = $request->input('company');
    Auth::user()->buyer->address = $request->input('address');
    Auth::user()->buyer->city_id = $request->input('city');
    Auth::user()->buyer->phone = $request->input('phone');
    if ($request->input('phone') != "") {
        Auth::user()->password = Hash::make($request->input('password'));
        if (Auth::user()->buyer->save())
          $request->session()->flash('success', 'Update Account Was Successfull!');
        else
          $request->session()->flash('error', 'Update Account Was Not Successfull!');
        return Redirect::to('account');
    } else {
      if (Auth::user()->buyer->save())
        $request->session()->flash('success', 'Update Account Was Successfull!');
      else
        $request->session()->flash('error', 'Update Account Was Not Successfull!');
      return Redirect::to('account');
    }

  }
}
