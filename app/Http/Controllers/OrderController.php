<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Category;
use App\Order;
use App\Complaint;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
  public function index()
  {
    $categories = Category::all();
    return view('order', compact('categories'));
  }

  public function actionView($id)
  {
    $categories = Category::all();
    $order = Order::find($id);
    return view('orderView', compact('order', 'categories'));
  }

  public function actionVerification($id)
  {
    $order = Order::find($id);
    $order->status = 0;
    $order->save();
    return Redirect::to('order');
  }

  public function actionComplaint(Request $request)
  {
    $complaint = new Complaint;
    $complaint->buyer_id = Auth::user()->buyer->id;
    $complaint->code = $request->input('order_number');
    $complaint->comment = $request->input('comment');
    $complaint->save();
    return Redirect::to('/');
  }

}
