<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Category;
use App\Product;
use App\Wishlist;
use App\TempOrderDetail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
  public function actionView($id)
  {
    $categories = Category::all();
    $product = Product::find($id);
    if ($product->deleted != '1')
      return view('productView', compact('product', 'categories'));
    else
      return Redirect::to('/');
  }

  public function actionStoreWishlist(Request $request)
  {
    $product = Product::find($request->input('id_prod'));
    $wishlist = new Wishlist;
    $wishlist->product_id = $request->input('id_prod');
    $wishlist->wishlistable_id = Auth::user()->buyer->id;
    $wishlist->total = $request->input('quantity');
    $wishlist->wishlistable_type = "App\Buyer";
    $wishlist->save();
    return Redirect::to('wishlist');
  }

  public function actionStoreOrder(Request $request)
  {
    $product = Product::find($request->input('id_prod'));
    $temp_order_detail = new TempOrderDetail;
    $temp_order_detail->product_id = $request->input('id_prod');
    $temp_order_detail->buyer_id = Auth::user()->buyer->id;
    $temp_order_detail->quantity = $request->input('quantity');
    $temp_order_detail->sub_total = $product->price * $request->input('quantity');
    $temp_order_detail->sub_weight = $product->weight * $request->input('quantity');
    $temp_order_detail->save();
    return Redirect::to('product/view/'.$request->input('id_prod'));
  }

  public function categoryView($id)
  {
    $categories = Category::all();
    $category = Category::find($id);
    return view('category', compact('categories', 'category'));
  }
}
