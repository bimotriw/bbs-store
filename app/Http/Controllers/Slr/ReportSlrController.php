<?php

namespace App\Http\Controllers\Slr;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReportSlrController extends Controller
{
  public function listOrder()
  {
    $orders = $this->dbOrder("all");
    return view('slr/report', compact('orders'));
  }

  public function dbOrder($status)
  {
    if ($status == "all") {
    $orders = DB::table('orders')
            ->join('buyers', 'orders.buyer_id', '=', 'buyers.id')
            ->join('order_details', 'orders.id', '=', 'order_details.orderdetailable_id')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->join('sellers', 'products.seller_id', '=', 'sellers.id')
            ->select('orders.id', 'orders.created_at', 'orders.code', 'buyers.name', 'order_details.status', 'orders.updated_at', 'order_details.sub_total')
            ->where('sellers.id', '=', Auth::user()->seller->id)
            ->get();
    } else {
      $orders = DB::table('orders')
              ->join('buyers', 'orders.buyer_id', '=', 'buyers.id')
              ->join('order_details', 'orders.id', '=', 'order_details.orderdetailable_id')
              ->join('products', 'order_details.product_id', '=', 'products.id')
              ->join('sellers', 'products.seller_id', '=', 'sellers.id')
              ->select('orders.id', 'orders.created_at', 'orders.code', 'buyers.name', 'order_details.status', 'orders.updated_at', 'order_details.sub_total')
              ->where('sellers.id', '=', Auth::user()->seller->id)
              ->where('order_details.status', '=', $status)
              ->get();
    }
    return $orders;
  }
}
