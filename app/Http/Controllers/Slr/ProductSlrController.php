<?php

namespace App\Http\Controllers\Slr;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Input;
use File;
use Validator;
use App\Product;
use App\ProductImage;
use App\Category;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductSlrController extends Controller
{
  public function index()
  {
    return view('slr/product');
  }

  public function actionNew()
  {
    $categories = Category::all();
    return view('slr/productNew', compact('categories'));
  }

  public function actionStore(Request $request)
  {
    if (!$request->input('category_product')) {
      $request->session()->flash('error', 'Category Product Is Required');
      return redirect('sellerbbs/products/new')
        ->withInput();
    }
    if (Input::hasFile('main_image')) {
      $validator = Validator::make($request->all(), [
        'main_image' => 'image',
      ]);
      if ($validator->fails()) {
        $request->session()->flash('error', 'Adding Image Doesnt Success Because It Is Not Kind The Right Kind Of Image');
        return redirect('sellerbbs/products/new')
          ->withErrors($validator)
          ->withInput();
      }
      $product = new Product;
      $product->name = $request->input('name');
      $product->price = $request->input('price');
      $product->weight = $request->input('weight');
      $product->description = $request->input('description');
      $product->specification = $request->input('specification');
      $product->category_id = $request->input('category_product');
      $product->save();
      $product = Product::find($product->id);
      $product->code = $request->input('category_product')."-".Auth::user()->seller->id.$product->id;
      $product->seller_id = Auth::user()->seller->id;
      $product_image = new ProductImage;
      $file = Input::file('main_image');
      $file->move('uploads/product_images/'.$product->id, $file->getClientOriginalName());
      $product_image->resize_small_url = $file->getClientOriginalName();
      $path = 'uploads/product_images/'.$product->id.'/'.$file->getClientOriginalName();
      $product_image->url = $path;
      $product_image->product_id = $product->id;
      $product_image->type = "main";
      $product_image->description = $request->input('image_description');

      if (Input::hasFile('prod_file')) {
        $validator1 = Validator::make($request->all(), [
          'prod_file' => 'mimes:pdf,ppt,pptx,doc,docx',
          ]);
        if ($validator1->fails()) {
          $product->delete();
          $request->session()->flash('error', 'Adding Document Doesnt Success Because It Is Not Kind The Right Kind Of Document');
          return redirect('sellerbbs/products/new')
            ->withErrors($validator1)
            ->withInput();
        }
        $file = Input::file('prod_file');
        $file->move('uploads/additional_documents/'.$product->id, $file->getClientOriginalName());
        $product->additional_document_resize = $file->getClientOriginalName();
        $path = 'uploads/additional_documents/'.$product->id.'/'.$file->getClientOriginalName();
        $product->additional_document = $path;
        if ($product->save() && $product_image->save())
          $request->session()->flash('success', 'New Product Has Been Added');
        else
          $request->session()->flash('error', 'New Product Has Not Added');
        return Redirect::to('sellerbbs/products');
      } else {
        if ($product->save() && $product_image->save())
          $request->session()->flash('success', 'New Product Has Been Added');
        else
          $request->session()->flash('error', 'New Product Has Not Added');
        return Redirect::to('sellerbbs/products');
      }
    } else {
      $request->session()->flash('error', 'The Main Image Is Required');
      return redirect('sellerbbs/products/new')
        ->withInput();
    }
  }

  public function actionView(Request $request, $id)
  {
    $product = Product::find($id);
    $categories = Category::all();
    if ($product->seller_id != Auth::user()->seller->id)
      return Redirect::to('sellerbbs/products');
    if ($product->deleted != 0) {
      $request->session()->flash('error', 'The Product Has Been Deleted');
      return Redirect::to('sellerbbs/products');
    }
    return view('slr/productView', compact('product', 'categories'));
  }

  public function actionInactive(Request $request, $id)
  {
    $product = Product::find($id);
    $product->deleted = 1;
    if ($product->save())
      $request->session()->flash('success', 'Delete Product Was Successfull!');
    else
      $request->session()->flash('error', 'Delete Product Was Not Successfull!');
    return Redirect::to('sellerbbs/products');
  }

  public function actionUpdate(Request $request)
  {
    $id = $request->input('id');
    $product = Product::find($id);
    $product->name = $request->input('name');
    $product->price = $request->input('price');
    $product->weight = $request->input('weight');
    $product->description = $request->input('description');
    $product->specification = $request->input('specification');
    if($product->category_id != $request->input('category_product')) {
      $product->category_id = $request->input('category_product');
      $product->code = $request->input('category_product')."-".Auth::user()->seller->id.$product->id;
    }
    if (Input::hasFile('file2')) {
      $validator = Validator::make($request->all(), [
        'file2' => 'mimes:pdf,ppt,pptx,doc,docx',
      ]);
      if ($validator->fails()) {
        $request->session()->flash('error', 'Adding Document Doesnt Success Because It Is Not Kind The Right Kind Of Document');
        return Redirect::to('sellerbbs/products/view/'.$id);
      }
      $file = Input::file('file2');
      if($product->additional_document_resize == $file->getClientOriginalName()) {
        File::delete($product->additional_document);
        $file->move('uploads/additional_documents/'.$id, $file->getClientOriginalName());
        $product->additional_document_resize = $file->getClientOriginalName();
        $path = 'uploads/additional_documents/'.$id.'/'.$file->getClientOriginalName();
        $product->additional_document = $path;
        if ($product->save())
          $request->session()->flash('success', 'Product Has Been Updated');
        else
          $request->session()->flash('error', 'Product Has Not Been Updated');
        return Redirect::to('sellerbbs/products/view/'.$id);
      } else {
        if (file_exists(public_path('uploads/additional_documents/'.$id.'/'.$file->getClientOriginalName()))) {
          $request->session()->flash('error', 'Image Name Already Exist');
          return Redirect::to('sellerbbs/products/view/'.$id);
        } else {
          File::delete($product->additional_document);
          $file->move('uploads/additional_documents/'.$id, $file->getClientOriginalName());
          $product->additional_document_resize = $file->getClientOriginalName();
          $path = 'uploads/additional_documents/'.$id.'/'.$file->getClientOriginalName();
          $product->additional_document = $path;
          if ($product->save())
            $request->session()->flash('success', 'Product Has Been Updated');
          else
            $request->session()->flash('error', 'Product Has Not Been Updated');
          return Redirect::to('sellerbbs/products');
        }
      }
      return Redirect::to('sellerbbs/products/view/'.$id);
    } else {
      if ($product->save())
        $request->session()->flash('success', 'Product Has Been Updated');
      else
        $request->session()->flash('error', 'Product Has Not Been Updated');
      return Redirect::to('sellerbbs/products');
    }
  }

  public function actionSwapProductImage(Request $request, $id_prod, $id)
  {
    $product = Product::find($id_prod);
    foreach($product->product_images as $product_image) {
      if ($product_image->type == 'main') {
        $product_image->type = 'other';
        if ($product_image->save())
          $request->session()->flash('success', 'Main Image Has Been Changed');
        else
          $request->session()->flash('error', 'Main Image Has Not Been Changed');
      }
      if ($product_image->id == $id) {
        $product_image->type = 'main';
        if ($product_image->save())
          $request->session()->flash('success', 'Main Image Has Been Changed');
        else
          $request->session()->flash('error', 'Main Image Has Not Been Changed');
      }
    }
    return Redirect::to('sellerbbs/products/view/'.$id_prod);
  }

  public function actionDeleteProductImage(Request $request, $id_prod, $id)
  {
    $product_image = ProductImage::find($id);
    if ($product_image->delete()) {
      File::delete($product_image->url);
      $request->session()->flash('success', 'Image Has Been Deleted');
    } else
      $request->session()->flash('error', 'Image Has Not Been Deleted');
    return Redirect::to('sellerbbs/products/view/'.$id_prod);
  }

  public function actionStoreProductImage(Request $request)
  {
    $product_image = new ProductImage;
    $product_image->description = $request->input('description');
    $id_prod = $request->input('id_prod');
    if (Input::hasFile('file1')) {
      $validator = Validator::make($request->all(), [
        'file1' => 'image',
      ]);
      if ($validator->fails()) {
        $request->session()->flash('error', 'Adding Image Doesnt Success Because It Is Not Kind The Right Kind Of Image');
        return Redirect::to('sellerbbs/products/view/'.$id_prod);
      }
      $file = Input::file('file1');
      if (file_exists(public_path('uploads/product_images/'.$id_prod.'/'.$file->getClientOriginalName()))) {
        $request->session()->flash('error', 'Image Name Already Exist');
        return Redirect::to('sellerbbs/products/view/'.$id_prod);
      } else {
        $file->move('uploads/product_images/'.$id_prod, $file->getClientOriginalName());
        $product_image->resize_small_url = $file->getClientOriginalName();
        $path = 'uploads/product_images/'.$id_prod.'/'.$file->getClientOriginalName();
        $product_image->url = $path;
        $product_image->product_id = $id_prod;
        if ($product_image->save())
          $request->session()->flash('success', 'Image Has Been Added');
        else
          $request->session()->flash('error', 'Image Has Not Been Added');
      }
      return Redirect::to('sellerbbs/products/view/'.$id_prod);
    } else {
      $request->session()->flash('error', 'The Image Is Required');
      return Redirect::to('sellerbbs/products/view/'.$id_prod );
    }
  }

  public function actionUpdateProductImage(Request $request)
  {
    $id = $request->input('id');
    $product_image = ProductImage::find($id);
    $id_prod = $product_image->product_id;
    $product_image->description = $request->input('description');
    if (Input::hasFile('file')) {
      $validator = Validator::make($request->all(), [
        'file' => 'image',
      ]);
      if ($validator->fails()) {
        $request->session()->flash('error', 'Adding Image Doesnt Success Because It Is Not Kind The Right Kind Of Image');
        return Redirect::to('sellerbbs/products/view/'.$id_prod);
      }
      $file = Input::file('file');
      if($product_image->resize_small_url == $file->getClientOriginalName()) {
        File::delete($product_image->url);
        $file->move('uploads/product_images/'.$id_prod, $file->getClientOriginalName());
        $product_image->resize_small_url = $file->getClientOriginalName();
        $path = 'uploads/product_images/'.$id_prod.'/'.$file->getClientOriginalName();
        $product_image->url = $path;
        if ($product_image->save())
          $request->session()->flash('success', 'Image Has Been Updated');
        else
          $request->session()->flash('error', 'Image Has Not Been Updated');
        return Redirect::to('sellerbbs/products/view/'.$id_prod);
      } else {
        if (file_exists(public_path('uploads/product_images/'.$id_prod.'/'.$file->getClientOriginalName()))) {
          $request->session()->flash('error', 'Image Name Already Exist');
          return Redirect::to('sellerbbs/products/view/'.$id_prod);
        } else {
          File::delete($product_image->url);
          $file->move('uploads/product_images/'.$id_prod, $file->getClientOriginalName());
          $product_image->resize_small_url = $file->getClientOriginalName();
          $path = 'uploads/product_images/'.$id_prod.'/'.$file->getClientOriginalName();
          $product_image->url = $path;
          if ($product_image->save())
            $request->session()->flash('success', 'Image Has Been Updated');
          else
            $request->session()->flash('error', 'Image Has Not Been Updated');
          return Redirect::to('sellerbbs/products/view/'.$id_prod);
        }
      }
      return Redirect::to('sellerbbs/products/view/'.$id_prod);
    } else {
      if ($product_image->save())
        $request->session()->flash('success', 'Image Has Been Updated');
      else
        $request->session()->flash('error', 'Image Has Not Been Updated');
      return Redirect::to('sellerbbs/products/view/'.$id_prod);
    }
  }

}
