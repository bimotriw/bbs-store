<?php

namespace App\Http\Controllers\Slr;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeSlrController extends Controller
{
  public function index()
  {
    $orders = $this->dbOrder("all");
    $ordersOrdered = $this->dbOrder(2);
    $ordersProcessed = $this->dbOrder('0');
    $ordersPaid = $this->dbOrder(1);
    return view('slr/home', compact('orders', 'ordersOrdered', 'ordersProcessed', 'ordersPaid'));
  }

  public function help()
  {
    return view('slr/help');
  }

  public function dbOrder($status)
  {
    if ($status == "all") {
    $orders = DB::table('orders')
            ->join('buyers', 'orders.buyer_id', '=', 'buyers.id')
            ->join('order_details', 'orders.id', '=', 'order_details.orderdetailable_id')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->join('sellers', 'products.seller_id', '=', 'sellers.id')
            ->select('orders.id', 'orders.created_at', 'orders.code', 'buyers.name', 'order_details.status', 'orders.updated_at', 'order_details.sub_total')
            ->where('sellers.id', '=', Auth::user()->seller->id)
            ->get();
    } else {
      $orders = DB::table('orders')
              ->join('buyers', 'orders.buyer_id', '=', 'buyers.id')
              ->join('order_details', 'orders.id', '=', 'order_details.orderdetailable_id')
              ->join('products', 'order_details.product_id', '=', 'products.id')
              ->join('sellers', 'products.seller_id', '=', 'sellers.id')
              ->select('orders.id', 'orders.created_at', 'orders.code', 'buyers.name', 'order_details.status', 'orders.updated_at', 'order_details.sub_total')
              ->where('sellers.id', '=', Auth::user()->seller->id)
              ->where('order_details.status', '=', $status)
              ->get();
    }
    return $orders;
  }
}
