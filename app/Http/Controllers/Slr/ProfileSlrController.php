<?php

namespace App\Http\Controllers\Slr;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Hash;
use Input;
use File;
use Validator;
use App\SellerImage;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProfileSlrController extends Controller
{
  public function index()
  {
    return view('slr/profile');
  }

  public function actionUpdateProfile(Request $request)
  {
    Auth::user()->seller->name = $request->input('name');
    Auth::user()->seller->company = $request->input('company_sel');
    Auth::user()->seller->address = $request->input('address');
    Auth::user()->seller->phone = $request->input('phone');
    Auth::user()->seller->description = $request->input('description');
    Auth::user()->seller->company_type = $request->input('company_type');
    Auth::user()->seller->year_established = $request->input('year_established');
    if (Auth::user()->seller->save())
      $request->session()->flash('success', 'Update Profile Was Successfull!');
    else
      $request->session()->flash('error', 'Update Profile Was Not Successfull!');
    return Redirect::to('sellerbbs/profile');
  }

  public function actionUpdatePassword(Request $request)
  {
    Auth::user()->password = Hash::make($request->input('password'));
    if (Auth::user()->save())
      $request->session()->flash('success', 'Update Password Was Successfull!');
    else
      $request->session()->flash('error', 'Update Password Was Not Successfull!');
    return Redirect::to('sellerbbs/profile');
  }

  public function actionUpdateAccountNumber(Request $request)
  {
    foreach(Auth::user()->seller->accounts as $account) {
      if ($account->account_type == 'BCA')
        $account->account_number = $request->input('BCA');
      if ($account->account_type == 'BRI')
        $account->account_number = $request->input('BRI');
      if ($account->account_type == 'Mandiri')
        $account->account_number = $request->input('Mandiri');
      if ($account->save())
        $request->session()->flash('success', 'Update Profile Was Successfull!');
      else
        $request->session()->flash('error', 'Update Profile Was Not Successfull!');
    }
    return Redirect::to('sellerbbs/profile');
  }

  public function actionSwapSellerImage(Request $request, $id)
  {
    foreach(Auth::user()->seller->seller_images as $seller_image) {
      if ($seller_image->type == 'main') {
        $seller_image->type = 'other';
        if ($seller_image->save())
          $request->session()->flash('success', 'Main Image Has Been Changed');
        else
          $request->session()->flash('error', 'Main Image Has Not Been Changed');
      }
      if ($seller_image->id == $id) {
        $seller_image->type = 'main';
        if ($seller_image->save())
          $request->session()->flash('success', 'Main Image Has Been Changed');
        else
          $request->session()->flash('error', 'Main Image Has Not Been Changed');
      }
    }
    return Redirect::to('sellerbbs/profile');
  }

  public function actionDeleteSellerImage(Request $request, $id)
  {
    $seller_image = SellerImage::find($id);
    if ($seller_image->delete()) {
      File::delete($seller_image->url);
      $request->session()->flash('success', 'Image Has Been Deleted');
    } else
      $request->session()->flash('error', 'Image Has Not Been Deleted');
    return Redirect::to('sellerbbs/profile');
  }

  public function actionUpdateSellerImage(Request $request)
  {
    $id = $request->input('id');
    $seller_image = SellerImage::find($id);
    $seller_image->description = $request->input('description');
    if (Input::hasFile('file')) {
      $validator = Validator::make($request->all(), [
        'file' => 'image',
      ]);
      if ($validator->fails()) {
        $request->session()->flash('error', 'Adding Image Doesnt Success Because It Is Not Kind The Right Kind Of Image');
        return Redirect::to('sellerbbs/profile');
      }
      $file = Input::file('file');
      if($seller_image->resize_small_url == $file->getClientOriginalName()) {
        File::delete($seller_image->url);
        $file->move('uploads/seller_images/'.Auth::user()->seller->id, $file->getClientOriginalName());
        $seller_image->resize_small_url = $file->getClientOriginalName();
        $path = 'uploads/seller_images/'.Auth::user()->seller->id.'/'.$file->getClientOriginalName();
        $seller_image->url = $path;
        if ($seller_image->save())
          $request->session()->flash('success', 'Image Has Been Updated');
        else
          $request->session()->flash('error', 'Image Has Not Been Updated');
        return Redirect::to('sellerbbs/profile');
      } else {
        if (file_exists(public_path('uploads/seller_images/'.Auth::user()->seller->id.'/'.$file->getClientOriginalName()))) {
          $request->session()->flash('error', 'Image Name Already Exist');
          return Redirect::to('sellerbbs/profile');
        } else {
          File::delete($seller_image->url);
          $file->move('uploads/seller_images/'.Auth::user()->seller->id, $file->getClientOriginalName());
          $seller_image->resize_small_url = $file->getClientOriginalName();
          $path = 'uploads/seller_images/'.Auth::user()->seller->id.'/'.$file->getClientOriginalName();
          $seller_image->url = $path;
          if ($seller_image->save())
            $request->session()->flash('success', 'Image Has Been Updated');
          else
            $request->session()->flash('error', 'Image Has Not Been Updated');
          return Redirect::to('sellerbbs/profile');
        }
      }
      return Redirect::to('sellerbbs/profile');
    } else {
      if ($seller_image->save())
        $request->session()->flash('success', 'Image Has Been Updated');
      else
        $request->session()->flash('error', 'Image Has Not Been Updated');
      return Redirect::to('sellerbbs/profile');
    }
  }

  public function actionStoreSellerImage(Request $request)
  {
    $seller_image = new SellerImage;
    $seller_image->description = $request->input('description');
    if (Input::hasFile('file1')) {
      $validator = Validator::make($request->all(), [
        'file1' => 'image',
      ]);
      if ($validator->fails()) {
        $request->session()->flash('error', 'Adding Image Doesnt Success Because It Is Not Kind The Right Kind Of Image');
        return Redirect::to('sellerbbs/profile');
      }
      $file = Input::file('file1');
      if (file_exists(public_path('uploads/seller_images/'.Auth::user()->seller->id.'/'.$file->getClientOriginalName()))) {
        $request->session()->flash('error', 'Image Name Already Exist');
        return Redirect::to('sellerbbs/profile');
      } else {
        $file->move('uploads/seller_images/'.Auth::user()->seller->id, $file->getClientOriginalName());
        $seller_image->resize_small_url = $file->getClientOriginalName();
        $path = 'uploads/seller_images/'.Auth::user()->seller->id.'/'.$file->getClientOriginalName();
        $seller_image->url = $path;
        $seller_image->seller_id = Auth::user()->seller->id;
        if ($seller_image->save())
          $request->session()->flash('success', 'Image Has Been Added');
        else
          $request->session()->flash('error', 'Image Has Not Been Added');
      }
      return Redirect::to('sellerbbs/profile');
    } else {
      $request->session()->flash('error', 'The Image Is Required');
      return Redirect::to('sellerbbs/profile');
    }
  }
}
