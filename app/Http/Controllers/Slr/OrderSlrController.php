<?php

namespace App\Http\Controllers\Slr;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use DB;
use Input;
use File;
use Validator;
use App\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OrderSlrController extends Controller
{
  public function index(Request $request)
  {
    $orders = $this->dbOrder("all");
    return view('slr/order', compact('orders'));
  }

  public function actionListOrdered(Request $request)
  {
    $orders = $this->dbOrder(2);
    return view('slr/order', compact('orders'));
  }

  public function actionListProcessed(Request $request)
  {
    $orders = $this->dbOrder('0');
    return view('slr/order', compact('orders'));
  }

  public function actionListPaid(Request $request)
  {
    $orders = $this->dbOrder(1);
    return view('slr/order', compact('orders'));
  }

  public function actionListInvalid(Request $request)
  {
    $orders = $this->dbOrder(3);
    return view('slr/order', compact('orders'));
  }

  public function actionView($id)
  {
    $order = Order::find($id);
    foreach (Auth::user()->seller->products as $product) {
      foreach ($product->order_detailOrders as $detail) {
        if ($detail->status == 2 && $detail->orderdetailable_id == $id) {
          $detail->notification_status_seller = 1;
          $detail->save();
        }
      }
    }
    return view('slr/orderView', compact('order'));
  }

  public function actionProcess(Request $request)
  {
    $id = $request->input('id_order');
    $order = Order::find($id);
    if ($order->code == $request->input('code_order_verification')) {
      if (Input::hasFile('verification_resi')) {
        $validator = Validator::make($request->all(), [
          'verification_resi' => 'image',
        ]);
        if ($validator->fails()) {
          $request->session()->flash('error', 'Confirm Sales Invoice Doesnt Success Because Photo Verification Is Not Kind The Right Kind Of Image');
          return Redirect::to('sellerbbs/orders/view/'.$id);
        }
        $file = Input::file('verification_resi');
        $file->move('uploads/verification_images/'.Auth::user()->seller->id.'-'.$id, $file->getClientOriginalName());
        $path = 'uploads/verification_images/'.Auth::user()->seller->id.'-'.$id.'/'.$file->getClientOriginalName();
        foreach ($order->order_details as $detail) {
          if ($detail->product->seller_id == Auth::user()->seller->id) {
            $detail->resize_small_url = $file->getClientOriginalName();
            $detail->url = $path;
            $detail->resi = $request->input('resi_verification');
            $detail->status = 0;
            if ($detail->save())
              $request->session()->flash('success', 'Sales Invoice Will Be Processed');
            else
              $request->session()->flash('error', 'Sales Invoice Invalid');
          }
        }
      }
    } else
      $request->session()->flash('error', 'The Code of Sales Invoice Didnt Match');
    return Redirect::to('sellerbbs/orders/view/'.$id);
  }

  public function dbOrder($status)
  {
    if ($status == "all") {
    $orders = DB::table('orders')
            ->join('buyers', 'orders.buyer_id', '=', 'buyers.id')
            ->join('order_details', 'orders.id', '=', 'order_details.orderdetailable_id')
            ->join('products', 'order_details.product_id', '=', 'products.id')
            ->join('sellers', 'products.seller_id', '=', 'sellers.id')
            ->select('orders.id', 'orders.created_at', 'orders.code', 'buyers.name', 'order_details.status', 'orders.updated_at')
            ->where('sellers.id', '=', Auth::user()->seller->id)
            ->get();
    } else {
      $orders = DB::table('orders')
              ->join('buyers', 'orders.buyer_id', '=', 'buyers.id')
              ->join('order_details', 'orders.id', '=', 'order_details.orderdetailable_id')
              ->join('products', 'order_details.product_id', '=', 'products.id')
              ->join('sellers', 'products.seller_id', '=', 'sellers.id')
              ->select('orders.id', 'orders.created_at', 'orders.code', 'buyers.name', 'order_details.status', 'orders.updated_at')
              ->where('sellers.id', '=', Auth::user()->seller->id)
              ->where('order_details.status', '=', $status)
              ->get();
    }
    return $orders;
  }
}
