<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Category;
use App\Wishlist;
use App\TempOrderDetail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class WishlistController extends Controller
{
  public function index()
  {
    $categories = Category::all();
    return view('wishlist', compact('categories'));
  }

  public function actionDelete($id)
  {
    $wishlist = Wishlist::find($id);
    $wishlist->delete();
    return Redirect::to('wishlist');
  }

  public function actionStore(Request $request)
  {
    $wishlist = Wishlist::find($request->input('id'));
    $temp_order_details = new TempOrderDetail;
    $temp_order_details->product_id = $wishlist->product_id;
    $temp_order_details->buyer_id = Auth::user()->buyer->id;
    $quantity = $request->input('quantity');
    $temp_order_details->quantity = $request->input('quantity');
    $temp_order_details->sub_total = $wishlist->product->price * $quantity;
    $temp_order_details->sub_weight = $wishlist->product->weight * $quantity;
    $temp_order_details->save();
    $wishlist->delete();
    return Redirect::to('wishlist');
  }
}
