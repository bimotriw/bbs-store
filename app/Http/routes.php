<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/
/* FrontEnd */
Route::post('/log', 'AuthController@store');
Route::get('/logout', 'AuthController@logout');
Route::get('/create/account', 'AuthController@createAccount');
Route::post('/create/account', 'AuthController@storeAccount');

Route::get('/', 'HomeController@index');
Route::get('/product/store/order/{id}', 'HomeController@actionStoreOrder');
Route::get('/product/store/wishlist/{id}', 'HomeController@actionStoreWishlist');
Route::get('/product/delete/wishlist/{id}', 'HomeController@actionDeleteWishlist');

Route::get('/account', 'HomeController@accountView');
Route::post('/account/edit', 'HomeController@editAccount');

Route::get('/product/view/{id}', 'ProductController@actionView');
Route::post('/product/view/store/order', 'ProductController@actionStoreOrder');
Route::post('/product/view/store/wishlist', 'ProductController@actionStoreWishlist');

Route::get('/category/view/{id}', 'ProductController@categoryView');

Route::get('/order', 'OrderController@index');
Route::get('/order/view/{id}', 'OrderController@actionView');
Route::get('/order/verification/{id}', 'OrderController@actionVerification');
Route::post('/order', 'OrderController@actionComplaint');

Route::get('/wishlist', 'WishlistController@index');
Route::get('/wishlist/delete/{id}', 'WishlistController@actionDelete');
Route::post('/wishlist/store', 'WishlistController@actionStore');

Route::get('/checkout', 'CheckOutController@index');
Route::get('/checkout/delete/{id}', 'CheckOutController@actionDelete');
Route::post('/checkout/update', 'CheckOutController@actionUpdateQuantity');
Route::post('/checkout/store', 'CheckOutController@actionStore');


/*----------------------------------------------------------------------------*/

/* SellerBBS */
Route::get('/sellerbbs', [
    'middleware' => 'seller',
    'uses' => 'Slr\HomeSlrController@index'
]);

Route::get('/sellerbbs/logout', [
    'middleware' => 'seller',
    'uses' => 'AuthController@logout'
]);

//profile
Route::get('/sellerbbs/profile', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@index'
]);
Route::get('/sellerbbs/swapprofileimage/{id}', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@actionSwapSellerImage'
]);
Route::get('/sellerbbs/deleteprofileimage/{id}', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@actionDeleteSellerImage'
]);
Route::post('/sellerbbs/editprofile', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@actionUpdateProfile'
]);
Route::post('/sellerbbs/editpassword', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@actionUpdatePassword'
]);
Route::post('/sellerbbs/editaccountnumber', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@actionUpdateAccountNumber'
]);
Route::post('/sellerbbs/storeprofileimage', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@actionStoreSellerImage'
]);
Route::post('/sellerbbs/updateprofileimage', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProfileSlrController@actionUpdateSellerImage'
]);

//product
Route::get('/sellerbbs/products', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@index'
]);
Route::get('/sellerbbs/products/view/{id}', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionView'
]);
Route::get('/sellerbbs/deleteproduct/{id}', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionInactive'
]);
Route::get('/sellerbbs/swapproductimage/{id_prod}/{id}', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionSwapProductImage'
]);
Route::get('/sellerbbs/deleteproductimage/{id_prod}/{id}', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionDeleteProductImage'
]);
Route::get('/sellerbbs/products/new', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionNew'
]);
Route::post('/sellerbbs/storeproduct', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionStore'
]);
Route::post('/sellerbbs/editproduct', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionUpdate'
]);
Route::post('/sellerbbs/storeproductimage', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionStoreProductImage'
]);
Route::post('/sellerbbs/updateproductimage', [
    'middleware' => 'seller',
    'uses' => 'Slr\ProductSlrController@actionUpdateProductImage'
]);

//order
Route::get('/sellerbbs/orders', [
    'middleware' => 'seller',
    'uses' => 'Slr\OrderSlrController@index'
]);
Route::get('/sellerbbs/orders/ordered', [
    'middleware' => 'seller',
    'uses' => 'Slr\OrderSlrController@actionListOrdered'
]);
Route::get('/sellerbbs/orders/processed', [
    'middleware' => 'seller',
    'uses' => 'Slr\OrderSlrController@actionListProcessed'
]);
Route::get('/sellerbbs/orders/paid', [
    'middleware' => 'seller',
    'uses' => 'Slr\OrderSlrController@actionListPaid'
]);
Route::get('/sellerbbs/orders/invalid', [
    'middleware' => 'seller',
    'uses' => 'Slr\OrderSlrController@actionListInvalid'
]);
Route::get('/sellerbbs/orders/view/{id}', [
    'middleware' => 'seller',
    'uses' => 'Slr\OrderSlrController@actionView'
]);
Route::post('/sellerbbs/orders/process', [
    'middleware' => 'seller',
    'uses' => 'Slr\OrderSlrController@actionProcess'
]);

//report
Route::get('/sellerbbs/reports/order', [
    'middleware' => 'seller',
    'uses' => 'Slr\ReportSlrController@listOrder'
]);

//help
Route::get('/sellerbbs/help', [
    'middleware' => 'seller',
    'uses' => 'Slr\HomeSlrController@help'
]);


/*----------------------------------------------------------------------------*/

/* AdminBBS */
Route::get('/adminbbs', [
    'middleware' => 'admin',
    'uses' => 'Adm\HomeAdmController@index'
]);
Route::get('/adminbbs/logout', [
    'middleware' => 'admin',
    'uses' => 'AuthController@logout'
]);

//buyers
Route::get('/adminbbs/buyers', [
    'middleware' => 'admin',
    'uses' => 'Adm\BuyerAdmController@actionList'
]);
Route::get('/adminbbs/buyers/view/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\BuyerAdmController@actionView'
]);
Route::get('/adminbbs/buyers/new', [
    'middleware' => 'admin',
    'uses' => 'Adm\BuyerAdmController@actionNew'
]);
Route::post('/adminbbs/storebuyer', [
    'middleware' => 'admin',
    'uses' => 'Adm\BuyerAdmController@actionStore'
]);
Route::post('/adminbbs/editbuyer', [
    'middleware' => 'admin',
    'uses' => 'Adm\BuyerAdmController@actionUpdate'
]);

//sellers
Route::get('/adminbbs/sellers', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionList'
]);
Route::get('/adminbbs/sellers/view/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionView'
]);
Route::get('/adminbbs/sellers/new', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionNew'
]);
Route::get('/adminbbs/inactiveseller/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionInactive'
]);
Route::get('/adminbbs/activeseller/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionActive'
]);
Route::post('/adminbbs/storeseller', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionStore'
]);
Route::post('/adminbbs/editseller', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionUpdate'
]);

//sellers->product
Route::get('/adminbbs/sellers/{id}/inactive/{id_prod}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionProductInactive'
]);
Route::get('/adminbbs/sellers/{id}/active/{id_prod}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionProductActive'
]);
Route::get('/adminbbs/sellers/{id}/products/{id_prod}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionProductView'
]);

/* NOT IN USE
Route::get('/adminbbs/sellers/{id}/product/{id_prod}/swap/{id_img}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionProductSwap'
]);
Route::get('/adminbbs/sellers/{id}/product/{id_prod}/delete/{id_img}', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionProductDelete'
]);
Route::get('/adminbbs/sellers/products/newimage', [
    'middleware' => 'admin',
    'uses' => 'Adm\SellerAdmController@actionStoreImage'
]);
*/

//catalogs->categories
Route::get('/adminbbs/catalogs/categories', [
    'middleware' => 'admin',
    'uses' => 'Adm\CategoryAdmController@actionList'
]);
Route::get('/adminbbs/catalogs/categories/delete/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\CategoryAdmController@actionDelete'
]);
Route::post('/adminbbs/catalogs/categories/new', [
    'middleware' => 'admin',
    'uses' => 'Adm\CategoryAdmController@actionStore'
]);
Route::post('/adminbbs/catalogs/editcategories', [
    'middleware' => 'admin',
    'uses' => 'Adm\CategoryAdmController@actionUpdate'
]);

//catalogs->product
Route::get('/adminbbs/catalogs/products', [
    'middleware' => 'admin',
    'uses' => 'Adm\ProductAdmController@actionList'
]);
Route::get('/adminbbs/catalogs/products/inactiveproduct/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\ProductAdmController@actionInactive'
]);
Route::get('/adminbbs/catalogs/products/activeproduct/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\ProductAdmController@actionActive'
]);

//orders
Route::get('/adminbbs/orders', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionList'
]);
Route::get('/adminbbs/orders/view/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionView'
]);

Route::get('/adminbbs/orders/successfull/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionSuccess'
]);
Route::get('/adminbbs/orders/fail/{id}', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionFail'
]);

Route::get('/adminbbs/orders/detail/successfull/{id}/{id_seller}', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionDetailSuccess'
]);
Route::get('/adminbbs/orders/detail/fail/{id}/{id_seller}', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionDetailFail'
]);

Route::get('/adminbbs/orders/responded', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionListResponded'
]);
Route::get('/adminbbs/orders/paid', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionListPaid'
]);
Route::get('/adminbbs/orders/invalid', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@actionListInvalid'
]);
Route::get('/adminbbs/orders/complaints', [
    'middleware' => 'admin',
    'uses' => 'Adm\OrderAdmController@complaints'
]);

//report
Route::get('/adminbbs/reports/order', [
    'middleware' => 'admin',
    'uses' => 'Adm\ReportAdmController@listOrder'
]);

//setting
Route::get('/adminbbs/settings/help', [
    'middleware' => 'admin',
    'uses' => 'Adm\HomeAdmController@help'
]);
Route::get('/adminbbs/settings/backup', [
    'middleware' => 'admin',
    'uses' => 'Adm\HomeAdmController@backup'
]);
