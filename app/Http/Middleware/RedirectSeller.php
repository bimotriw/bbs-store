<?php

namespace App\Http\Middleware;

use Auth;
use Redirect;
use Closure;

class RedirectSeller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if(Auth::check())
      {
        if(Auth::user()->role == 0)
          return Redirect::to('/adminbbs');
        if(Auth::user()->role != 2)
          return Redirect::to('/');
      }
      else
        return Redirect::to('/');

      return $next($request);
    }
}
