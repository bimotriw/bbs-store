<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $fillable = ['code','buyer_id','status'];

  public function buyer()
  {
    return $this->belongsTo('App\Buyer');
  }

  public function order_details()
  {
    return $this->morphMany('App\OrderDetail', 'orderdetailable');
  }
}
