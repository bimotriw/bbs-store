<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
  protected $fillable = ['buyer_id','product_id'];

  public function wishlistable()
  {
    return $this->morphTo();
  }

  public function product()
  {
    return $this->belongsTo('App\Product');
  }
}
