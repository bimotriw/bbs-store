<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Seller extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
  use Authenticatable, Authorizable, CanResetPassword;
  protected $fillable = ['name','email','company','address','phone','description','company_type','year_established','deleted'];

  public function user()
  {
    return $this->hasOne('App\User');
  }

  public function seller_images()
  {
    return $this->hasMany('App\SellerImage');
  }

  public function products()
  {
    return $this->hasMany('App\Product');
  }

  public function accounts()
  {
    return $this->hasMany('App\SellerAccount');
  }

  public function product_images()
  {
    return $this->hasManyThrough('App\ProductImage', 'App\Product');
  }
}
