<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempOrderDetail extends Model
{
  public function buyer()
  {
    return $this->belongsTo('App\Buyer');
  }

  public function product()
  {
    return $this->belongsTo('App\Product');
  }
}
