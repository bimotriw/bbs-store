<!doctype html>
<html lang="en">
	<head>
		<title>BBStore</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!--meta info-->
		<meta name="author" content="">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<link rel="icon" type="image/ico" href="{{url('assets/images/fav.png')}}">
		<!--stylesheet include-->
		{!! Html::style('assets/css/flexslider.css') !!}
		{!! Html::style('assets/css/bootstrap.min.css') !!}
		{!! Html::style('assets/css/owl.carousel.css') !!}
		{!! Html::style('assets/css/owl.transitions.css') !!}
		{!! Html::style('assets/css/jquery.custom-scrollbar.css') !!}
		{!! Html::style('assets/css/style.css') !!}
		<!--font include-->
		{!! Html::style('assets/css/font-awesome.min.css') !!}
		@if (Request::is('checkout'))
			{!! Html::style('assets/shipping/easyui.css') !!}
		@endif
	</head>
	<body>
		<!--boxed layout-->
		<div class="wide_layout relative w_xs_auto">
			<!--[if (lt IE 9) | IE 9]>
				<div style="background:#fff;padding:8px 0 10px;">
				<div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix" style="padding:9px 0 0;float:left;width:83%;"><i class="fa fa-exclamation-triangle scheme_color f_left m_right_10" style="font-size:25px;color:#e74c3c;"></i><b style="color:#e74c3c;">Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:16%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_4 r_corners bg_scheme_color color_light d_inline_b t_align_c" target="_blank" style="margin-bottom:2px;">Update Now!</a></div></div></div></div>
			<![endif]-->
			<!--markup header type 2-->
			<header role="banner">
				<!--header top part-->
				<section class="h_top_part">
					<div class="container">
						<div class="row clearfix">
							<div class="col-lg-4 col-md-4 col-sm-3 t_xs_align_c">
								@if (!Auth::check())
									<p class="f_size_small">Welcome visitor can you	<a href="#" data-popup="#login_popup">Log In</a> or <a href="{{url('create/account')}}">Create an Account</a> </p>
								@endif
								</div>
							<nav class="col-lg-4 col-md-4 col-sm-6 t_align_c t_xs_align_c"></nav>
							<div class="col-lg-4 col-md-4 col-sm-3 t_align_r t_xs_align_c">
								<ul class="d_inline_b horizontal_list clearfix f_size_small users_nav">
									<li>
										<a href="#" class="default_t_color">
											@if (Auth::check())
												{!!HTML::linkAction('HomeController@accountView', Auth::user()->buyer->name, array())!!}
											@else
												<a href="#" data-popup="#login_popup" class="default_t_color">My Account</a>
											@endif
										</a>
									</li>
									@if (Auth::check())
										<li>{!!HTML::linkAction('OrderController@index', 'Order List', array(), array('class' => 'default_t_color'))!!}</li>
										<li>{!!HTML::linkAction('WishlistController@index', 'Wishlist', array(), array('class' => 'default_t_color'))!!}</li>
										<li>{!!HTML::linkAction('CheckOutController@index', 'Checkout', array(), array('class' => 'default_t_color'))!!}</li>
										<li><a href="{{url('logout')}}" class="">Logout</a></li>
									@else
										<li><a href="#" data-popup="#login_popup" class="default_t_color">Orders List</a></li>
										<li><a href="#" data-popup="#login_popup" class="default_t_color">Wishlist</a></li>
										<li><a href="#" data-popup="#login_popup" class="default_t_color">Checkout</a></li>
									@endif
								</ul>
							</div>
						</div>
					</div>
				</section>
				<!--header bottom part-->
				<section class="h_bot_part container">
					<div class="clearfix row">
						<div class="col-lg-6 col-md-6 col-sm-4 t_xs_align_c">
							<a href="{{url('/')}}" class="logo m_xs_bottom_15 d_xs_inline_b">
								{!!HTML::image('assets/images/logo.png', '...')!!}
							</a>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-8">
							<div class="row clearfix">
								<div class="col-lg-6 col-md-6 col-sm-6 t_align_r t_xs_align_c m_xs_bottom_15">
									<dl class="l_height_medium">
										<dt class="f_size_small">Call us toll free:</dt>
										<dd class="f_size_ex_large color_dark"><b>(0341) 713857</b></dd>
									</dl>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<form class="relative type_2" role="search">
										<input type="text" placeholder="Search" name="search" class="r_corners f_size_medium full_width">
										<button class="f_right search_button tr_all_hover f_xs_none">
											<i class="fa fa-search"></i>
										</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--main menu container-->
				<div class="container">
					<section class="menu_wrap type_2 relative clearfix t_xs_align_c m_bottom_20">
						<!--button for responsive menu-->
						<button id="menu_button" class="r_corners centered_db d_none tr_all_hover d_xs_block m_bottom_15">
							<span class="centered_db r_corners"></span>
							<span class="centered_db r_corners"></span>
							<span class="centered_db r_corners"></span>
						</button>
						<!--main menu-->
						<nav role="navigation" class="f_left f_xs_none d_xs_none t_xs_align_l">
							<ul class="horizontal_list main_menu clearfix">
								<li class="current relative f_xs_none m_xs_bottom_5"><a href="{{url('/')}}" class="tr_delay_hover color_light tt_uppercase"><b>Home</b></a></li>
							</ul>
						</nav>
						<ul class="f_right horizontal_list clearfix t_align_l t_xs_align_c site_settings d_xs_inline_b f_xs_none">
							<!--shopping cart-->
							<li class="m_left_5 relative container3d" id="shopping_button">
								<a role="button" href="#" class="button_type_3 color_light bg_scheme_color d_block r_corners tr_delay_hover box_s_none">
									<span class="d_inline_middle shop_icon">
										<i class="fa fa-shopping-cart"></i>
										@if (Auth::check())
											<?php $tot_order = 0 ?>
											@foreach (Auth::user()->buyer->temp_order_details as $order)
												@if ($order->product->deleted != '1')
													<?php $tot_order++; ?>
												@endif
											@endforeach
											<span class="count tr_delay_hover type_2 circle t_align_c">{{$tot_order}}</span>
										@endif
									</span>
									<?php $cost = 0; ?>
									@if (Auth::check())
										@foreach (Auth::user()->buyer->temp_order_details as $order)
											@if ($order->product->deleted != '1')
												<?php $cost = $cost + $order->sub_total; ?>
											@endif
										@endforeach
										<?php $cost = number_format ($cost, 0, ',', '.'); ?>
									@endif
									<b>Rp. {{$cost}},-</b>
								</a>
								@if (Auth::check())
								<div class="shopping_cart top_arrow tr_all_hover r_corners">
									<div class="f_size_medium sc_header">Recently added item(s)</div>
									<ul class="products_list">
										@if (count(Auth::user()->buyer->temp_order_details))
											@foreach (Auth::user()->buyer->temp_order_details as $order)
												@if ($order->product->deleted != '1')
													<li>
														<div class="clearfix">
															<!--product image-->
															@foreach ($order->product->product_images as $image)
																@if ($image->type == 'main')
																	{!!HTML::image($image->url, '...', array('class' => 'f_left m_right_10 cart_order_img'))!!}
																@endif
															@endforeach
															<!--product description-->
															<div class="f_left product_description">
																<a href="#" class="color_dark m_bottom_5 d_block">{{$order->product->name}}</a>
																<span class="f_size_medium">CODE : {{$order->product->code}}</span>
															</div>
															<!--product price-->
															<div class="f_right f_size_medium">
																<div class="clearfix">
																	<?php $price = number_format ($order->product->price, 0, ',', '.'); ?>
																	{{$order->quantity}} x <b class="color_dark">Rp. {{$price}},-</b>
																</div>
																<!--<button class="close_product color_dark tr_hover"><i class="fa fa-times"></i></button>-->
															</div>
														</div>
													</li>
												@endif
											@endforeach
										@endif
									</ul>
									<!--total price-->
									<ul class="total_price bg_light_color_1 t_align_r color_dark">
										<li>Total: <b class="f_size_large bold scheme_color sc_price t_align_l d_inline_b m_left_15">Rp. {{$cost}},-</b></li>
									</ul>
									<div class="sc_footer t_align_c">
										{!!Html::linkAction('CheckOutController@index', 'Checkout', array(), array('class' => 'button_type_4 bg_scheme_color d_inline_middle r_corners tr_all_hover color_light', 'role' => 'button'))!!}
									</div>
								</div>
								@endif
							</li>
						</ul>
					</section>
				</div>
			</header>
			<!--content-->
				@yield('content')
			<!--markup footer-->
			<footer id="footer" class="type_2">
				<div class="container clearfix t_mxs_align_c">
					<p class="f_left f_mxs_none m_mxs_bottom_10">&copy; 2015 <span class="color_light">BBStore</span></p>
				</div>
			</footer>
		</div>
		<!--social widgets-->
		<ul class="social_widgets d_xs_none">
			<!--contact form-->
			<li class="relative">
				<button class="sw_button t_align_c contact"><i class="fa fa-envelope-o"></i></button>
				<div class="sw_content">
					<h3 class="color_dark m_bottom_20">Contact Us</h3>
					<p class="f_size_medium m_bottom_15">Lorem ipsum dolor sit amet, consectetuer adipis mauris</p>
					<form id="contactform" class="mini">
						<input class="f_size_medium m_bottom_10 r_corners full_width" type="text" name="cf_name" placeholder="Your name">
						<input class="f_size_medium m_bottom_10 r_corners full_width" type="email" name="cf_email" placeholder="Email">
						<textarea class="f_size_medium r_corners full_width m_bottom_20" placeholder="Message" name="cf_message"></textarea>
						<button type="submit" class="button_type_4 r_corners mw_0 tr_all_hover color_dark bg_light_color_2">Send</button>
					</form>
				</div>
			</li>
		</ul>
		<!--login popup-->
		<div class="popup_wrap d_none" id="login_popup">
			<section class="popup r_corners shadow">
				<button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
				<h3 class="m_bottom_20 color_dark">Log In</h3>
				{!!Form::open(array('action' => 'AuthController@store', 'class' => 'validator-form'))!!}
					<ul>
						<li class="m_bottom_15">
							<label for="email" class="m_bottom_5 d_inline_b">Email</label><br>
							<input type="text" name="email" id="username" class="r_corners full_width">
						</li>
						<li class="m_bottom_25">
							<label for="password" class="m_bottom_5 d_inline_b">Password</label><br>
							<input type="password" name="password" id="password" class="r_corners full_width">
						</li>
						<li class="clearfix m_bottom_30">
							<button class="button_type_4 tr_all_hover r_corners f_left bg_scheme_color color_light f_mxs_none m_mxs_bottom_15">Log In</button>
						</li>
					</ul>
				{!!Form::close()!!}
				<footer class="bg_light_color_1 t_mxs_align_c">
					<h3 class="color_dark d_inline_middle d_mxs_block m_mxs_bottom_15">New Customer?</h3>
					<a href="{{url('create/account')}}" role="button" class="tr_all_hover r_corners button_type_4 bg_dark_color bg_cs_hover color_light d_inline_middle m_mxs_left_0">Create an Account</a>
				</footer>
			</section>
		</div>
		<button class="t_align_c r_corners type_2 tr_all_hover animate_ftl" id="go_to_top"><i class="fa fa-angle-up"></i></button>
		<!--scripts include-->
		{!! Html::script('assets/js/jquery-2.1.0.min.js') !!}
		{!! Html::script('assets/js/retina.js') !!}
		{!! Html::script('assets/js/jquery.flexslider-min.js') !!}
		{!! Html::script('assets/js/waypoints.min.js') !!}
		{!! Html::script('assets/js/jquery.isotope.min.js') !!}
		{!! Html::script('assets/js/owl.carousel.min.js') !!}
		{!! Html::script('assets/js/jquery.tweet.min.js') !!}
		{!! Html::script('assets/js/jquery.custom-scrollbar.js') !!}
		{!! Html::script('assets/js/scripts.js') !!}

		{!! Html::script('assets/js/jquery-ui.min.js') !!}
		{!! Html::script('assets/js/jquery-migrate-1.2.1.min.js') !!}
		{!! Html::script('assets/js/elevatezoom.min.js') !!}
		{!! Html::script('assets/js/jquery.fancybox-1.3.4.js') !!}

		<script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-5306f8f674bfda4c"></script>
		<script>
	    $(document).ready(function(){
	      $('#notif').delay(3000).fadeOut(400);
	    });
	  </script>

	</body>
</html>
