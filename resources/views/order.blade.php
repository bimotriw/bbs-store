@extends('master')

@section('content')

<div class="page_content_offset" id="container_content">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9">
        <h2 class="tt_uppercase color_dark m_bottom_30">Orders List</h2>
        <div class="alert_box r_corners info">
					<i class="fa fa-info-circle"></i><p>Click <span class="scheme_color">Order Number</span> For Verify Your Purchase.</p>
				</div>
        <br>
        <!--orders list table-->
        <table class="table_type_3 responsive_table full_width r_corners wrapper shadow bg_light_color_1 m_bottom_30 t_align_l">
          <thead>
            <tr class="f_size_large">
              <!--titles for td-->
              <th>Order Number</th>
              <th>Order Date</th>
              <th>Order Status</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            @if (Auth::user()->buyer->orders)
              @foreach (Auth::user()->buyer->orders as $order)
                <tr>
                  <!--order number-->
                  <td data-title="Order Number"><a href="order_details.html" class="color_dark">{!!Html::linkAction('OrderController@actionView', $order->code, array($order->id))!!}</a></td>
                  <!--order date-->
                  <td data-title="Order Date">{{date_format($order->created_at, 'd F Y')}}</td>
                  <!--order status-->
                  <td data-title="Order Status">
                    @if ($order->status == '2')
                      <a>Unpaid</a>
                    @elseif ($order->status == '0')
                      Verification
                    @elseif ($order->status == '1')
                      Confirmed by admin
                    @elseif ($order->status == '3')
                      Invalid
                    @endif
                  </td>
                  <!--quanity-->
                  <?php $cost = $order->shipping_cost + $order->total +$order->id; ?>
                  <?php $cost = number_format ($cost, 0, ',', '.'); ?>
                  <td data-title="Total"><span class="f_size_large fw_medium scheme_color">Rp. {{$cost}},-</span></td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>

@stop
