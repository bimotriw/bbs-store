@extends('master')

@section('content')

<div class="page_content_offset">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
        <div class="clearfix m_bottom_30 t_xs_align_c">
      <div class="photoframe type_2 shadow r_corners f_left f_sm_none d_xs_inline_b product_single_preview relative m_right_30 m_bottom_5 m_sm_bottom_20 m_xs_right_0 w_mxs_full">
        <div class="relative d_inline_b m_bottom_10 qv_preview d_xs_block">
          @foreach ($product->product_images as $image)
            @if ($image->type == 'main')
              <img id="zoom_image" src="{{url($image->url)}}" data-zoom-image="{{url($image->url)}}" class="tr_all_hover main_product_image" alt="">
            @endif
          @endforeach
          </a>
        </div>
        <!--carousel-->
        <div class="relative qv_carousel_wrap">
          <button class="button_type_11 bg_light_color_1 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_single_prev">
            <i class="fa fa-angle-left "></i>
          </button>
          <ul class="qv_carousel_single d_inline_middle">
            @foreach ($product->product_images as $image)
                <a href="#" data-image="{{url($image->url)}}" data-zoom-image="{{url($image->url)}}"><img class="other_product_image" src="{{url($image->url)}}" alt=""></a>
            @endforeach
            </ul>
          <button class="button_type_11 bg_light_color_1 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_single_next">
            <i class="fa fa-angle-right "></i>
          </button>
        </div>
      </div>
      <div class="p_top_10 t_xs_align_l">
        <!--description-->
        <h2 class="color_dark fw_medium m_bottom_10">{{$product->name}}</h2>
        <hr class="m_bottom_10 divider_type_3">
        <table class="description_table m_bottom_10">
          <tr>
            <td>Product Code:</td>
            <td>{{$product->code}}</td>
          </tr>
          <tr>
            <td>Category:</td>
            <td>{{$product->category->name}}</td>
          </tr>
        </table>
        <h5 class="fw_medium m_bottom_10">Product Weight</h5>
        <table class="description_table m_bottom_5">
          <tr>
            <td>Product Weight:</td>
            <td>{{$product->weight}} gram</td>
          </tr>
        </table>
        <hr class="divider_type_3 m_bottom_10">
        <p class="m_bottom_10">{{$string = substr($product->description,0,200).'...'}}</p>
        <hr class="divider_type_3 m_bottom_15">
        <div class="m_bottom_15">
          <?php $cost = number_format ($product->price, 0, ',', '.'); ?>
          <span class="v_align_b f_size_big m_left_5 scheme_color fw_medium">Rp. {{$cost}},-</span>
        </div>
        {!!Form::open(array('action' => 'WishlistController@actionStore', 'class' => 'validator-form', 'id' => 'formQty'))!!}
          <table class="description_table type_2 m_bottom_15">
            <tr>
              <td class="v_align_m">Quantity:</td>
              <td class="v_align_m">
                <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                  <button type="button" class="bg_tr d_block f_left" data-direction="down">-</button>
                  <input type="text" name="quantity" readonly value="1" class="f_left">
                  <button type="button" class="bg_tr d_block f_left" data-direction="up">+</button>
                </div>
              </td>
            </tr>
          </table>
          <div class="d_ib_offset_0 m_bottom_20">
            <script>
              function submitForm(action){
                  document.getElementById('formQty').action = action;
                  document.getElementById('formQty').submit();
              }
            </script>
            <input type="hidden" name="id_prod" value="{{$product->id}}">
            @if (Auth::check())
              <button onclick="submitForm('store/order')" class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover d_inline_b f_size_large">Add to Cart</button>
              <button onclick="submitForm('store/wishlist')" class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0"><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span><i class="fa fa-heart-o f_size_big"></i></button>
            @else
            <button type="button" data-popup="#login_popup" class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover d_inline_b f_size_large">Add to Cart</button>
            <button type="button" data-popup="#login_popup" class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0"><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span><i class="fa fa-heart-o f_size_big"></i></button>
            @endif
          </div>
        {!!Form::close()!!}
      </div>
    </div>
    <!--tabs-->
    <div class="tabs m_bottom_45">
      <!--tabs navigation-->
      <nav>
        <ul class="tabs_nav horizontal_list clearfix">
          <li class="f_xs_none"><a href="#tab-1" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block">Description</a></li>
          <li class="f_xs_none"><a href="#tab-2" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block">Specifications</a></li>
          <li class="f_xs_none"><a href="#tab-3" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block">About Seller</a></li>
        </ul>
      </nav>
      <section class="tabs_content shadow r_corners">
        <div id="tab-1">
          <p class="m_bottom_10">{{$product->description}}</p>
        </div>
        <div id="tab-2">
          <p class="m_bottom_10">{{$product->specification}}</p>
        </div>
        <div id="tab-3">
          <div class="row clearfix">
						<div class="col-lg-8 col-md-8 col-sm-8">
							<h3 class="fw_medium m_bottom_15">{{$product->seller->company}}</h3> ({{$product->seller->name}}) - <a>({{$product->seller->phone}})</a>
							<hr class="m_bottom_15">
							<!--review-->
							<article>
								<div class="clearfix m_bottom_10">
									<p class="f_size_medium f_left f_mxs_none m_mxs_bottom_5">Comapany Address : {{$product->seller->address}}</p>
								</div>
                <div class="clearfix m_bottom_10">
									<p class="f_size_medium f_left f_mxs_none m_mxs_bottom_5">Comapany Type : {{$product->seller->company_type}}</p>
								</div>
                <div class="clearfix m_bottom_10">
									<p class="f_size_medium f_left f_mxs_none m_mxs_bottom_5">Year Established : <a>{{$product->seller->year_established}}</a></p>
								</div>
                <hr class="m_bottom_15">
                <h5 class="m_bottom_15">Description</h5>
								<p class="m_bottom_15">{{$product->seller->description}}</p>
							</article>
							<hr class="m_bottom_15">
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
              @foreach ($product->seller->seller_images as $image)
                @if ($image->type == 'main')
                  {!!Html::image($image->url, '...')!!}
                  <hr class="m_bottom_15">
                  <p class="m_bottom_15">{{$image->description}}</p>
                @endif
              @endforeach
						</div>
					</div>
        </div>
      </section>
    </div>


    </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>

@stop
