@extends('slr/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-bar-chart"></i> Report <small>Orders</small></h1></div>

<?php
  if(count($orders)) {
    $total_income_complete = 0;
    $total_income_hold = 0;
    foreach ($orders as $order) {
      $datas[$order->id]= array(
        'id' => $order->id,
        'created_at' => $order->created_at,
        'code' => $order->code,
        'name' => $order->name,
        'status' => $order->status,
        'created_at' => $order->created_at
      );
      if ($order->status == 1)
        $total_income_complete = $total_income_complete + $order->sub_total;
      elseif ($order->status == 0 || $order->status == 2)
        $total_income_hold = $total_income_hold + $order->sub_total;
    }
  } else
    $datas = array();
?>

<?php
  for ($x = 0; $x <= 12; $x++) {
    $val[$x] = array (
      'value1' => 0,
      'value2' => 0,
      'value3' => 0,
      'month' => ""
    );
  }
?>

@foreach ($datas as $data)
  @if ($data['status'] == "2")
    <?php
      $dateValue = $data['created_at'];
      $time = strtotime($dateValue);
      $month = date("m",$time);
      $month = $month + 0;
      $val[$month]['value1'] = $val[$month]['value1'] + 1;
      $val[$month]['month'] = date("F",$time);
    ?>
  @endif
  @if ($data['status'] == "0")
    <?php
      $dateValue = $data['created_at'];
      $time = strtotime($dateValue);
      $month = date("m",$time);
      $month = $month + 0;
      $val[$month]['value2'] = $val[$month]['value2'] + 1;
      $val[$month]['month'] = date("F",$time);
    ?>
  @endif
  @if ($data['status'] == "1")
    <?php
      $dateValue = $data['created_at'];
      $time = strtotime($dateValue);
      $month = date("m",$time);
      $month = $month + 0;
      $val[$month]['value3'] = $val[$month]['value3'] + 1;
      $val[$month]['month'] = date("F",$time);
    ?>
  @endif
@endforeach

<?php
  $total_income_hold  = number_format ($total_income_hold, 0, ',', '.');
  $total_income_complete = number_format ($total_income_complete, 0, ',', '.');
?>

<div class="row">
  <div class="col-md-6 col-sm-6">
    <div class="panel panel-default clearfix dashboard-stats rounded">
      <span id="dashboard-stats-sparkline3" class="sparkline transit"></span>
      <i class="fa fa-money bg-warning transit stats-icon"></i>
      <h3 class="transit">Rp. {{$total_income_hold}},-</h3>
      <p class="text-muted transit">On Hold Payment</p>
    </div>
  </div>
  <div class="col-md-6 col-sm-6">
    <div class="panel panel-default clearfix dashboard-stats rounded">
      <span id="dashboard-stats-sparkline4" class="sparkline transit"></span>
      <i class="fa fa-money bg-success transit stats-icon"></i>
      <h3 class="transit">Rp. {{$total_income_complete}},-</h3>
      <p class="text-muted transit">Completed Payment</p>
    </div>
  </div>
</div>

<div class="row">
  <div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt"></i> Reports Order</div>
    <div class="panel-body">
      <div id="line-chartOrder" style="height:250px;"></div>
    </div>
  </div>
</div>

@stop
