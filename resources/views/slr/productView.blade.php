@extends('slr/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-laptop"></i> Product Details</h1></div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Product Details</div>
      <div class="panel-body">
        {!!Form::open(array('action' => 'Slr\ProductSlrController@actionUpdate', 'class' => 'validator-form', 'files' => true))!!}
          {!!Form::hidden('id', "$product->id", array('class' => 'form-control', 'id' => 'id'))!!}
          <div class="form-group">
            {!!Form::label('code', 'Code')!!}
            {!!Form::text('name', "$product->code", array('class' => 'form-control', 'disabled'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('name', 'Product Name')!!}
            {!!Form::text('name', "$product->name", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('category_product', 'Category')!!}
            <select name="category_product" class="form-control chosen-select" data-placeholder="Choose Category" required>
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->id == $product->category_id)
                    <option value="{{$category->id}}" selected>{{$category->name}}</option>
                  @else
                    <option value="{{$category->id}}">{{$category->name}}</option>
                  @endif
                @endforeach
              @endif
            </select>
          </div>
          <div class="form-group">
            {!!Form::label('price', 'Price')!!}
            <div class="input-group">
              <span class="input-group-addon">Rp.</span>
              {!!Form::text('price', "$product->price", array('class' => 'form-control'))!!}
              <span class="input-group-addon">,00</span>
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('weight', 'Weight')!!}
            <div class="input-group">
              {!!Form::text('weight', "$product->weight", array('class' => 'form-control'))!!}
              <span class="input-group-addon">gram</span>
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Description')!!}
            {!!Form::textarea('description', "$product->description", ['class' => 'form-control'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('specification', 'Specification')!!}
            {!!Form::textarea('specification', "$product->specification", ['class' => 'form-control'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('additional_document', 'Additional Document')!!}
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add file...</span>
                <input id="file2" onChange="myFunctionProductFile()" type="file" name="file2">
            </span>
            <div id="fileText2"><i class='fa fa-arrow-right'></i> {!!Html::link($product->additional_document, $product->additional_document_resize)!!}</div>
          </div>
          <button type="submit" class="btn btn-purple">Edit</button>
        {!!Form::close()!!}
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Product Images</div>
      <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
          <thead>
            <tr>
              <th>Images</th>
              <th>Type</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if (count($product->product_images))
              @foreach ($product->product_images as $image)
                <tr>
                  <td>{!!Html::image($image->url, 'a picture', array('class' => 'thumb'))!!}</td>
                  <td>
                    {!!$image->type=='main'  ?
                      Form::button('main', array('class' => 'btn btn-purple btn-sm')) :
                      Form::button('other', array('class' => 'btn btn-success btn-sm'))
                    !!}
                  </td>
                  <td>
                    {!!Form::button('', array('class' => 'btn btn-purple fa fa-edit btnModalProductImage tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Edit', 'data-toggle' => 'modal', 'data-target' => '#modal-form', 'data-id' => $image->id, 'data-type' => $image->type, 'data-url' => $image->url, 'data-description' => $image->description, 'data-par' => $product->id))!!}
                    @if ($image->type != 'main')
                      {!!Html::linkAction('Slr\ProductSlrController@actionSwapProductImage', '', array($product->id, $image->id), array('class' => 'btn btn-warning fa fa-refresh tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Change To Be Main Image'))!!}
                      {!!Form::button('', array('class' => 'btn btn-danger fa fa-trash btnModalProductImageDelete tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Delete', 'data-toggle' => 'modal', 'data-target' => '#modal-delete-product-image', 'data-id' => $image->id, 'data-id_prod' => $product->id))!!}
                    @endif
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
          {!!Form::button('', array('class' => 'btn btn-success glyphicon glyphicon-plus btnModalProductImageNew tooltip-btn', 'data-placement' => 'right', 'data-original-title' => 'Add New', 'data-toggle' => 'modal', 'data-target' => '#modal-form1', 'data-id_prod' => $product->id))!!}
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal With Form -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!!Form::open(array('action' => 'Slr\ProductSlrController@actionUpdateProductImage', 'class' => 'validator-form', 'files' => true))!!}
        {!!Form::hidden('id', "", array('class' => 'form-control', 'id' => 'id_edit'))!!}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt"></i> <span id="title_edit"></span>  <span id="type_edit"></span></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {!!Form::label('image', 'Image')!!}
            <span class="btn btn-success fileinput-button">
              <i class="glyphicon glyphicon-plus"></i>
              <span>Add file...</span>
              <input id="file" onChange="myFunction()" type="file" name="file">
            </span>
            <i class="fa fa-arrow-right"></i> <span id="fileText"></span>
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Description')!!}
            {!!Form::textarea('description', "", ['class' => 'form-control', 'id' => 'description_edit'])!!}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

<!-- Modal With Form -->
<div class="modal fade" id="modal-form1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!!Form::open(array('action' => 'Slr\ProductSlrController@actionStoreProductImage', 'class' => 'validator-form', 'files' => true))!!}
        {!!Form::hidden('id_prod', "", array('class' => 'form-control', 'id' => 'id_prod_edit'))!!}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt"></i> <span id="title_edit1"></span></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {!!Form::label('image', 'Image')!!}
            <span class="btn btn-success fileinput-button">
              <i class="glyphicon glyphicon-plus"></i>
              <span>Add file...</span>
              <input id="file1" onChange="myFunction()" type="file" name="file1" required="required">
            </span>
            <span id="fileText1"></span>
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Description')!!}
            {!!Form::textarea('description', "", ['class' => 'form-control', 'id' => 'description_edit1'])!!}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

<!-- Modal With Form Delete -->
<div class="modal fade" id="modal-delete-product-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this image?</p>
      </div>
      <div class="modal-footer" id="modal-footer"></div>
    </div>
  </div>
</div>

@stop
