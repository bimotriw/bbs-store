@extends('slr/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-laptop"></i> Products List</h1></div>

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-list-alt"></i> Products List</div>
  <div class="panel-body">

    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
      <thead>
        <tr>
          <th>Code</th>
          <th>Image</th>
          <th>Name</th>
          <th>Weight (gr)</th>
          <th>Category</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @if (count(Auth::user()->seller->products))
          @foreach (Auth::user()->seller->products as $product)
            @if ($product->deleted == '0')
              <tr>
                <td>{{$product->code}}</td>
                <td>
                  @foreach ($product->product_images as $image)
                    @if ($image->type == "main")
                      {!!Html::image($image->url, 'a picture', array('class' => 'thumb'))!!}
                    @endif
                  @endforeach
                </td>
                <td>{{$product->name}}</td>
                <?php $weight = number_format ($product->weight, 0, '', '.'); ?>
                <td>{{$weight}}</td>
                <td>{{$product->category->name}}</td>
                <td>
                  {!!Html::linkAction('Slr\ProductSlrController@actionView', '', array($product->id), array('class' => 'btn btn-purple fa fa-edit tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Edit'))!!}
                  {!!Form::button('', array('class' => 'btn btn-danger fa fa-trash btnModalProductDelete tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Delete', 'data-toggle' => 'modal', 'data-target' => '#modal-delete-product', 'data-id' => $product->id))!!}
                </td>
              </tr>
            @endif
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>

<!-- Modal With Form Delete -->
<div class="modal fade" id="modal-delete-product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this product?</p>
      </div>
      <div class="modal-footer" id="modal-footer"></div>
    </div>
  </div>
</div>

@stop
