@extends('slr/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-laptop"></i> New Product</h1></div>

<div class="row">
  {!!Form::open(array('action' => 'Slr\ProductSlrController@actionStore', 'class' => 'validator-form', 'files' => true))!!}
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Product Details</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('name', 'Product Name')!!}
            {!!Form::text('name', "", array('class' => 'form-control', 'required'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('category_product', 'Category')!!}
            <select name="category_product" class="form-control chosen-select" data-placeholder="Choose Category" required>
              <option value="" selected></option>
              @if (count($categories))
                @foreach ($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              @endif
            </select>
          </div>
          <div class="form-group">
            {!!Form::label('price', 'Price')!!}
            <div class="input-group">
              <span class="input-group-addon">Rp.</span>
              {!!Form::text('price', "", array('class' => 'form-control', 'required'))!!}
              <span class="input-group-addon">,-</span>
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('weight', 'Weight')!!}
            <div class="input-group">
              {!!Form::text('weight', "", array('class' => 'form-control'))!!}
              <span class="input-group-addon">gram</span>
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Description')!!}
            {!!Form::textarea('description', "", ['class' => 'form-control'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('specification', 'Specification')!!}
            {!!Form::textarea('specification', "", ['class' => 'form-control'])!!}
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Additional Document</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('additional_document', 'Additional Document')!!}
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add file...</span>
                <input id="prod_file" onChange="myFunctionProductFile1()" type="file" name="prod_file">
            </span>
            <div id="prod_fileText"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Main Image</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('image', 'Image')!!}
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Add file...</span>
                <input id="main_image" onChange="myFunctionMainImage()" type="file" name="main_image">
            </span>
            <div id="main_imageText"></div>
          </div>
          <div class="form-group">
            {!!Form::label('image_description', 'Image Description')!!}
            {!!Form::textarea('image_description', "", ['class' => 'form-control'])!!}
          </div>
          <button type="submit" class="btn btn-primary">Add New</button>
        </div>
      </div>
    </div>
  {!!Form::close()!!}
</div>

@stop
