@extends('slr/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-question-circle"></i> Help</h1></div>

<div class="row">
  <ul role="tablist" class="nav nav-tabs" id="myTab">
    <li class="active"><a data-toggle="tab" role="tab" href="#profile">Profile</a></li>
    <li><a data-toggle="tab" role="tab" href="#products">Products</a></li>
    <li><a data-toggle="tab" role="tab" href="#orders">Orders</a></li>
  </ul>
  <div class="tab-content" id="myTabContent">

    <div id="profile" class="tab-pane tabs-up fade in active panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            {!!Html::image('uploads/contents_help/slr/slrProfile.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-6">
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Profile Details</h3></div>
            <dl>
              <dt class="red">Name</dt>
              <dd>Nama Pemilik Toko <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Company</dt>
              <dd>Nama Toko <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Address</dt>
              <dd>Alamat Toko <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Phone</dt>
              <dd>Telp Toko <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Description</dt>
              <dd>Deskripsi Singkat Toko <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Company Type</dt>
              <dd>Bidang Produk Yang Dijual Toko <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Year Established</dt>
              <dd>Tahun Berdirinya Toko <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            {!!Html::image('uploads/contents_help/slr/slrProfile1.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-6">
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Account Seller</h3></div>
            <dl>
              <dt class="red">Email</dt>
              <dd>Email Untuk Login.</dd>
            </dl>
            <dl>
              <dt class="red">Password</dt>
              <dd>Password Untuk Login.</dd>
            </dl>
            <dl>
              <dt class="red">Retype-Password</dt>
              <dd>Retype-Password Untuk Memastikan Password Yang Akan Anda Rubah Sesuai <small class="red"><i class="fa fa-arrow-right"></i> harus diisi jika anda memasukkan password baru & harus diisikan sama dengan Password.</small></dd>
            </dl>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            {!!Html::image('uploads/contents_help/slr/slrProfile2.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-6">
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Account Numbers</h3></div>
            <dl>
              <dt class="red">BCA</dt>
              <dd>No. Rek BCA Toko / Pemilik <small class="red"><i class="fa fa-arrow-right"></i> harus diisi untuk proses transaksi.</small></dd>
            </dl>
            <dl>
              <dt class="red">BRI</dt>
              <dd>No. Rek BRI Toko / Pemilik <small class="red"><i class="fa fa-arrow-right"></i> harus diisi untuk proses transaksi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Mandiri</dt>
              <dd>No. Rek Mandiri Toko / Pemilik <small class="red"><i class="fa fa-arrow-right"></i> harus diisi untuk proses transaksi.</small></dd>
            </dl>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            {!!Html::image('uploads/contents_help/slr/slrProfile3.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-7">
            {!!Html::image('uploads/contents_help/slr/slrProfile4.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-5">
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Company Images</h3></div>
            <dl>
              <dt class="red">Add New {!!Html::image('uploads/contents_help/slr/addNew.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Menambah Gambar Toko <small class="red"><i class="fa fa-arrow-right"></i> akan muncul form <i class="fa fa-list-alt"></i>Add Image.</small></dd>
            </dl>
            <dl>
              <dt class="red">Tipe <i class="fa fa-arrow-right"></i> main {!!Html::image('uploads/contents_help/slr/main.png', '...', array('class' => ''))!!}</dt>
              <dd>Tipe Dari Gambar Toko <small class="red"><i class="fa fa-arrow-right"></i> jika tipe gambar 'main' maka gambar akan dijadikan gambar utama dari toko.</small></dd>
            </dl>
            <dl>
              <dt class="red">Tipe <i class="fa fa-arrow-right"></i> other {!!Html::image('uploads/contents_help/slr/other.png', '...', array('class' => ''))!!}</dt>
              <dd>Tipe Dari Gambar Toko.</dd>
            </dl>
            <dl>
              <dt class="red">Edit {!!Html::image('uploads/contents_help/slr/edit.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Mengubah Gambar Toko <small class="red"><i class="fa fa-arrow-right"></i> akan muncul form <i class="fa fa-list-alt"></i>Edit Image dengan tampilan seperti <i class="fa fa-list-alt"></i>Add Image.</small></dd>
            </dl>
            <dl>
              <dt class="red">Swap {!!Html::image('uploads/contents_help/slr/swap.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Mengubah Tipe Gambar Toko Dari 'other' Menjadi 'main'.</dd>
            </dl>
            <dl>
              <dt class="red">Delete {!!Html::image('uploads/contents_help/slr/delete.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Menghapus Gambar Toko.</dd>
            </dl>
            <hr>
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Add Image</h3></div>
            <dl>
              <dt class="red">Add File {!!Html::image('uploads/contents_help/slr/addFile.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Memasukkan Gambar.</dd>
            </dl>
            <dl>
              <dt class="red">Image Description</dt>
              <dd>Deskripsi Dari Gambar.</small></dd>
            </dl>
          </div>
        </div>
      </div>
    </div>

    <div id="products" class="tab-pane tabs-up fade panel panel-default">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-6">
            {!!Html::image('uploads/contents_help/slr/slrProduct.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-6">
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Product Details</h3></div>
            <dl>
              <dt class="red">Product Name</dt>
              <dd>Nama Produk <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Category</dt>
              <dd>Kategori Produk <small class="red"><i class="fa fa-arrow-right"></i> kategori sudah disediakan oleh Admin <i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Price</dt>
              <dd>@Harga Produk <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Weight</dt>
              <dd>@Berat Produk <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Description</dt>
              <dd>Deskripsi Singkat Produk <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Specification</dt>
              <dd>Spesifikasi Singkat Produk.</dd>
            </dl>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            {!!Html::image('uploads/contents_help/slr/slrProduct1.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-6">
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Additional Document</h3></div>
            <dl>
              <dt class="red">Add File {!!Html::image('uploads/contents_help/slr/addFile.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Memasukkan File <small class="red"><i class="fa fa-arrow-right"></i> format file harus : pdf / ppt / pptx / doc / docx.</small></dd>
            </dl>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            {!!Html::image('uploads/contents_help/slr/slrProduct2.png', '...', array('class' => ''))!!}
          </div>
          <div class="col-md-6">
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Main Image</h3></div>
            <dl>
              <dt class="red">Add File {!!Html::image('uploads/contents_help/slr/addFile.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Memasukkan Gambar 'main' Produk <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">Image Description</dt>
              <dd>Deskripsi Dari Gambar 'main' Produk.</dd>
            </dl>
          </div>
        </div>
      </div>
    </div>

    <div id="orders" class="tab-pane tabs-up fade panel panel-default">
      <div class="panel-body">
        <p>Pada Orders terdapat 4 status dalam transaksi yaitu :</p>
        <div class="row">
          <div class="col-md-5">
            <dl>
              <dt class="red">{!!Html::image('uploads/contents_help/slr/ordered.png', '...', array('class' => ''))!!}</dt>
              <dd><i class="fa fa-arrow-right"></i> Pada status ini terdapat pembeli yang memesan produk yang dijual oleh penjual. Untuk mengkonfirmasi bahwa barang yang diorder sudah dikirim, penjual harus mengkonfirmasi {!!Html::image('uploads/contents_help/slr/confirmationOrder.png', '...', array('class' => ''))!!} pada form <span class="red">Sales Invoice</span>.</dd>
              <dd class="red"><i class="fa fa-arrow-right"></i> Jika dalam verifikasi anda mengisi tidak sesuai dengan yang seharusnya maka order akan dibatalkan dan order ber-status {!!Html::image('uploads/contents_help/slr/invalid.png', '...', array('class' => ''))!!}.</dd>
            </dl>
          </div>
          <div class="col-md-7">
            <dl>
              <dt class="red">{!!Html::image('uploads/contents_help/slr/confirmationOrder1.png', '...', array('class' => ''))!!}</dt>
            </dl>
            <div class="page-header"><h3><i class="fa fa-list-alt"></i> Main Image</h3></div>
            <dl>
              <dt class="red">Code Sales Invoice</dt>
              <dd>Code Sales Invoice Yang Akan Diproses <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
            <dl>
              <dt class="red">No. Resi</dt>
              <dd>no. Resi Pengiriman <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></small></dd>
            </dl>
            <dl>
              <dt class="red">Add File {!!Html::image('uploads/contents_help/slr/addFile.png', '...', array('class' => ''))!!}</dt>
              <dd>Tombol Untuk Memasukkan Gambar Foto Verifikasi Dari Nota Pengiriman Barang <small class="red"><i class="fa fa-arrow-right"></i> harus diisi.</small></dd>
            </dl>
          </div>
          <div class="col-md-12">
            <dl>
              <dt class="red">{!!Html::image('uploads/contents_help/slr/processed.png', '...', array('class' => ''))!!}</dt>
              <dd><i class="fa fa-arrow-right"></i> Pada status ini, <span class="red">admin</span> akan melakukan pengecekan data.</dd>
            </dl>
          </div>
          <div class="col-md-12">
            <dl>
              <dt class="red">{!!Html::image('uploads/contents_help/slr/paid.png', '...', array('class' => ''))!!}</dt>
              <dd><i class="fa fa-arrow-right"></i> Pada status ini, <span class="red">admin</span> telah melakukan pengecekan data dan melakukan transaksi ke salah satu nomer rekening penjual.</dd>
            </dl>
          </div>
          <div class="col-md-12">
            <dl>
              <dt class="red">{!!Html::image('uploads/contents_help/slr/invalid.png', '...', array('class' => ''))!!}</dt>
              <dd><i class="fa fa-arrow-right"></i> Pada status ini, <span class="red">admin</span> telah melakukan pengecekan data dan menemukan ketidak sesuaian data.</dd>
              <dd class="red"><i class="fa fa-arrow-right"></i> Hubungi Admin untuk melakukan komplain dalam transaksi invalid.</dd>
            </dl>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

@stop
