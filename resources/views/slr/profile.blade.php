@extends('slr/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-user"></i> My Profile</h1></div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Profile Details</div>
      <div class="panel-body">
        {!!Form::open(array('action' => 'Slr\ProfileSlrController@actionUpdateProfile', 'class' => 'validator-form'))!!}
          <div class="form-group">
            {!!Form::label('name', 'Name')!!}
            {!!Form::text('name', Auth::user()->seller->name, array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('company_sel', 'Company')!!}
            {!!Form::text('company_sel', Auth::user()->seller->company, array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('address', 'Address')!!}
            {!!Form::textarea('address', Auth::user()->seller->address, ['class' => 'form-control', 'size' => '30x3'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('phone', 'Phone')!!}
            {!!Form::text('phone', Auth::user()->seller->phone, array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Description')!!}
            {!!Form::textarea('description', Auth::user()->seller->description, ['class' => 'form-control'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('company_type', 'Company Type')!!}
            {!!Form::text('company_type', Auth::user()->seller->company_type, array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('year_established', 'Year Established')!!}
            {!!Form::text('year_established', Auth::user()->seller->year_established, array('class' => 'form-control'))!!}
          </div>
          <button type="submit" class="btn btn-purple">Edit</button>
        {!!Form::close()!!}
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Account Seller</div>
      <div class="panel-body">
        <div class="form-group">
          {!!Form::label('email', 'Email')!!}
          {!!Form::text('email', Auth::user()->email, array('class' => 'form-control', 'disabled'))!!}
        </div>
        <button type="submit" class="btn btn-purple" data-toggle="modal" data-target="#modal-change-password">Edit Password</button>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Account Numbers</div>
      <div class="panel-body">
        {!!Form::open(array('action' => 'Slr\ProfileSlrController@actionUpdateAccountNumber', 'class' => 'validator-form'))!!}
          @foreach (Auth::user()->seller->accounts as $account)
            <div class="form-group">
              {!!Form::label($account->account_type, $account->account_type)!!}
              {!!Form::text($account->account_type, $account->account_number, array('class' => 'form-control chosen-rtl'))!!}
            </div>
          @endforeach
          <button type="submit" class="btn btn-purple">Edit</button>
        {!!Form::close()!!}
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Company Images</div>
      <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
          <thead>
            <tr>
              <th>Images</th>
              <th>Type</th>
              <th>Description</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if (count(Auth::user()->seller->seller_images))
              @foreach (Auth::user()->seller->seller_images as $image)
                <tr>
                  <td>{!!Html::image($image->url, 'a picture', array('class' => 'thumb'))!!}</td>
                  <td>
                    {!!$image->type == 'main'  ?
                      Form::button('main', array('class' => 'btn btn-purple btn-sm')) :
                      Form::button('other', array('class' => 'btn btn-success btn-sm'))
                    !!}
                  </td>
                  <td>{{$image->description}}</td>
                  <td>
                    {!!Form::button('', array('class' => 'btn btn-purple fa fa-edit btnModalSellerImage tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Edit', 'data-toggle' => 'modal', 'data-target' => '#modal-form', 'data-id' => $image->id, 'data-type' => $image->type, 'data-url' => $image->url, 'data-description' => $image->description, 'data-par' => Auth::user()->seller->id))!!}
                    @if ($image->type != 'main')
                      {!!Html::linkAction('Slr\ProfileSlrController@actionSwapSellerImage', '', array($image->id), array('class' => 'btn btn-warning fa fa-refresh tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Change To Be Main Image'))!!}
                      {!!Form::button('', array('class' => 'btn btn-danger fa fa-trash btnModalSellerImageDelete tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Delete', 'data-toggle' => 'modal', 'data-target' => '#modal-delete-company-image', 'data-id' => $image->id))!!}
                    @endif
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
          {!!Form::button('', array('class' => 'btn btn-success glyphicon glyphicon-plus btnModalSellerImageNew tooltip-btn', 'data-placement' => 'right', 'data-original-title' => 'Add New', 'data-toggle' => 'modal', 'data-target' => '#modal-form1'))!!}
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal With Form -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!!Form::open(array('action' => 'Slr\ProfileSlrController@actionUpdateSellerImage', 'class' => 'validator-form', 'files' => true))!!}
        {!!Form::hidden('id', "", array('class' => 'form-control', 'id' => 'id_edit'))!!}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt"></i> <span id="title_edit"></span>  <span id="type_edit"></span></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {!!Form::label('image', 'Image')!!}
            <span class="btn btn-success fileinput-button">
              <i class="glyphicon glyphicon-plus"></i>
              <span>Add file...</span>
              <input id="file" onChange="myFunction()" type="file" name="file">
            </span>
            <i class="fa fa-arrow-right"></i> <span id="fileText"></span>
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Image Description')!!}
            {!!Form::textarea('description', "", ['class' => 'form-control', 'id' => 'description_edit'])!!}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

<!-- Modal With Form -->
<div class="modal fade" id="modal-form1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!!Form::open(array('action' => 'Slr\ProfileSlrController@actionStoreSellerImage', 'class' => 'validator-form', 'files' => true))!!}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt"></i> <span id="title_edit1"></span></h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {!!Form::label('image', 'Image')!!}
            <span class="btn btn-success fileinput-button">
              <i class="glyphicon glyphicon-plus"></i>
              <span>Add file...</span>
              <input id="file1" onChange="myFunction()" type="file" name="file1" required="required">
            </span>
            <span id="fileText1"></span>
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Image Description')!!}
            {!!Form::textarea('description', "", ['class' => 'form-control', 'id' => 'description_edit1'])!!}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

<!-- Modal With Form Change Password -->
<div class="modal fade" id="modal-change-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to change your password?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-purple confirmChangePassword" data-toggle="modal" data-target="#modal-change-password1" data-dismiss="modal">Edit</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal With Form Change Password -->
<div class="modal fade" id="modal-change-password1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt"></i> Edit Password</h4>
      </div>
      {!!Form::open(array('action' => 'Slr\ProfileSlrController@actionUpdatePassword', 'class' => 'validator-form'))!!}
        <div class="modal-body">
          <div class="form-group">
            {!!Form::label('password', 'Password')!!}
            {!!Form::password('password', array('class' => 'form-control', 'id' => 'password'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('confirmPassword', 'Retype-Password')!!}
            {!!Form::password('confirmPassword', array('class' => 'form-control'))!!}
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-purple">Edit</button>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

<!-- Modal With Form Delete -->
<div class="modal fade" id="modal-delete-company-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this image?</p>
      </div>
      <div class="modal-footer" id="modal-footer"></div>
    </div>
  </div>
</div>

@stop
