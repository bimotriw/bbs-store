@extends('slr/master')

@section('content')
<div class="page-header">
  <h1>
    <i class="fa fa-shopping-cart"></i> Orders List
    @if (Request::is('sellerbbs/orders/ordered'))
      {!!Form::button('Ordered', array('class' => 'btn btn-warning btn-sm'))!!}
    @elseif (Request::is('sellerbbs/orders/processed'))
      {!!Form::button('Processed', array('class' => 'btn btn-purple btn-sm'))!!}
    @elseif (Request::is('sellerbbs/orders/paid'))
      {!!Form::button('Paid', array('class' => 'btn btn-success btn-sm'))!!}
    @elseif (Request::is('sellerbbs/orders/invalid'))
      {!!Form::button('Invalid', array('class' => 'btn btn-danger btn-sm'))!!}
    @endif
  </h1>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-list-alt"></i> Orders List</div>
  <div class="panel-body">
    <?php
      if(count($orders)) {
        foreach ($orders as $order) {
          $datas[$order->id]= array(
            'id' => $order->id,
            'created_at' => $order->created_at,
            'code' => $order->code,
            'name' => $order->name,
            'status' => $order->status,
            'updated_at' => $order->updated_at
          );
        }
      } else
        $datas = array();
    ?>
    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
      <thead>
        <tr>
          <th>Date</th>
          <th>CODE</th>
          <th>Buyer Name</th>
          <th>Status</th>
          <th>Updated at</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @if (count($datas))
          @foreach ($datas as $order)
            @if ($order['status'] != "3")
              <tr>
                <td>{{$order['created_at']}}</td>
                <td>{{$order['code']}}</td>
                <td>{{$order['name']}}</td>
                <td>
                  @if ($order['status'] == "0")
                    {!!Form::button('Processed', array('class' => 'btn btn-purple btn-sm'))!!}
                  @elseif ($order['status'] == "1")
                    {!!Form::button('Paid', array('class' => 'btn btn-success btn-sm'))!!}
                  @elseif ($order['status'] == "2")
                    {!!Form::button('Ordered', array('class' => 'btn btn-warning btn-sm'))!!}
                  @elseif ($order['status'] == "3")
                    {!!Form::button('-', array('class' => 'btn btn-default btn-sm'))!!}
                  @elseif ($order['status'] == "4")
                    {!!Form::button('Invalid', array('class' => 'btn btn-danger btn-sm'))!!}
                  @elseif ($order['status'] == "5")
                    {!!Form::button('Invalid Purchase', array('class' => 'btn btn-danger btn-sm'))!!}
                  @endif
                </td>
                <td>{{$order['updated_at']}}</td>
                <td>
                  {!!Html::linkAction('Slr\OrderSlrController@actionView', '', array($order['id']), array('class' => 'btn btn-success fa fa-eye tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'View'))!!}
                </td>
              </tr>
            @endif
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>

@stop
