@extends('slr/master')

@section('content')

<?php $grand_total_weight = 0; ?>
@foreach ($order->order_details as $order_detail)
  <?php
    $datas[$order_detail->product->seller->id][]= array(
      'id' => $order_detail->product->seller->id,
      'status' => $order_detail->status,
      'company' => $order_detail->product->seller->company,
      'name' => $order_detail->product->seller->name,
      'address' => $order_detail->product->seller->address,
      'phone' => $order_detail->product->seller->phone,
      'prod_id' => $order_detail->product->id,
      'code' => $order_detail->product->code,
      'sub_weight' => $order_detail->sub_weight,
      'qty' => $order_detail->quantity,
      'sub_tot' => $order_detail->sub_total
    );
  ?>
@endforeach

@foreach ($datas as $data)
  <?php $weight[$data[0]['id']] = 0; ?>
  @foreach ($datas[$data[0]['id']] as $detail)
    <?php $weight[$data[0]['id']] = $weight[$data[0]['id']] + ($detail['sub_weight'] / 1000); ?>
  @endforeach
@endforeach
<?php $total_weight = 0; ?>
@foreach ($weight as $d)
  <?php $ceil_weight = ceil($d); ?>
  <?php $grand_total_weight = $grand_total_weight + $ceil_weight ?>
@endforeach

<div class="page-header">
  <h1>
    <i class="fa fa-shopping-cart"></i> Sales Invoice <small>#{{$order->code}}</small>
    @if ($datas[Auth::user()->seller->id][0]['status'] == "0")
      {!!Form::button('Processed', array('class' => 'btn btn-purple btn-sm'))!!}
    @elseif ($datas[Auth::user()->seller->id][0]['status'] == "1")
      {!!Form::button('Paid', array('class' => 'btn btn-success btn-sm'))!!}
    @elseif ($datas[Auth::user()->seller->id][0]['status'] == "2")
      {!!Form::button('Ordered', array('class' => 'btn btn-warning btn-sm'))!!}
    @elseif ($datas[Auth::user()->seller->id][0]['status'] == "3")
        {!!Form::button('-', array('class' => 'btn btn-default btn-sm'))!!}
    @elseif ($datas[Auth::user()->seller->id][0]['status'] == "4")
      {!!Form::button('Invalid', array('class' => 'btn btn-danger btn-sm'))!!}
    @elseif ($datas[Auth::user()->seller->id][0]['status'] == "5")
      {!!Form::button('Invalid Purchase', array('class' => 'btn btn-danger btn-sm'))!!}
    @endif
  </h1>
</div>

<div class="page-header text-right"><h3 class="no-margn">BBStore Comp</h3></div>
<hr>

<div class="row">
  <div class="col-md-6">
    <dl>
      <dt>Sales Invoice Details</dt>
      <dd>sales invoice detail / number : {{$data[0]['id']." / ".$order->id}}</dd>
    </dl>
    <address>
      <strong>BBStore, Inc.</strong><br>
      Jln Ters Maninjau Barat 2, B3 A6<br>
      Sawojajar, Malang<br>
      <abbr title="Phone">P:</abbr> (0341) 713857
    </address>
    <dl>
      <strong>Date :</strong> {{date_format($order->created_at, 'd F Y')}}
    </dl>
  </div>

  <div class="col-md-6 text-right">
    <address>
      <strong>Company Name</strong><br>
      {{$datas[Auth::user()->seller->id][0]['company']}}
      <!--{!!Html::link('sellerbbs/profile', $datas[Auth::user()->seller->id][0]['company'])!!}-->
    </address>
    <address>
      <strong>Seller Name</strong><br>
      {{$datas[Auth::user()->seller->id][0]['name']}}
      <!--{!!Html::link('sellerbbs/profile', $datas[Auth::user()->seller->id][0]['name'])!!}-->
    </address>
    <address>
      <strong>Address</strong><br>
      {{$datas[Auth::user()->seller->id][0]['address']}}<br>
      <abbr title="Phone">P:</abbr> {{$datas[Auth::user()->seller->id][0]['phone']}}
    </address>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-body">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Code</th>
          <th>Qty</th>
          <th class="total text-right">Amount ( Rp. )</th>
        </tr>
      </thead>

      <tbody>
        <?php $grandtotal = 0; ?>
        <?php $total_weight = 0; ?>
        @foreach ($datas[Auth::user()->seller->id] as $data)
          <?php $total_weight = $total_weight + $data['sub_weight']; ?>
          <tr>
            <td>{{$data['code']}}</td>
            <!--<td>{!!Html::link('sellerbbs/products/view/'.$data['prod_id'], $data['code'])!!}</td>-->
            <td>{{$data['qty']}}</td>
            <?php $sub_tot = number_format ($data['sub_tot'], 0, ',', '.'); ?>
            <td class="total text-right">{{$sub_tot}}</td>
          </tr>
          <?php $grandtotal = $data['sub_tot'] + $grandtotal; ?>
        @endforeach
        <?php $ceil_total_weight = ceil($total_weight / 1000); ?>
        <?php $shipping_cost = $ceil_total_weight / $grand_total_weight * $order->shipping_cost; ?>
        <?php $shipping_costs = number_format ($shipping_cost, 0, ',', '.'); ?>
        <tr class="total_bar">
          <td class="grand_total"></td>
          <td class="grand_total">Shipping Costs:</td>
          <td class="grand_total text-right">{{$shipping_costs}}</td>
        </tr>
        <?php $finaltot = $grandtotal + $shipping_cost; ?>
        <?php $tot = number_format ($finaltot, 0, ',', '.'); ?>
        <tr class="total_bar">
          <td class="grand_total"></td>
          <td class="grand_total">Total:</td>
          <td class="grand_total text-right">{{$tot}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-lg-6">
    <button class="btn btn-warning" onClick="PrintElem('#printOut')" type="button">Print Sales Invoice</button>
  </div>
  @if ($datas[Auth::user()->seller->id][0]['status'] == "2")
    <div class="col-lg-6 text-right">
      {!!Form::button('Confirm Sales Invoice of Rp.'.$tot.'', array('class' => 'btn btn-purple btnModalConfirmOrder', 'data-toggle' => 'modal', 'data-target' => '#modal-form'))!!}
    </div>
  @endif
</div>

<!-- Modal With Form -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!!Form::open(array('action' => 'Slr\OrderSlrController@actionProcess', 'class' => 'validator-form', 'files' => true))!!}
        {!!Form::hidden('id_order', $order->id, array('class' => 'form-control', 'id' => 'id_order_edit'))!!}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title"><i class="fa fa-tags"></i> Confirm Sales Invoice</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {!!Form::label('code_order_verification', 'Code Sales Invoice')!!}
            <div class="input-group">
              <span class="input-group-addon">#</span>
              {!!Form::text('code_order_verification', "", array('class' => 'form-control'))!!}
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('resi_verification', 'No. Resi')!!}
            {!!Form::text('resi_verification', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('image', 'Photo Verification')!!}
            <span class="btn btn-success fileinput-button">
              <i class="glyphicon glyphicon-plus"></i>
              <span>Add file...</span>
              <input id="verification_resi" onChange="myFunctionVerificationResi()" type="file" name="verification_resi" required="required">
            </span>
            <span id="verification_resiText"></span>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

@stop
