<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>BBS Seller</title>

	<meta name="description" content="">
	<meta name="author" content="Bimo Tri W">
  <link rel="icon" type="image/ico" href="{{url('assets/images/fav.png')}}">

	<!-- Bootstrap core CSS -->
  {!!Html::style('admin-assets/css/bootstrap/bootstrap.css')!!}

  <!-- datatables Styling  -->
  {!!Html::style('admin-assets/css/plugins/datatables/jquery.dataTables.css')!!}

	<!-- Calendar Styling  -->
  {!!Html::style('admin-assets/css/plugins/calendar/calendar.css')!!}

  <!-- Fonts  -->
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>

  <!-- Base Styling  -->
  {!!Html::style('admin-assets/css/app/app.v1.css')!!}

  <!-- Bootstrap Validator  -->
  {!!Html::style('admin-assets/css/plugins/bootstrap-validator/bootstrap-validator.css')!!}

  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  {!!Html::style('admin-assets/css/plugins/file-upload/jquery.fileupload.css')!!}
  {!!Html::style('admin-assets/css/plugins/file-upload/jquery.fileupload-ui.css')!!}

  <!-- Chosen Select  -->
  {!!Html::style('admin-assets/css/plugins/bootstrap-chosen/chosen.css')!!}


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body data-ng-app>

  <!-- Preloader -->
  <div class="loading-container">
    <div class="loading">
      <div class="l1">
        <div></div>
      </div>
      <div class="l2">
        <div></div>
      </div>
      <div class="l3">
        <div></div>
      </div>
      <div class="l4">
        <div></div>
      </div>
    </div>
  </div>
  <!-- Preloader -->


	<aside class="left-panel">

    @foreach (Auth::user()->seller->seller_images as $image)
      @if ($image->type == 'main')
        <div class="user text-center">
          {!!HTML::image($image->url, '...', array('class' => 'img-circle'))!!}
          <h4 class="user-name">{{Auth::user()->seller->name}}</h4>
        </div>
      @endif
    @endforeach

    <nav class="navigation">
    	<ul class="list-unstyled">
        <li {{Request::is('sellerbbs') ? ' class=active' : null}}><a href="{{url('sellerbbs')}}"><i class="fa fa-bookmark-o"></i> <span class="nav-label">Dashboard</span></a></li>
        <li {{Request::is('sellerbbs/profile') ? ' class=active' : null}}><a href="{{url('sellerbbs/profile')}}"><i class="fa fa-user"></i> <span class="nav-label">My Profile</span></a>
        <li {{Request::is('sellerbbs/products*') ? ' class=active' : null}}><a href=""><i class="fa fa-laptop"></i> <span class="nav-label">Products</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('sellerbbs/products')}}"><i class="fa fa-navicon"></i> List</a></li>
            <li><a href="{{url('sellerbbs/products/new')}}"><i class="fa fa-plus"></i> Create New</a></li>
          </ul>
        </li>
        <li {{Request::is('sellerbbs/orders*') ? ' class=active' : null}}><a href=""><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('sellerbbs/orders')}}"><i class="fa fa-navicon"></i> List</a></li>
            <li><a href="{{url('sellerbbs/orders/ordered')}}"><i class="fa fa-shopping-cart"></i> Ordered</a></li>
            <li><a href="{{url('sellerbbs/orders/processed')}}"><i class="fa fa-refresh"></i> Processed</a></li>
            <li><a href="{{url('sellerbbs/orders/paid')}}"><i class="fa fa-credit-card"></i> Paid</a></li>
            <li><a href="{{url('sellerbbs/orders/invalid')}}"><i class="fa fa-exclamation"></i> Invalid</a></li>
          </ul>
        </li>
        <li {{Request::is('sellerbbs/reports*') ? ' class=active' : null}}><a href=""><i class="fa fa-bar-chart"></i> <span class="nav-label">Reports</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('sellerbbs/reports/order')}}"><i class="fa fa-shopping-cart"></i> Orders</a></li>
          </ul>
        </li>
        <li {{Request::is('sellerbbs/help') ? ' class=active' : null}}><a href="{{url('sellerbbs/help')}}"><i class="fa fa-question-circle"></i> Help</a></li>
      </ul>
    </nav>

  </aside>
  <!-- Aside Ends-->

  <section class="content">

    <header class="top-head container-fluid">
      <button type="button" class="navbar-toggle pull-left">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <form role="search" class="navbar-left app-search pull-left hidden-xs">
        <input type="text" placeholder="Enter keywords..." class="form-control form-control-circle">
      </form>

      <ul class="nav-toolbar">

        <?php $notif = 0; ?>
        @if (count(Auth::user()->seller->products))
          <?php $par[] = ""; ?>
          @foreach (Auth::user()->seller->products as $product)
            @foreach ($product->order_detailOrders as $detail)
              <?php $par[$detail->orderdetailable_id] = ''; ?>
              <?php $par1[$detail->orderdetailable_id] = ''; ?>
              @if ($detail->notification_status_seller == '0' && $detail->status == '2')
                <?php $par[$detail->orderdetailable_id] = $detail->orderdetailable_id; ?>
              @endif
            @endforeach
          @endforeach
          @if (count($par))
            @foreach ($par as $param)
              @if ($param != '')
                <?php $notif++; ?>
              @endif
            @endforeach
          @endif
        @endif

        <li class="dropdown tooltip-btn" data-toggle="tooltip" data-placement="left" title="" data-original-title="Notification"><a href="" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="badge">{!!$notif!='0'  ? $notif : ''!!}</span></a>
          <div class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">
            <div class="panel-heading">
              Notification
            </div>
            <div class="list-group">

              @if (count(Auth::user()->seller->products))
                @foreach (Auth::user()->seller->products as $product)
                  @foreach ($product->order_detailOrders as $detail)
                    @if ($detail->notification_status_seller == '0' && $detail->status == '2')
                      @if ($par1[$detail->orderdetailable_id] == '')
                        <?php $par1[$detail->orderdetailable_id] = $detail->orderdetailable_id; ?>
                        <a href="{{url('sellerbbs/orders/view/'.$detail->orderdetailable_id)}}" class="list-group-item">
                          <div class="media">
                            <span class="label label-warning media-object img-circle pull-left">Ordered</span>
                            <div class="media-body">
                              <h5 class="media-heading">{{$detail->order->code}} <br> <small>{{date_format($detail->order->updated_at, 'd M Y - H:i:s')}}</small></h5>
                            </div>
                          </div>
                        </a>
                      @endif
                    @endif
                  @endforeach
                @endforeach
              @endif

              <a href="" class="list-group-item"></a>
            </div>
          </div>
        </li>

        <li class="tooltip-btn" data-toggle="tooltip" data-placement="left" title="" data-original-title="Log Out"><a href="{{url('sellerbbs/logout')}}"><i class="glyphicon glyphicon-log-out"></i></a></li>
      </ul>
    </header>
    <!-- Header Ends -->


    <div class="warper container-fluid" id="printOut">

      @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="notif">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          {{Session::get('success')}}
        </div>
      @endif
      @if (Session::get('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" id="notif">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          {{Session::get('error')}}
        </div>
      @endif
      @if (Session::get('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert" id="notif">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          {{Session::get('warning')}}
        </div>
      @endif

      @yield('content')

    </div>
    <!-- Warper Ends Here (working area) -->


    <footer class="container-fluid footer">
    	Copyright &copy;<?php echo date("Y"); ?>
      <a href="" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a>
    </footer>


  </section>
  <!-- Content Block Ends Here (right box)-->


  <!-- JQuery v1.9.1 -->

  {!!Html::script('admin-assets/js/jquery/jquery-1.9.1.min.js')!!}
	{!!Html::script('admin-assets/js/plugins/underscore/underscore-min.js')!!}

  <!-- Bootstrap -->
  {!!Html::script('admin-assets/js/bootstrap/bootstrap.min.js')!!}

  <!-- Globalize -->
  {!!Html::script('admin-assets/js/globalize/globalize.min.js')!!}

  <!-- NanoScroll -->
  {!!Html::script('admin-assets/js/plugins/nicescroll/jquery.nicescroll.min.js')!!}

  <!-- Chart JS -->
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/dx.chartjs.js')!!}
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/world.js')!!}

 	<!-- For Demo Charts -->
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/demo-charts.js')!!}
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/demo-vectorMap.js')!!}

  <!-- Sparkline JS -->
  {!!Html::script('admin-assets/js/plugins/sparkline/jquery.sparkline.min.js')!!}

  <!-- For Demo Sparkline -->
  {!!Html::script('admin-assets/js/plugins/sparkline/jquery.sparkline.demo.js')!!}

  <!-- Angular JS -->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.14/angular.min.js"></script>

  <!-- ToDo List Plugin -->
  {!!Html::script('admin-assets/js/angular/todo.js')!!}

  <!-- Calendar JS -->
  {!!Html::script('admin-assets/js/plugins/calendar/calendar.js')!!}

  <!-- Calendar Conf
  {!!Html::script('admin-assets/js/plugins/calendar/calendar-conf.js')!!}
  -->
  <!-- Custom JQuery -->
  {!!Html::script('admin-assets/js/app/custom.js')!!}

  <!-- Data Table -->
  {!!Html::script('admin-assets/js/plugins/datatables/jquery.dataTables.js')!!}
  {!!Html::script('admin-assets/js/plugins/datatables/DT_bootstrap.js')!!}
  {!!Html::script('admin-assets/js/plugins/datatables/jquery.dataTables-conf.js')!!}

  <!-- Chosen -->
  {!!Html::script('admin-assets/js/plugins/bootstrap-chosen/chosen.jquery.js')!!}

  <!-- Bootstrap Validator -->
  {!!Html::script('admin-assets/js/plugins/bootstrap-validator/bootstrapValidator.min.js')!!}
  {!!Html::script('admin-assets/js/plugins/bootstrap-validator/bootstrapValidator-conf.js')!!}


	<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-56821827-1', 'auto');
    ga('send', 'pageview');
  </script>

  <script>
    $(document).ready(function(){
      $('#notif').delay(3000).fadeOut(400);
    });
  </script>

  @if (Request::is('sellerbbs/reports/order') || Request::is('sellerbbs'))
    <script>
      $("#line-chartOrder").dxChart({
          dataSource: <?php echo json_encode($val); ?>,
          commonSeriesSettings: {
              argumentField: "month"
          },
          series: [
              { valueField: "value1", name: "Ordered", color: "#f0ad4e" },
              { valueField: "value2", name: "Processed", color: "#8d82b5" },
              { valueField: "value3", name: "Paid", color: "#449d44" }
          ],
          tooltip:{
              enabled: true,
      		font: { size: 12 }
          },
          legend: {
              visible: true
          },
      	valueAxis:{
      		grid:{
      			color: '#9D9EA5',
      			width: 0.1
      			}
      	}
      });
    </script>
  @endif

</body>
</html>
