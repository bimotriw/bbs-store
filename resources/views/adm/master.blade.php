<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>BBS Administration</title>

	<meta name="description" content="">
	<meta name="author" content="Bimo Tri W">
  <link rel="icon" type="image/ico" href="{{url('assets/images/fav.png')}}">

	<!-- Bootstrap core CSS -->
  {!!Html::style('admin-assets/css/bootstrap/bootstrap.css')!!}

  <!-- datatables Styling  -->
  {!!Html::style('admin-assets/css/plugins/datatables/jquery.dataTables.css')!!}

	<!-- Calendar Styling  -->
  {!!Html::style('admin-assets/css/plugins/calendar/calendar.css')!!}

  <!-- Fonts  -->
  <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>

  <!-- Base Styling  -->
  {!!Html::style('admin-assets/css/app/app.v1.css')!!}

  <!-- Bootstrap Validator  -->
  {!!Html::style('admin-assets/css/plugins/bootstrap-validator/bootstrap-validator.css')!!}

  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  {!!Html::style('admin-assets/css/plugins/file-upload/jquery.fileupload.css')!!}
  {!!Html::style('admin-assets/css/plugins/file-upload/jquery.fileupload-ui.css')!!}

  <!-- Chosen Select  -->
  {!!Html::style('admin-assets/css/plugins/bootstrap-chosen/chosen.css')!!}


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body data-ng-app>


  <!-- Preloader -->
  <div class="loading-container">
    <div class="loading">
      <div class="l1">
        <div></div>
      </div>
      <div class="l2">
        <div></div>
      </div>
      <div class="l3">
        <div></div>
      </div>
      <div class="l4">
        <div></div>
      </div>
    </div>
  </div>
  <!-- Preloader -->


	<aside class="left-panel">

    <div class="user text-center">
      {!!HTML::image('admin-assets/images/avtar/user.png', '...', array('class' => 'img-circle'))!!}
      <h4 class="user-name">BBS Admin</h4>
    </div>



    <nav class="navigation">
    	<ul class="list-unstyled">
        <li {{Request::is('adminbbs') ? ' class=active' : null}}><a href="{{url('adminbbs')}}"><i class="fa fa-bookmark-o"></i> <span class="nav-label">Dashboard</span></a></li>
        <li {{Request::is('adminbbs/buyers*') ? ' class=active' : null}}><a href="{{url('adminbbs/buyers')}}"><i class="fa fa-user"></i> <span class="nav-label">Users (Buyer)</span></a></li>
        <!--
        <li {{Request::is('adminbbs/buyers*') ? ' class=active' : null}}><a href=""><i class="fa fa-user"></i> <span class="nav-label">Users (Buyer)</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('adminbbs/buyers')}}"><i class="fa fa-navicon"></i> List</a></li>
            <li><a href="{{url('adminbbs/buyers/new')}}"><i class="fa fa-plus"></i> Create New</a></li>
          </ul>
        </li>
        -->
        <!--
        <li {{Request::is('adminbbs/sellers*') ? ' class=active' : null}}><a href="{{url('adminbbs/sellers')}}"><i class="fa fa-user-md"></i> <span class="nav-label">Users (Seller)</span></a></li>
        -->
        <li {{Request::is('adminbbs/sellers*') ? ' class=active' : null}}><a href=""><i class="fa fa-user-md"></i> <span class="nav-label">Users (Seller)</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('adminbbs/sellers')}}"><i class="fa fa-navicon"></i> List</a></li>
            <li><a href="{{url('adminbbs/sellers/new')}}"><i class="fa fa-plus"></i> Create New</a></li>
          </ul>
        </li>
        <li {{Request::is('adminbbs/catalogs*') ? ' class=active' : null}}><a href=""><i class="fa fa-tags"></i> <span class="nav-label">Catalogs</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('adminbbs/catalogs/categories')}}"><i class="fa fa-tasks"></i> Categories</a></li>
            <li><a href="{{url('adminbbs/catalogs/products')}}"><i class="fa fa-laptop"></i> Products</a></li>
          </ul>
        </li>
        <li {{Request::is('adminbbs/orders*') ? ' class=active' : null}}><a href=""><i class="fa fa-shopping-cart"></i> <span class="nav-label">Orders</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('adminbbs/orders')}}"><i class="fa fa-navicon"></i> List</a></li>
            <li><a href="{{url('adminbbs/orders/responded')}}"><i class="fa fa-mail-forward"></i> Responded</a></li>
            <li><a href="{{url('adminbbs/orders/paid')}}"><i class="fa fa-credit-card"></i> Paid</a></li>
            <li><a href="{{url('adminbbs/orders/invalid')}}"><i class="fa fa-exclamation"></i> Invalid</a></li>
            <li><a href="{{url('adminbbs/orders/complaints')}}"><i class="fa fa-comment"></i> Complaints</a></li>
          </ul>
        </li>
        <li {{Request::is('adminbbs/reports*') ? ' class=active' : null}}><a href=""><i class="fa fa-bar-chart"></i> <span class="nav-label">Reports</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('adminbbs/reports/order')}}"><i class="fa fa-shopping-cart"></i> Orders</a></li>
          </ul>
        </li>
        <li {{Request::is('adminbbs/settings*') ? ' class=active' : null}}><a href=""><i class="fa fa-wrench"></i> <span class="nav-label">Settings</span></a>
          <ul class="list-unstyled">
            <li><a href="{{url('adminbbs/settings/backup')}}"><i class="fa fa-database"></i> Backup / Restore</a></li>
            <!--<li><a href="{{url('adminbbs/settings/help')}}"><i class="fa fa-question-circle"></i> Help</a></li>-->
          </ul>
        </li>
      </ul>
    </nav>

  </aside>
    <!-- Aside Ends-->

  <section class="content">

    <header class="top-head container-fluid">
      <button type="button" class="navbar-toggle pull-left">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <form role="search" class="navbar-left app-search pull-left hidden-xs">
        <input type="text" placeholder="Enter keywords..." class="form-control form-control-circle">
     	</form>

      <ul class="nav-toolbar">

        <?php $notif = 0; ?>
        @foreach ($orders as $order)
          @if ($order->notification_status == '0' && $order->status == '0')
            <?php $notif++; ?>
          @endif
        @endforeach
        @foreach ($order_details as $detail)
          <?php $par[$detail->product->seller->id] = ''; ?>
        @endforeach
        @foreach ($order_details as $detail)
          @if ($detail->notification_status == '0' && $detail->status == '0')
            @if ($par[$detail->product->seller->id] == '')
              <?php $par[$detail->product->seller->id] = $detail->product->seller->id; ?>
              <?php $notif++; ?>
            @endif
          @endif
        @endforeach
        <li class="dropdown tooltip-btn" data-toggle="tooltip" data-placement="left" title="" data-original-title="Notification"><a href="" data-toggle="dropdown"><i class="fa fa-bell-o"></i><span class="badge">{!!$notif!='0'  ? $notif : ''!!}</span></a>
          <div class="dropdown-menu arrow pull-right md panel panel-default arrow-top-right notifications">
            <div class="panel-heading">
            	Notification
            </div>

            <div class="list-group">
              @foreach ($orders as $order)
                @if ($order->notification_status == '0' && $order->status == '0')
                  <a href="{{url('adminbbs/orders/view/'.$order->id)}}" class="list-group-item">
                    <div class="media">
                      <span class="label label-purple media-object img-circle pull-left">Purchase - Responded</span>
                      <div class="media-body">
                        <h5 class="media-heading">{{$order->code}} <br> <small>{{date_format($order->updated_at, 'd M Y - H:i:s')}}</small></h5>
                      </div>
                    </div>
                  </a>
                @endif
              @endforeach

              @foreach ($order_details as $detail)
                <?php $par[$detail->product->seller->id] = ''; ?>
              @endforeach
              @foreach ($order_details as $detail)
                @if ($detail->notification_status == '0' && $detail->status == '0')
                  @if ($par[$detail->product->seller->id] == '')
                    <?php $par[$detail->product->seller->id] = $detail->product->seller->id; ?>
                    <a href="{{url('adminbbs/orders/view/'.$detail->order->id)}}" class="list-group-item">
                      <div class="media">
                        <span class="label label-purple media-object img-circle pull-left">Seller - Responded</span>
                        <div class="media-body">
                          <h5 class="media-heading">{{$detail->order->code}} <br> {{$detail->product->seller->name}} <br> <small>{{date_format($detail->order->updated_at, 'd M Y - H:i:s')}}</small></h5>
                        </div>
                      </div>
                    </a>
                  @endif
                @endif
              @endforeach
              <a href="" class="list-group-item"></a>
            </div>

          </div>
        </li>
        <li class="tooltip-btn" data-toggle="tooltip" data-placement="left" title="" data-original-title="Log Out"><a href="{{url('adminbbs/logout')}}"><i class="glyphicon glyphicon-log-out"></i></a></li>
      </ul>
    </header>
    <!-- Header Ends -->


    <div class="warper container-fluid">

      @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible" role="alert" id="notif">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          {{Session::get('success')}}
        </div>
      @endif
      @if (Session::get('error'))
        <div class="alert alert-danger alert-dismissible" role="alert" id="notif">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          {{Session::get('error')}}
        </div>
      @endif
      @if (Session::get('warning'))
        <div class="alert alert-warning alert-dismissible" role="alert" id="notif">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          {{Session::get('warning')}}
        </div>
      @endif

      @yield('content')

    </div>
    <!-- Warper Ends Here (working area) -->


    <footer class="container-fluid footer">
    	Copyright &copy;<?php echo date("Y"); ?>
      <a href="" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a>
    </footer>


  </section>
  <!-- Content Block Ends Here (right box)-->


  <!-- JQuery v1.9.1 -->
  {!!Html::script('admin-assets/js/jquery/jquery-1.9.1.min.js')!!}
	{!!Html::script('admin-assets/js/plugins/underscore/underscore-min.js')!!}

  <!-- Bootstrap -->
  {!!Html::script('admin-assets/js/bootstrap/bootstrap.min.js')!!}

  <!-- Globalize -->
  {!!Html::script('admin-assets/js/globalize/globalize.min.js')!!}

  <!-- NanoScroll -->
  {!!Html::script('admin-assets/js/plugins/nicescroll/jquery.nicescroll.min.js')!!}

  <!-- Chart JS -->
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/dx.chartjs.js')!!}
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/world.js')!!}

 	<!-- For Demo Charts -->
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/demo-charts.js')!!}
  {!!Html::script('admin-assets/js/plugins/DevExpressChartJS/demo-vectorMap.js')!!}

  <!-- Sparkline JS -->
  {!!Html::script('admin-assets/js/plugins/sparkline/jquery.sparkline.min.js')!!}

  <!-- For Demo Sparkline -->
  {!!Html::script('admin-assets/js/plugins/sparkline/jquery.sparkline.demo.js')!!}

  <!-- Angular JS -->
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.14/angular.min.js"></script>

  <!-- ToDo List Plugin -->
  {!!Html::script('admin-assets/js/angular/todo.js')!!}

  <!-- Calendar JS -->
  {!!Html::script('admin-assets/js/plugins/calendar/calendar.js')!!}

  <!-- Calendar Conf
  {!!Html::script('admin-assets/js/plugins/calendar/calendar-conf.js')!!}
  -->
  <!-- Custom JQuery -->
  {!!Html::script('admin-assets/js/app/custom.js')!!}

  <!-- Data Table -->
  {!!Html::script('admin-assets/js/plugins/datatables/jquery.dataTables.js')!!}
  {!!Html::script('admin-assets/js/plugins/datatables/DT_bootstrap.js')!!}
  {!!Html::script('admin-assets/js/plugins/datatables/jquery.dataTables-conf.js')!!}

  <!-- Chosen -->
  {!!Html::script('admin-assets/js/plugins/bootstrap-chosen/chosen.jquery.js')!!}

  <!-- Bootstrap Validator -->
  {!!Html::script('admin-assets/js/plugins/bootstrap-validator/bootstrapValidator.min.js')!!}
  {!!Html::script('admin-assets/js/plugins/bootstrap-validator/bootstrapValidator-conf.js')!!}


	<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-56821827-1', 'auto');
    ga('send', 'pageview');
  </script>

  <script>
  $(document).on("click", ".btnModal", function () {
     var categoryId = $(this).data('id');
     var categoryName = $(this).data('name');
     var categoryIdParent = $(this).data('idparent');
     var categoryParentName = $(this).data('parent');
     $("#name_edit").val( categoryName );
     $("#id_edit").val( categoryId );
     $("#parent_edit").val( categoryParentName );
  });
  function myFunction() {
    var x = document.getElementById("file").value;
    var res = x.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
    var y = res.replace("Cfakepath", "");
    document.getElementById("fileText").innerHTML = y;
  }
  function myFunctionImage() {
    var x = document.getElementById("image").value;
    var res = x.replace(/([*+?^=!:${}()|\[\]\/\\])/g, "");
    var y = res.replace("Cfakepath", "");
    document.getElementById("fileTextImage").innerHTML = y;
  }
  </script>

  <script>
    $(document).ready(function(){
      $('#notif').delay(3000).fadeOut(400);
    });
  </script>

  @if (Request::is('adminbbs/reports/order') || Request::is('adminbbs'))
    <script>
      $("#line-chartOrder").dxChart({
          dataSource: <?php echo json_encode($val); ?>,
          commonSeriesSettings: {
              argumentField: "month"
          },
          series: [
              { valueField: "value1", name: "Ordered", color: "#f0ad4e" },
              { valueField: "value2", name: "Processed", color: "#8d82b5" },
              { valueField: "value3", name: "Paid", color: "#449d44" }
          ],
          tooltip:{
              enabled: true,
          font: { size: 12 }
          },
          legend: {
              visible: true
          },
        valueAxis:{
          grid:{
            color: '#9D9EA5',
            width: 0.1
            }
        }
      });
    </script>
  @endif

</body>
</html>
