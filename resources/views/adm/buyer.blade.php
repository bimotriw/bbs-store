@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-user"></i> Buyers List</h1></div>

  <div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt"></i> Buyers List</div>
    <div class="panel-body">

      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
        <thead>
          <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Company</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if (count($buyers))
            @foreach ($buyers as $buyer)
              <tr>
                <td>{{$buyer->name}}</td>
                <td>{{$buyer->user->email}}</td>
                <td>{{$buyer->phone}}</td>
                <td>{{$buyer->company}}</td>
                <td>
                  {!!Html::linkAction('Adm\BuyerAdmController@actionView', '', array($buyer->id), array('class' => 'btn btn-success fa fa-eye tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'View'))!!}
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
    </table>
  </div>
</div>

@stop
