@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-user-md"></i> Seller Details <small>Product Details</small></h1></div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Product Details</div>
      <div class="panel-body">
        <div class="form-group">
          {!!Form::label('code', 'Code')!!}
          {!!Form::text('name', "$product->code", array('class' => 'form-control', 'disabled'))!!}
        </div>
        <div class="form-group">
          {!!Form::label('name', 'Product Name')!!}
          {!!Form::text('name', "$product->name", array('class' => 'form-control', 'disabled'))!!}
        </div>
        <div class="form-group">
          {!!Form::label('price', 'Price')!!}
          <div class="input-group">
            <span class="input-group-addon">Rp.</span>
            {!!Form::text('price', "$product->price", array('class' => 'form-control', 'disabled'))!!}
            <span class="input-group-addon">,00</span>
          </div>
        </div>
        <div class="form-group">
          {!!Form::label('weight', 'Weight')!!}
          <div class="input-group">
            {!!Form::text('weight', "$product->weight", array('class' => 'form-control', 'disabled'))!!}
            <span class="input-group-addon">gram</span>
          </div>
        </div>
        <div class="form-group">
          {!!Form::label('description', 'Description')!!}
          {!!Form::textarea('description', "$product->description", ['class' => 'form-control', 'disabled'])!!}
        </div>
        <div class="form-group">
          {!!Form::label('specification', 'Specification')!!}
          {!!Form::textarea('specification', "$product->specification", ['class' => 'form-control', 'disabled'])!!}
        </div>
        <div class="form-group">
          {!!Form::label('additional_document', 'Additional Document')!!}
          <!--
          <span class="btn btn-success fileinput-button">
              <i class="glyphicon glyphicon-plus"></i>
              <span>Add files...</span>
              <input id="file" onChange="myFunction()" type="file" name="files" multiple>
          </span>
          -->
          <div id="fileText">{!!Html::link($product->additional_document, $product->additional_document_resize)!!}</div>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Product Images</div>
      <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
          <thead>
            <tr>
              <th>Images</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
            @if (count($seller->product_images))
              @foreach ($seller->product_images as $images)
                @if ($images->product_id == $product->id)
                  <tr>
                    <td>{!!Html::image($images->url, 'a picture', array('class' => 'thumb'))!!}</td>
                    <td>
                      {!!$images->type == 'main'  ?
                        Form::button('main', array('class' => 'btn btn-purple btn-sm')) :
                        Form::button('other', array('class' => 'btn btn-success btn-sm'))
                      !!}
                    </td>
                  </tr>
                @endif
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

@stop
