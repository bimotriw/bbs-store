@extends('adm/master')

@section('content')
<div class="page-header">
  <h1>
    <i class="fa fa-shopping-cart"></i> Order List
    @if (Request::is('adminbbs/orders/responded'))
      {!!Form::button('Responded', array('class' => 'btn btn-purple btn-sm'))!!}
      <?php $status = "0"; ?>
    @elseif (Request::is('adminbbs/orders/paid'))
      {!!Form::button('Paid', array('class' => 'btn btn-success btn-sm'))!!}
      <?php $status = "1"; ?>
    @elseif (Request::is('adminbbs/orders/invalid'))
      {!!Form::button('Invalid', array('class' => 'btn btn-danger btn-sm'))!!}
      <?php $status = "3"; ?>
    @endif
  </h1>
</div>

<div class="panel panel-default">
  <div class="panel-heading"><i class="fa fa-list-alt"></i> Orders List</div>
  <div class="panel-body">

    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
      <thead>
        <tr>
          <th>Date</th>
          <th>CODE</th>
          <th>Buyer Name</th>
          <th>Updated at</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @if (count($orders))
          @foreach ($orders as $order)
            @if ($order->status == $status)
              <tr>
                <td>{{$order->created_at}}</td>
                <td>{{$order->code}}</td>
                <td>{!!Html::link('adminbbs/buyers/view/'.$order->buyer->id, $order->buyer->name)!!}</td>
                <td>{{$order->updated_at}}</td>
                <td>
                  {!!Html::linkAction('Adm\OrderAdmController@actionView', '', array($order->id), array('class' => 'btn btn-success fa fa-eye'))!!}
                </td>
              </tr>
            @endif
          @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>

@stop
