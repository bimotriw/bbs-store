@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-user-md"></i> New Seller</h1></div>

<div class="row">
  {!!Form::open(array('action' => 'Adm\SellerAdmController@actionStore', 'class' => 'validator-form'))!!}
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Seller Details</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('name', 'Name')!!}
            {!!Form::text('name', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('company_sel', 'Company')!!}
            {!!Form::text('company_sel', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('address', 'Address')!!}
            {!!Form::textarea('address', "", ['class' => 'form-control', 'size' => '30x3'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('phone', 'Phone')!!}
            {!!Form::text('phone', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('description', 'Description')!!}
            {!!Form::textarea('description', "", ['class' => 'form-control'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('company_type', 'Company Type')!!}
            {!!Form::text('company_type', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('year_established', 'Year Established')!!}
            {!!Form::text('year_established', "", array('class' => 'form-control'))!!}
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Seller Account</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('email', 'Email')!!}
            {!!Form::text('email', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('password', 'Password')!!}
            {!!Form::password('password', array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('confirmPassword', 'Retype-Password')!!}
            {!!Form::password('confirmPassword', array('class' => 'form-control'))!!}
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Account Number</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('bca', 'BCA')!!}
            {!!Form::text('bca', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('bri', 'BRI')!!}
            {!!Form::text('bri', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('mandiri', 'Mandiri')!!}
            {!!Form::text('mandiri', "", array('class' => 'form-control'))!!}
          </div>
          <button type="submit" class="btn btn-primary">Add New</button>
        </div>
      </div>

    {!!Form::close()!!}
  </div>

</div>

@stop
