@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-tasks"></i> Categories List</h1></div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Categories List</div>
      <div class="panel-body">

        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
          <thead>
            <tr>
              <th>Name</th>
              <th>Parent</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if (count($categories))
              @foreach ($categories as $category)
                <?php $parent_name = NULL ?>
                <tr>
                  <td>{{$category->name}}</td>
                  <td>
                    @if (count($categories))
                      @foreach ($categories as $parent)
                        @if ($parent->id == $category->parent_id)
                          {{$parent->name}}
                          <?php $parent_name = $parent->name; ?>
                        @endif
                      @endforeach
                    @endif
                  </td>
                  <td>
                    {!!Form::button('', array('class' => 'btn btn-purple fa fa-edit btnModal tooltip-btn', 'data-toggle' => 'modal', 'data-target' => '#modal-form', 'data-id' => $category->id, 'data-name' => $category->name, 'data-idparent' => $category->parent_id, 'data-parent' => $parent_name, 'onClick' => 'myFunctionSelected()', 'data-placement' => 'left', 'data-original-title' => 'Edit'))!!}
                    {!!Form::button('', array('class' => 'btn btn-danger fa fa-trash btnModalCategoryDelete tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Delete', 'data-toggle' => 'modal', 'data-target' => '#modal-delete-category', 'data-id' => $category->id))!!}
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> New Category</div>
      <div class="panel-body">
        {!!Form::open(array('action' => 'Adm\CategoryAdmController@actionStore', 'class' => 'validator-form'))!!}
          <div class="form-group">
            {!!Form::label('name', 'Name')!!}
            {!!Form::text('name', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('parent', 'Parent')!!}
            <select name="parent" class="form-control chosen-select" data-placeholder="Choose Parent">
              <option value="0"></option>
              @if(count($categories))
                @foreach($categories as $category)
                  <option value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
              @endif
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Add New</button>
        {!!Form::close()!!}
      </div>
    </div>
  </div>
</div>

<!-- Modal With Form -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      {!!Form::open(array('action' => 'Adm\CategoryAdmController@actionUpdate', 'class' => 'validator-form'))!!}
        {!!Form::hidden('id', "", array('class' => 'form-control', 'id' => 'id_edit'))!!}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-list-alt"></i> Edit Category</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            {!!Form::label('name', 'Name')!!}
            {!!Form::text('name', "", array('class' => 'form-control', 'id' => 'name_edit'))!!}
          </div>
          <ul role="tablist" class="nav nav-tabs" id="myTab">
            <li class="active"><a data-toggle="tab" role="tab" href="#currentParent">Current Parent</a></li>
            <li><a data-toggle="tab" role="tab" href="#newParent">New Parent</a></li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div id="currentParent" class="tab-pane tabs-up fade in active panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  {!!Form::label('parent', 'Parent')!!}
                  {!!Form::text('parent', "", array('class' => 'form-control', 'id' => 'parent_edit', 'disabled'))!!}
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9">
                    <label class="cr-styled">
                          <input type="checkbox" name="check" ng-model="todo.done">
                          <i class="fa"></i>
                      </label>
                      Reset Parent ?!
                  </div>
                </div>
              </div>
            </div>
            <div id="newParent" class="tab-pane tabs-up fade panel panel-default">
              <div class="panel-body">
                <div class="form-group">
                  {!!Form::label('parent', 'Parent')!!}
                  <select id='parent_edit' name="parent_edit" class="form-control chosen-select" data-placeholder="Choose Parent">
                    <option value="0"></option>
                    @if(count($categories))
                      @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

<!-- Modal With Form Delete -->
<div class="modal fade" id="modal-delete-category" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to delete this category?</p>
      </div>
      <div class="modal-footer" id="modal-footer"></div>
    </div>
  </div>
</div>

@stop
