@extends('adm/master')

@section('content')
<div class="page-header">
  <h1><i class="fa fa-user-md"></i> Seller Details</h1>
  {!!$seller->deleted == '0'  ?
    Form::button('active', array('class' => 'btn btn-success btn-sm')) :
    Form::button('banned', array('class' => 'btn btn-purple btn-sm'))
  !!}
</div>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Seller Details</div>
      <div class="panel-body">
        <div class="form-group">
          {!!Form::label('name', 'Name')!!}
          {!!Form::text('name', "$seller->name", array('class' => 'form-control', 'disabled'))!!}
        </div>
        <div class="form-group">
          {!!Form::label('company_sel', 'Company')!!}
          {!!Form::text('company_sel', "$seller->company", array('class' => 'form-control', 'disabled'))!!}
        </div>
        <div class="form-group">
          {!!Form::label('address', 'Address')!!}
          {!!Form::textarea('address', "$seller->address", ['class' => 'form-control', 'disabled', 'size' => '30x3'])!!}
        </div>
        <div class="form-group">
          {!!Form::label('phone', 'Phone')!!}
          {!!Form::text('phone', "$seller->phone", array('class' => 'form-control', 'disabled'))!!}
        </div>
        <div class="form-group">
          {!!Form::label('description', 'Description')!!}
          {!!Form::textarea('description', "$seller->description", ['class' => 'form-control', 'disabled'])!!}
        </div>
        <div class="form-group">
          {!!Form::label('company_type', 'Company Type')!!}
          {!!Form::text('company_type', "$seller->company_type", array('class' => 'form-control', 'disabled'))!!}
        </div>
        <div class="form-group">
          {!!Form::label('year_established', 'Year Established')!!}
          {!!Form::text('year_established', "$seller->year_established", array('class' => 'form-control', 'disabled'))!!}
        </div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Company Images</div>
      <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
          <thead>
            <tr>
              <th>Images</th>
              <th>Type</th>
            </tr>
          </thead>
          <tbody>
            @if (count($seller->seller_images))
              @foreach ($seller->seller_images as $image)
                <tr>
                  <td>{!!Html::image($image->url, 'a picture', array('class' => 'thumb'))!!}</td>
                  <td>
                    {!!$image->type == 'main'  ?
                      Form::button('main', array('class' => 'btn btn-purple btn-sm')) :
                      Form::button('other', array('class' => 'btn btn-success btn-sm'))
                    !!}
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Account Seller</div>
      <div class="panel-body">
        {!!Form::open(array('action' => 'Adm\SellerAdmController@actionUpdate', 'class' => 'validator-form'))!!}
          {!!Form::hidden('id', $seller->id)!!}
          <div class="form-group">
            {!!Form::label('email', 'Email')!!}
            {!!Form::text('email', $seller->user->email, array('class' => 'form-control', 'disabled'))!!}
          </div>
          <!--
          <div class="form-group">
            {!!Form::label('password', 'Password')!!}
            {!!Form::password('password', array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('confirmPassword', 'Retype-Password')!!}
            {!!Form::password('confirmPassword', array('class' => 'form-control'))!!}
          </div>
          <button type="submit" class="btn btn-purple">Edit</button>
          -->
        {!!Form::close()!!}
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Account Number</div>
      <div class="panel-body">
        @foreach ($seller->accounts as $account)
          <div class="form-group">
            {!!Form::label($account->account_type, $account->account_type)!!}
            {!!Form::text($account->account_type, $account->account_number, array('class' => 'form-control chosen-rtl', 'disabled'))!!}
          </div>
        @endforeach
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Products</div>
      <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
          <thead>
            <tr>
              <th>Code</th>
              <th>Product Name</th>
              <th>Price</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @if (count($seller->products))
              @foreach ($seller->products as $product)
                <tr>
                  <td>{{$product->code}}</td>
                  <td>{{$product->name}}</td>
                  <td>{{$product->price}}</td>
                  <td>
                    {!!$product->deleted == '0'  ?
                      Form::button('active', array('class' => 'btn btn-success btn-sm')) :
                      Form::button('inactive', array('class' => 'btn btn-purple btn-sm'))
                    !!}
                  </td>
                  <td>
                    {!!Html::linkAction('Adm\SellerAdmController@actionProductView', '', array($seller->id, $product->id), array('class' => 'btn btn-success fa fa-eye tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'View'))!!}
                    <!--
                    {!!$product->deleted == '0'  ?
                      Html::linkAction('Adm\SellerAdmController@actionProductInactive', '', array($seller->id, $product->id), array('class' => 'btn btn-danger fa fa-trash tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Delete')) :
                      Html::linkAction('Adm\SellerAdmController@actionProductActive', '', array($seller->id, $product->id), array('class' => 'btn btn-warning fa fa-refresh tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Restore'))
                    !!}
                    -->
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

@stop
