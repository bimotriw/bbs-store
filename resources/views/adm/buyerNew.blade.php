@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-user"></i> New Buyer</h1></div>

<div class="row">
  {!!Form::open(array('action' => 'Adm\BuyerAdmController@actionStore', 'class' => 'validator-form'))!!}
    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Buyer Details</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('name', 'Name')!!}
            {!!Form::text('name', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('company', 'Company')!!}
            {!!Form::text('company', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('address', 'Address')!!}
            {!!Form::textarea('address', "", ['class' => 'form-control', 'size' => '30x3'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('phone', 'Phone')!!}
            {!!Form::text('phone', "", array('class' => 'form-control'))!!}
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><i class="fa fa-list-alt"></i> Buyer Account</div>
        <div class="panel-body">
          <div class="form-group">
            {!!Form::label('email', 'Email')!!}
            {!!Form::text('email', "", array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('password', 'Password')!!}
            {!!Form::password('password', array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('confirmPassword', 'Retype-Password')!!}
            {!!Form::password('confirmPassword', array('class' => 'form-control'))!!}
          </div>
          <button type="submit" class="btn btn-primary">Add New</button>
        </div>
      </div>

    {!!Form::close()!!}
  </div>

</div>

@stop
