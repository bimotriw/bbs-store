@extends('adm/master')

@section('content')
<div id="printOut">
<div class="page-header">
  <h1>
    Purchase Invoice <small>#{{$order->code}}</small>
    @if ($order->status == "0")
      {!!Form::button('Responded', array('class' => 'btn btn-purple btn-sm'))!!}
    @elseif ($order->status == "1")
      {!!Form::button('Paid', array('class' => 'btn btn-success btn-sm'))!!}
    @elseif ($order->status == "2")
      {!!Form::button('Ordered', array('class' => 'btn btn-warning btn-sm'))!!}
    @elseif ($order->status == "3")
      {!!Form::button('Invalid', array('class' => 'btn btn-danger btn-sm'))!!}
    @endif
  </h1>
</div>

<div class="page-header text-right"><h3 class="no-margn">BBStore Comp</h3></div>
<hr>

<div class="row">
  <div class="col-md-6">
    <dl>
      <dt>Purchase Invoice Details</dt>
      <dd>purchase invoice detail / number : {{$order->id}}</dd>
    </dl>
    <address>
      <strong>BBStore, Inc.</strong><br>
      Jln Ters Maninjau Barat 2, B3 A6<br>
      Sawojajar, Malang<br>
      <abbr title="Phone">P:</abbr> (0341) 713857
    </address>
    <dl>
      <strong>Date :</strong> {{date_format($order->created_at, 'd F Y')}}
    </dl>
  </div>

  <div class="col-md-6 text-right">
    <address>
      <strong>Buyer Name</strong><br>
      {{$order->buyer->name}}
      <!--{!!Html::link('adminbbs/buyers/view/'.$order->buyer->id, $order->buyer->name)!!}-->
    </address>
    <address>
      <strong>Address</strong><br>
      {{$order->buyer->address}}<br>
      <abbr title="Phone">P:</abbr> {{$order->buyer->phone}}
    </address>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-body">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Code</th>
          <th>Seller Name</th>
          <th>Qty</th>
          <th class="total text-right">Amount ( Rp. )</th>
        </tr>
      </thead>

      <tbody>
        @if ($order->order_details)
          <?php $grand_total_weight = 0; ?>
          @foreach ($order->order_details as $detail)
            <?php $sub_tot = number_format ($detail->sub_total, 2, ',', '.'); ?>
            <?php
              $sellers[$detail->product->seller->id][]= array(
                'id' => $detail->product->seller->id,
                'company' => $detail->product->seller->company,
                'name' => $detail->product->seller->name,
                'address' => $detail->product->seller->address,
                'phone' => $detail->product->seller->phone,
                'prod_id' => $detail->product->id,
                'code' => $detail->product->code,
                'sub_weight' => $detail->sub_weight,
                'qty' => $detail->quantity,
                'status' => $detail->status,
                'sub_tot' => $detail->sub_total,
                'resi' => $detail->resi,
                'url' => $detail->url,
                'url_resize' => $detail->resize_small_url,
                'sub_tot_rp' => $sub_tot
              );
            ?>
            <tr>
              <!--
              <td>{!!Html::link('adminbbs/sellers/'.$detail->product->seller->id.'/products/'.$detail->product->id, $detail->product->code)!!}</td>
              <td>{!!Html::link('adminbbs/sellers/view/'.$detail->product->seller->id, $detail->product->seller->name)!!}</td>
              -->
              <td>{{$detail->product->code}}</td>
              <td>{{$detail->product->seller->name}}</td>
              <td>{{$detail->quantity}}</td>
              <td class="total text-right">{{$sub_tot}}</td>
            </tr>
          @endforeach
        @endif
        <?php $shipping_cost = number_format ($order->shipping_cost, 2, ',', '.'); ?>
        <tr class="total_bar">
          <td colspan="2" class="grand_total"></td>
          <td class="grand_total">Shipping Costs:</td>
          <td class="grand_total text-right">{{$shipping_cost}}</td>
        </tr>
        <?php $codetrans = substr($order->code, -4); ?>
        <?php $codetranss = number_format ($codetrans, 2, ',', '.'); ?>
        <tr class="total_bar">
          <td colspan="2" class="grand_total"></td>
          <td class="grand_total">Code Transaction:</td>
          <td class="grand_total text-right">{{$codetranss}}</td>
        </tr>
        <?php $finaltot=$order->total + $order->shipping_cost + $codetrans; ?>
        <?php $tot = number_format ($finaltot, 2, ',', '.'); ?>
        <tr class="total_bar">
          <td colspan="2" class="grand_total"></td>
          <td class="grand_total">Total:</td>
          <td class="grand_total text-right">{{$tot}}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
    @if ($order->status == "0")
      {!!Form::button('Invalid Purchase Invoice', array('class' => 'btn btn-danger', 'data-toggle' => 'modal', 'data-target' => '#modal-invalid-purchase'))!!}
    @endif
    <button class="btn btn-warning" onClick="PrintElem('#printOut')" type="button">Print Purchase Invoice</button>
  </div>
  @if ($order->status == "0")
    <div class="col-lg-6 text-right">
      {!!Form::button('Submit Purchase Invoice of Rp.'.$tot.'', array('class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modal-confirmation-purchase'))!!}
    </div>
  @endif
</div>
<?php $purchase_tot = $tot; ?>
</div>
<hr>

<div class="row top_body">
  <div class="col-lg-12">
    <ul role="tablist" class="nav nav-tabs" id="myTab">
      <li class="active"><a data-toggle="tab" role="tab" href="#home">#</a></li>
      @foreach ($sellers as $seller)
        <?php $weight[$seller[0]['id']] = 0; ?>
        <li><a data-toggle="tab" role="tab" href="#seller{{$seller[0]['id']}}">{{$seller[0]['name']}}</a></li>
        @foreach ($sellers[$seller[0]['id']] as $detail_seller)
          <?php $weight[$seller[0]['id']] = $weight[$seller[0]['id']] + ($detail_seller['sub_weight'] / 1000); ?>
        @endforeach
      @endforeach
      <?php $total_weight = 0; ?>
      @foreach ($weight as $d)
        <?php $ceil_weight = ceil($d); ?>
        <?php $grand_total_weight = $grand_total_weight + $ceil_weight ?>
      @endforeach
    </ul>

    <div class="tab-content" id="myTabContent">
      <div id="home" class="tab-pane tabs-up fade in active panel panel-default">
        <div class="panel-body">
          <p></p>
        </div>
      </div>
      <?php $parValidation = 0; ?>
      @foreach ($sellers as $seller)
        <?php $parValidation++; ?>
        <div id="seller{{$seller[0]['id']}}" class="tab-pane tabs-up fade panel panel-default">
          <div class="panel-body">
            <div class="page-header">
              <h1>
                Sales Invoice <small>#{{$order->code}}</small>
                @if ($seller[0]['status'] == "0")
                  {!!Form::button('Processed', array('class' => 'btn btn-purple btn-sm'))!!}
                @elseif ($seller[0]['status'] == "1")
                  {!!Form::button('Paid', array('class' => 'btn btn-success btn-sm'))!!}
                @elseif ($seller[0]['status'] == "2")
                  {!!Form::button('Ordered', array('class' => 'btn btn-warning btn-sm'))!!}
                @elseif ($seller[0]['status'] == "3")
                  {!!Form::button('-', array('class' => 'btn btn-default btn-sm'))!!}
                @elseif ($seller[0]['status'] == "4")
                  {!!Form::button('Invalid', array('class' => 'btn btn-danger btn-sm'))!!}
                @elseif ($seller[0]['status'] == "5")
                  {!!Form::button('Invalid Purchase', array('class' => 'btn btn-danger btn-sm'))!!}
                @endif
              </h1>
            </div>

            <div class="page-header text-right"><h3 class="no-margn">BBStore Comp</h3></div>
            <hr>

            <div class="row">
              <div class="col-md-6">
                <dl>
                  <dt>Sales Invoice Details</dt>
                  <dd>sales invoice detail / number : {{$seller[0]['id']." / ".$order->id}}</dd>
                </dl>
                <address>
                  <strong>BBStore, Inc.</strong><br>
                  Jln Ters Maninjau Barat 2, B3 A6<br>
                  Sawojajar, Malang<br>
                  <abbr title="Phone">P:</abbr> (0341) 713857
                </address>
                <dl>
                  <strong>Date :</strong> {{date_format($order->created_at, 'd F Y')}}
                </dl>
              </div>

              <div class="col-md-6 text-right">
                <address>
                  <strong>Company Name</strong><br>
                  {{$seller[0]['company']}}
                  <!--{!!Html::link('adminbbs/sellers/view/'.$seller[0]['id'], $seller[0]['company'])!!}-->
                </address>
                <address>
                  <strong>Seller Name</strong><br>
                  {{$seller[0]['name']}}
                  <!--{!!Html::link('adminbbs/sellers/view/'.$seller[0]['id'], $seller[0]['name'])!!}-->
                </address>
                <address>
                  <strong>Address</strong><br>
                  {{$seller[0]['address']}}<br>
                  <abbr title="Phone">P:</abbr> {{$seller[0]['phone']}}
                </address>
              </div>
            </div>

            <div class="panel panel-default">
              <div class="panel-body">
                <table class="table table-striped">
                   <thead>
                     <tr>
                       <th>Code</th>
                       <th>Qty</th>
                       <th class="total text-right">Amount ( Rp. )</th>
                     </tr>
                  </thead>

                  <tbody>
                    <?php $grandtotal = 0; ?>
                    <?php $total_weight = 0; ?>
                    @foreach ($sellers[$seller[0]['id']] as $detail_seller)
                      <?php $total_weight = $total_weight + $detail_seller['sub_weight']; ?>
                      <tr>
                        <td>{{$detail_seller['code']}}</td>
                        <!--<td>{!!Html::link('adminbbs/sellers/'.$detail_seller['id'].'/products/'.$detail_seller['prod_id'], $detail_seller['code'])!!}</td>-->
                        <td>{{$detail_seller['qty']}}</td>
                        <td class="total text-right">{{$detail_seller['sub_tot_rp']}}</td>
                      </tr>
                      <?php $grandtotal = $detail_seller['sub_tot'] + $grandtotal; ?>
                    @endforeach
                    <?php $ceil_total_weight = ceil($total_weight / 1000); ?>
                    <?php $shipping_cost = $ceil_total_weight / $grand_total_weight * $order->shipping_cost; ?>
                    <?php $shipping_costs = number_format ($shipping_cost, 2, ',', '.'); ?>
                    <tr class="total_bar">
                      <td class="grand_total"></td>
                      <td class="grand_total">Shipping Costs:</td>
                      <td class="grand_total text-right">{{$shipping_costs}}</td>
                    </tr>
                    <?php $finaltot = $grandtotal + $shipping_cost; ?>
                    <?php $tot = number_format ($finaltot, 2, ',', '.'); ?>
                    <tr class="total_bar">
                      <td class="grand_total"></td>
                      <td class="grand_total">Total:</td>
                      <td class="grand_total text-right">{{$tot}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-6">
                @if ($seller[0]['status'] == "0")
                  {!!Form::button('Invalid Sales Invoice', array('class' => 'btn btn-danger', 'data-toggle' => 'modal', 'data-target' => '#modal-invalid-sales'.$parValidation.''))!!}
                @endif
                <button class="btn btn-warning" onClick="PrintElem('#seller{{$seller[0]['id']}}')" type="button">Print Sales Invoice</button>
              </div>
              @if ($seller[0]['status'] == "0")
                <div class="col-lg-6 text-right">
                  {!!Form::button('Submit Sales Invoice of Rp.'.$tot.'', array('class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#modal-confirmation-sales'.$parValidation.''))!!}
                </div>
              @endif
            </div>
          </div>
        </div>

        <!-- Modal With Form Confirmation Sales -->
        <div class="modal fade" id="modal-confirmation-sales{{$parValidation}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  {!!Form::label('code_order_verification', 'Code Sales Invoice')!!}
                  <div class="input-group">
                    <span class="input-group-addon">#</span>
                    {!!Form::text('code_order_verification', $order->code, array('class' => 'form-control', 'disabled'))!!}
                  </div>
                </div>
                <div class="form-group">
                  {!!Form::label('resi', 'Resi')!!}
                  {!!Form::text('resi', $seller[0]['resi'], array('class' => 'form-control', 'disabled'))!!}
                </div>
                <div class="form-group">
                  {!!Form::label('image', 'Photo Verification')!!}
                  <span id="verification_resiText">{!!Html::link($seller[0]['url'], $seller[0]['url_resize'])!!}</span>
                </div>
                <p class="red">Are you sure to submit this sales invoice?</p>
              </div>
              <div class="modal-footer" id="modal-footer">
                <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                {!!Html::linkAction('Adm\OrderAdmController@actionDetailSuccess', 'Submit Sales Invoice of Rp.'.$tot.'', array($order->id, $seller[0]['id']), array('class' => 'btn btn-success'))!!}
              </div>
            </div>
          </div>
        </div>

        <!-- Modal With Form Invalid Sales -->
        <div class="modal fade" id="modal-invalid-sales{{$parValidation}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
              </div>
              <div class="modal-body">
                <p>Are you sure to change this sales invoice status to be Invalid?</p>
              </div>
              <div class="modal-footer" id="modal-footer">
                <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                {!!Html::linkAction('Adm\OrderAdmController@actionDetailFail', 'Invalid Sales Invoice', array($order->id, $seller[0]['id']), array('class' => 'btn btn-danger'))!!}
              </div>
            </div>
          </div>
        </div>

      @endforeach
    </div>
  </div>
</div>

<!-- Modal With Form Confirmation Purchase -->
<div class="modal fade" id="modal-confirmation-purchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to submit this purchase invoice?</p>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
        {!!Html::linkAction('Adm\OrderAdmController@actionSuccess', 'Submit Purchase Invoice of Rp.'.$purchase_tot.'', array($order->id), array('class' => 'btn btn-success'))!!}
      </div>
    </div>
  </div>
</div>

<!-- Modal With Form Invalid Purchase -->
<div class="modal fade" id="modal-invalid-purchase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to change this purchase invoice status to be Invalid?</p>
      </div>
      <div class="modal-footer" id="modal-footer">
        <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
        {!!Html::linkAction('Adm\OrderAdmController@actionFail', 'Invalid Purchase Invoice', array($order->id), array('class' => 'btn btn-danger'))!!}
      </div>
    </div>
  </div>
</div>

@stop
