@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-laptop"></i> Products List</h1></div>

  <div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt"></i> Products List</div>
    <div class="panel-body">

      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
        <thead>
          <tr>
            <th>Code</th>
            <th>Image</th>
            <th>Name</th>
            <th>Weight (gr)</th>
            <th>Category</th>
            <th>Seller</th>
            <th>Company</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if (count($products))
            @foreach ($products as $product)
              <tr>
                <td>{{$product->code}}</td>
                <td>
                  @foreach ($product->product_images as $image)
                    @if ($image->type == "main")
                      {!!Html::image($image->url, 'a picture', array('class' => 'thumb'))!!}
                    @endif
                  @endforeach
                </td>
                <td>
                  {{$product->name}}
                  {!!$product->deleted == '0'  ?
                    "" :
                    Form::button('inactive', array('class' => 'btn btn-purple btn-xs'))
                  !!}
                </td>
                <td>{{$product->weight}}</td>
                <td>{{$product->category->name}}</td>
                <td>{!! Html::link('adminbbs/sellers/view/'.$product->seller->id, $product->seller->name)!!}</td>
                <td>{{$product->seller->company}}</td>
                <td>
                  {!! Html::linkAction('Adm\SellerAdmController@actionProductView', '', array($product->seller->id, $product->id), array('class' => 'btn btn-success fa fa-eye tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'View'))!!}
                  <!--
                  {!!$product->deleted == '0'  ?
                    Html::linkAction('Adm\ProductAdmController@actionInactive', '', array($product->id), array('class' => 'btn btn-danger fa fa-trash tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Delete')) :
                    Html::linkAction('Adm\ProductAdmController@actionActive', '', array($product->id), array('class' => 'btn btn-warning fa fa-refresh tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Restore'))
                  !!}
                  -->
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
    </table>
  </div>
</div>

@stop
