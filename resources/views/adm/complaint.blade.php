@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-comment"></i> Complaints List</h1></div>

  <div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt"></i> Complaints List</div>
    <div class="panel-body">

      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
        <thead>
          <tr>
            <th>Date</th>
            <th>Code</th>
            <th>Name</th>
            <th>Comment</th>
          </tr>
        </thead>
        <tbody>
          @if (count($complaints))
            @foreach ($complaints as $complaint)
              <tr>
                <td>{{$complaint->created_at}}</td>
                @foreach ($orders as $order)
                  @if ($order->code == $complaint->code)
                    <td>#{!!Html::linkAction('Adm\OrderAdmController@actionView', $complaint->code, array($order->id), array('class' => 'tooltip-btn', 'data-placement' => 'right', 'data-original-title' => 'Order Details'))!!}</td>
                  @endif
                @endforeach
                <td>{!!Html::linkAction('Adm\BuyerAdmController@actionView', $complaint->buyer->name, array($complaint->buyer->id), array('class' => 'tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'Buyer Details'))!!}</td>
                <td>{{$complaint->comment}}</td>
              </tr>
            @endforeach
          @endif
        </tbody>
    </table>
  </div>
</div>

@stop
