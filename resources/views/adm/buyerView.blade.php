@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-user"></i> Buyer Details</h1></div>

<div class="row">

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Buyer Details</div>
      <div class="panel-body">

        <form role="form">
          <div class="form-group">
            {!!Form::label('name', 'Name')!!}
            {!!Form::text('name', "$buyer->name", array('class' => 'form-control', 'disabled'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('company', 'Company')!!}
            {!!Form::text('company', "$buyer->company", array('class' => 'form-control', 'disabled'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('address', 'Address')!!}
            {!!Form::textarea('address', "$buyer->address", ['class' => 'form-control', 'disabled', 'size' => '30x3'])!!}
          </div>
          <div class="form-group">
            {!!Form::label('phone', 'Phone')!!}
            {!!Form::text('phone', "$buyer->phone", array('class' => 'form-control', 'disabled'))!!}
          </div>
        </form>

      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Account Buyer</div>
      <div class="panel-body">
        {!!Form::open(array('action' => 'Adm\BuyerAdmController@actionUpdate', 'class' => 'validator-form'))!!}
          {!!Form::hidden('id', $buyer->id)!!}
          <div class="form-group">
            {!!Form::label('email', 'Email')!!}
            {!!Form::text('email', $buyer->user->email, array('class' => 'form-control', 'disabled'))!!}
          </div>
          <!--
          <div class="form-group">
            {!!Form::label('password', 'Password')!!}
            {!!Form::password('password', array('class' => 'form-control'))!!}
          </div>
          <div class="form-group">
            {!!Form::label('confirmPassword', 'Retype-Password')!!}
            {!!Form::password('confirmPassword', array('class' => 'form-control'))!!}
          </div>
          {!!Form::submit('Edit', array('class' => 'btn btn-purple'))!!}
        -->
        {!!Form::close()!!}
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><i class="fa fa-list-alt"></i> Wishlists</div>
      <div class="panel-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
          <thead>
            <tr>
              <th>Code</th>
              <th>Product</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            @if(count($buyer->wishlists))
              @foreach($buyer->wishlists as $data)
                <tr>
                  <td>
                    {!!Html::link('adminbbs/sellers/'.$data->product->seller->id.'/products/'.$data->product->id, $data->product->code)!!}
                    {!!$data->product->deleted=='0'  ?
                      "" :
                      Form::button('inactive', array('class' => 'btn btn-purple btn-xs'))
                    !!}
                  </td>
                  <td>{!!Html::link('adminbbs/sellers/'.$data->product->seller->id.'/products/'.$data->product->id, $data->product->name)!!}</td>
                  <td>{{$data->total}}</td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>

@stop
