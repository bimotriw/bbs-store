@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-shopping-cart"></i> Orders List</h1></div>

  <div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt"></i> Orders List</div>
    <div class="panel-body">

      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
        <thead>
          <tr>
            <th>Date</th>
            <th>CODE</th>
            <th>Buyer Name</th>
            <th>Status</th>
            <th>Updated at</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if (count($orders))
            @foreach ($orders as $order)
              <tr>
                <td>{{$order->created_at}}</td>
                <td>{{$order->code}}</td>
                <td>{!! Html::link('adminbbs/buyers/view/'.$order->buyer->id, $order->buyer->name)!!}</td>
                <td>
                  @if ($order->status == "0")
                    {!!Form::button('Responded', array('class' => 'btn btn-purple btn-sm'))!!}
                  @elseif ($order->status == "1")
                    {!!Form::button('Paid', array('class' => 'btn btn-success btn-sm'))!!}
                  @elseif ($order->status == "2")
                    {!!Form::button('Ordered', array('class' => 'btn btn-warning btn-sm'))!!}
                  @elseif ($order->status == "3")
                    {!!Form::button('Invalid', array('class' => 'btn btn-danger btn-sm'))!!}
                  @endif
                </td>
                <td>{{$order->updated_at}}</td>
                <td>
                  {!!Html::linkAction('Adm\OrderAdmController@actionView', '', array($order->id), array('class' => 'btn btn-success fa fa-eye'))!!}
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
    </table>
  </div>
</div>

@stop
