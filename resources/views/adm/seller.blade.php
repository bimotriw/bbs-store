@extends('adm/master')

@section('content')
<div class="page-header"><h1><i class="fa fa-user-md"></i> Sellers List</h1></div>

  <div class="panel panel-default">
    <div class="panel-heading"><i class="fa fa-list-alt"></i> Sellers List</div>
    <div class="panel-body">

      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="basic-datatable">
        <thead>
          <tr>
            <th>Company</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @if(count($sellers))
            @foreach($sellers as $seller)
              <tr>
                <td>{{$seller->company}}</td>
                <td>{{$seller->name}}</td>
                <td>{{$seller->user->email}}</td>
                <td>{{$seller->phone}}</td>
                <td>
                  {!!$seller->deleted=='0'  ?
                    Form::button('active', array('class' => 'btn btn-success btn-sm')) :
                    Form::button('blocked', array('class' => 'btn btn-purple btn-sm'))
                  !!}
                </td>
                <td>
                  {!! Html::linkAction('Adm\SellerAdmController@actionView', '', array($seller->id), array('class' => 'btn btn-success fa fa-eye tooltip-btn', 'data-placement' => 'left', 'data-original-title' => 'View')) !!}
                  {!!$seller->deleted=='0'  ?
                    Form::button('', array('class' => 'btn btn-danger fa fa-trash tooltip-btn btnModalSellerDeactive', 'data-placement' => 'left', 'data-original-title' => 'Block', 'data-toggle' => 'modal', 'data-target' => '#modal-seller-deactive', 'data-id' => $seller->id)) :
                    Form::button('', array('class' => 'btn btn-warning fa fa-refresh tooltip-btn btnModalSellerActive', 'data-placement' => 'left', 'data-original-title' => 'Unblock', 'data-toggle' => 'modal', 'data-target' => '#modal-seller-active', 'data-id' => $seller->id))
                  !!}
                </td>
              </tr>
            @endforeach
          @endif
        </tbody>
    </table>
  </div>
</div>

<!-- Modal With Form Deactive -->
<div class="modal fade" id="modal-seller-deactive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to block this seller?</p>
      </div>
      <div class="modal-footer" id="modal-footer-block"></div>
    </div>
  </div>
</div>

<!-- Modal With Form Active -->
<div class="modal fade" id="modal-seller-active" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title red" id="myModalLabel"><i class="fa fa-exclamation-triangle"></i> Attention!</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure to unblock this seller?</p>
      </div>
      <div class="modal-footer" id="modal-footer-unblock"></div>
    </div>
  </div>
</div>

@stop
