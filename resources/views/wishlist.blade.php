@extends('master')

@section('content')

<div class="page_content_offset" id="container_content">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
        <h2 class="tt_uppercase color_dark m_bottom_20">My Wishlist</h2>

        <table class="table_type_1 responsive_table full_width t_align_l r_corners wraper shadow bg_light_color_1 m_bottom_30">
          <thead>
            <tr class="f_size_large">
              <!--titles for td-->
              <th>Product Image</th>
              <th>Product Name &amp; Category</th>
              <th>Price</th>
              <th>Quanity</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach (Auth::user()->buyer->wishlists as $wishlist)
              @if ($wishlist->product->deleted != '1')
                {!!Form::open(array('action' => 'WishlistController@actionStore', 'class' => 'validator-form'))!!}
                  {!!Form::hidden('id', $wishlist->id, array('class' => 'form-control', 'id' => 'id_edit'))!!}
                  <tr>
                    <!--product image-->
                    @foreach ($wishlist->product->product_images as $image)
                      @if ($image->type == 'main')
                        <td data-title="Product Image">{!!Html::image($image->url, '...')!!}</td>
                      @endif
                    @endforeach
                    <!--product name and category-->
                    <td data-title="Product Name">
                      {!!Html::linkAction('ProductController@actionView', $wishlist->product->name, array($wishlist->product->id), array('class' => 'fw_medium d_inline_b f_size_ex_large color_dark m_bottom_5'))!!}
                      <br>
                      <a href="#" class="default_t_color">{{$wishlist->product->category->name}}</a>
                    </td>
                    <!--product price-->
                    <td data-title="Price">
                      <?php $cost = number_format ($wishlist->product->price, 0, ',', '.'); ?>
                      <span class="scheme_color fw_medium">Rp. {{$cost}},-</span>
                    </td>
                    <!--quanity-->
                    <td data-title="Quantity">
                      <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                        <button type="button" class="bg_tr d_block f_left" data-direction="down">-</button>
                        {!!Form::text('quantity', $wishlist->total, array('class' => 'f_left', 'readonly'))!!}
                        <button type="button" class="bg_tr d_block f_left" data-direction="up">+</button>
                      </div>
                    </td>
                    <!--add or remove buttons-->
                    <td data-title="Action">
                      <button type="submit" class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_10">Add to Cart</button>
                      <br>
                      <i class='fa fa-times m_right_5'></i> {!!Html::linkAction('WishlistController@actionDelete', 'Remove', array($wishlist->id), array('class' => 'color_dark'))!!}
                    </td>
                  </tr>
                {!!Form::close()!!}
              @endif
            @endforeach
          </tbody>
        </table>
      </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>

@stop
