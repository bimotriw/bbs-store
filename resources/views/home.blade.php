@extends('master')

@section('content')

@if (Session::get('success'))
  <div class="message_container d_none m_top_20" style="display: block;" id="notif">
    <div class="alert_box r_corners color_green success">
      <i class="fa fa-smile-o"></i>
      <p>{{Session::get('success')}}</p>
    </div>
  </div>
@endif

<section class="container">
  <div class="row clearfix">
    <!--slider-->
    <div class="col-lg-12 col-md-12 col-sm-9 m_xs_bottom_30">
      <div class="flexslider animate_ftr long">
        <ul class="slides">
          @if (count($contents))
            @foreach ($contents as $slide)
              @if ($slide->type == 'slider')
                <li>
                  {!!Html::image($slide->url, '')!!}
                  <section class="slide_caption t_align_c d_xs_none">
                    <div class="f_size_large color_light tt_uppercase slider_title_3 m_bottom_10">{{$slide->title}}</div>
                    <hr class="slider_divider d_inline_b m_bottom_10">
                    <div class="color_light slider_title_4 tt_uppercase t_align_c m_bottom_45 m_md_bottom_20">{!!$slide->content!!}</div>
                  </section>
                </li>
              @endif
            @endforeach
          @endif
        </ul>
      </div>
    </div>
    <!--
    <div class="col-lg-3 col-md-3 col-sm-3 t_xs_align_c s_banners">
      @if (count($promos1))
        @foreach ($promos1 as $slide)
          <a href="#" class="d_block d_xs_inline_b m_bottom_20 animate_ftr">
            {!!Html::image($slide->url, '')!!}
          </a>
        @endforeach
      @endif
    </div>
    banners-->
  </div>
</section>

<div class="page_content_offset">
	<div class="container">
		<!--
		<section class="row clearfix">
      @if (count($contents))
        @foreach ($contents as $slide)
          @if ($slide->type == 'banner')
      			<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_50 m_sm_bottom_35">
      				<a href="#" class="d_block banner animate_ftr wrapper r_corners relative m_xs_bottom_30">
      					{!!Html::image($slide->url, '')!!}
      					<span class="banner_caption d_block vc_child t_align_c w_sm_auto">
      						<span class="d_inline_middle">
      							<span class="d_block scheme_color banner_title type_2 m_bottom_5 m_mxs_bottom_5">{!!$slide->title!!}</span>
      							<span class="d_block divider_type_2 centered_db m_bottom_5"></span>
      							<span class="d_block color_light tt_uppercase m_bottom_25 m_xs_bottom_10 banner_title">{!!$slide->content!!}</span>
      							<span class="button_type_6 bg_scheme_color tt_uppercase r_corners color_light d_inline_b tr_all_hover box_s_none f_size_ex_large">Shop Now!</span>
      						</span>
      					</span>
      				</a>
      			</div>
          @endif
        @endforeach
      @endif
		</section>
    -->
		<div class="row clearfix">
			<aside class="col-lg-3 col-md-3 col-sm-3 m_xs_bottom_30">
				<!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
				<!--wishlist-->
				<figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Wishlist</h3>
					</figcaption>
          @if (Auth::check())
            <div class="widget_content">
              @foreach (Auth::user()->buyer->wishlists as  $wishlist)
    					  <div class="clearfix m_bottom_15 relative cw_product">
                  @foreach ($wishlist->product->product_images as $product_image)
                    @if ($product_image->type == 'main')
                      {!!HTML::image($product_image->url, '...', array('class' => 'wishlist_image_home f_left m_right_15 m_sm_bottom_10 f_sm_none f_xs_left m_xs_bottom_0'))!!}
                    @endif
                  @endforeach
    							<a href="{{url('product/view/'.$wishlist->product->id)}}">{{$wishlist->product->code}}</a> <br> <a href="{{url('product/view/'.$wishlist->product->id)}}" class="color_dark d_block bt_link">{{$wishlist->product->name}}</a>
    							<a href="{{url('product/delete/wishlist/'.$wishlist->id)}}" class="color_dark d_block bt_link"><button type="button" class="f_size_medium f_right color_dark bg_tr tr_all_hover close_fieldset"><i class="fa fa-times lh_inherit"></i></button></a>
    						</div>
  						  <hr class="m_bottom_15">
              @endforeach
  					</div>
          @else
  					<div class="widget_content">
  						You have to <a href="#" data-popup="#login_popup">Log In</a> to show your wishlist.
  					</div>
          @endif
				</figure>
				<!--
				<a href="#" class="widget animate_ftr d_block r_corners m_bottom_30">
          @if (count($promos2))
            @foreach ($promos2 as $slide)
              {!!Html::image($slide->url, '')!!}
            @endforeach
          @endif
				</a>
        banner-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Complaint Form</h3>
					</figcaption>
          @if (Auth::check())
            <div class="widget_content">
              {!!Form::open(array('action' => 'OrderController@actionComplaint', 'class' => 'validator-form'))!!}
                <ul>
                  <li class="m_bottom_15">
                    <label for="full_name" class="d_inline_b m_bottom_5 required">Order Number</label>
                    <input type="text" name="order_number" class="full_width r_corners">
                  </li>
                  <li class="m_bottom_15">
                    <label for="address" class="d_inline_b m_bottom_5 required">Comment</label>
                    <textarea id="cf_message" name="comment" class="full_width r_corners"></textarea>
                  </li>
                  <li>
                    <button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15">Complaint</button>
                  </li>
                </ul>
              {!!Form::close()!!}
  					</div>
          @else
  					<div class="widget_content">
  						You have to <a href="#" data-popup="#login_popup">Log In</a> to show your wishlist.
  					</div>
          @endif
				</figure>
			</aside>
			<section class="col-lg-9 col-md-9 col-sm-9">
				<h2 class="tt_uppercase color_dark m_bottom_10 heading5 animate_ftr">Featured products</h2>
				<!--products-->
				<section class="products_container a_type_2 category_grid clearfix m_bottom_25">
          @if (count($products))
            @foreach ($products as $product)
              @if ($product->deleted != '1' && $product->seller->deleted != '1')
    				    <!--product item-->
        				<div class="product_item rated w_xs_full">
        					<figure class="r_corners photoframe animate_ftb long type_2 t_align_c shadow relative">
        						<!--product preview-->
        						<a href="#" class="d_block relative pp_wrap m_bottom_15">
                      @foreach ($product_images as $image)
                        @if ($image->type == 'main' && $image->product_id == $product->id)
                          {!!Html::image($image->url, '', array('class' => 'tr_all_hover'))!!}
                        @endif
                      @endforeach
        							<span role="button" data-popup='#quick_view_product{{$product->id}}' class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
        						</a>
        						<!--description and price of product-->
        						<figcaption>
        							<h5 class="m_bottom_10"><a href="#" class="color_dark">{{$product->name}}</a></h5>
        							<div class="clearfix">
                        <?php $price = number_format ($product->price, 0, ',', '.'); ?>
        								<p class="scheme_color f_size_large m_bottom_15">Rp. {{$price}},-</p>
        							</div>
                      @if (Auth::check())
                        <a href="{{url('product/store/order/'.$product->id)}}"><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15">Add to Cart</button></a>
                        <a href="{{url('product/store/wishlist/'.$product->id)}}"><button class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0"><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span><i class="fa fa-heart-o f_size_big"></i></button></a>
											@else
                        <button data-popup="#login_popup" class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15">Add to Cart</button>
                        <button data-popup="#login_popup" class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0"><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span><i class="fa fa-heart-o f_size_big"></i></button>
											@endif

        						</figcaption>
        					</figure>
        				</div>

                <!--custom popup-->
                <div class="popup_wrap d_none" id='quick_view_product{{$product->id}}'>
            			<section class="popup r_corners shadow">
            				<button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
            				<div class="clearfix">
            					<div class="custom_scrollbar">
            						<!--left popup column-->
            						<div class="f_left half_column">
            							<div class="relative d_inline_b m_bottom_10 qv_preview">
                            @foreach ($product_images as $image)
                              @if ($image->type == 'main' && $image->product_id == $product->id)
                                {!!Html::image($image->url, '', array('class' => 'tr_all_hover'))!!}
                              @endif
                            @endforeach
            							</div>
            							<!--carousel-->
            							<div class="relative qv_carousel_wrap m_bottom_20">
            								<button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
            									<i class="fa fa-angle-left "></i>
            								</button>
            								<ul class="qv_carousel d_inline_middle">
                              @foreach ($product_images as $image)
                                @if ($image->product_id == $product->id)
                                  <li data-src={{$image->url}}>{!!Html::image($image->url, '')!!}</li>
                                @endif
                              @endforeach
            								</ul>
            								<button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
            									<i class="fa fa-angle-right "></i>
            								</button>
            							</div>
            						</div>
            						<!--right popup column-->
            						<div class="f_right half_column">
            							<!--description-->
            							<h2 class="m_bottom_10"><a href="#" class="color_dark fw_medium">{!!Html::linkAction('ProductController@actionView', $product->name, array($product->id))!!}</a></h2>
            							<hr class="m_bottom_10 divider_type_3">
            							<table class="description_table m_bottom_10">
            								<tr>
            									<td>Category:</td>
            									<td>
                                @foreach($categories as $category)
                                  @if($category->id==$product->category_id)
                                    {!!Html::linkAction('ProductController@categoryView', $category->name, array($category->id))!!}
                                  @endif
                                @endforeach
                              </td>
            								</tr>
            								<tr>
            									<td>Product Code:</td>
            									<td>{{$product->code}}</td>
            								</tr>
                            <tr>
            									<td>Product Weight:</td>
            									<td>{{$product->weight}} gram</td>
            								</tr>
            							</table>
            							<p class="m_bottom_10">{{$product->specification}}</p>
            							<hr class="divider_type_3 m_bottom_10">
            							<p class="m_bottom_10">{{$product->description}}</p>
            							<hr class="divider_type_3 m_bottom_15">
            							<div class="m_bottom_15">
                            <?php $cost = number_format ($product->price, 0, ',', '.'); ?>
            								<span class="v_align_b f_size_big m_left_5 scheme_color fw_medium">Rp.{{$cost}},-</span>
            							</div>
                          {!!Form::open(array('action' => 'WishlistController@actionStore', 'class' => 'validator-form', 'id' => 'formQty'.$product->id))!!}
              							<table class="description_table type_2 m_bottom_15">
              								<tr>
              									<td class="v_align_m">Quantity:</td>
              									<td class="v_align_m">
              										<div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
              											<button type="button" class="bg_tr d_block f_left" data-direction="down">-</button>
              											<input type="text" name="quantity" readonly value="1" class="f_left">
              											<button type="button" class="bg_tr d_block f_left" data-direction="up">+</button>
              										</div>
              									</td>
              								</tr>
              							</table>
              							<div class="clearfix m_bottom_15">
                              <script>
                                function submitForm(action, target){
                                    document.getElementById(target).action = action;
                                    document.getElementById(target).submit();
                                }
                              </script>
                              <input type="hidden" name="id_prod" value="{{$product->id}}">
                              @if (Auth::check())
              								  <button onclick="submitForm('product/view/store/order', 'formQty{{$product->id}}')" class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">Add to Cart</button>
              								  <button onclick="submitForm('product/view/store/wishlist' 'formQty{{$product->id}}')" class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-heart-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                              @else
                                <button type="button" data-popup="#login_popup" class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">Add to Cart</button>
                                <button type="button" data-popup="#login_popup" class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-heart-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                              @endif
                            </div>
                          {!!Form::close()!!}
            						</div>
            					</div>
            				</div>
            			</section>
            		</div>
              @endif
            @endforeach
          @endif

          @if(count($new_products))
            @foreach($new_products as $product)
              <!--custom popup-->
              <div class="popup_wrap d_none" id='quick_view_product-{{$product->id}}'>
          			<section class="popup r_corners shadow">
          				<button class="bg_tr color_dark tr_all_hover text_cs_hover close f_size_large"><i class="fa fa-times"></i></button>
          				<div class="clearfix">
          					<div class="custom_scrollbar">
          						<!--left popup column-->
          						<div class="f_left half_column">
          							<div class="relative d_inline_b m_bottom_10 qv_preview">
                          @foreach($product_images as $image)
                            @if($image->type=='main' && $image->product_id==$product->id)
                              {!!Html::image($image->url, '', array('class' => 'tr_all_hover'))!!}
                            @endif
                          @endforeach
          							</div>
          							<!--carousel-->
          							<div class="relative qv_carousel_wrap m_bottom_20">
          								<button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_prev">
          									<i class="fa fa-angle-left "></i>
          								</button>
          								<ul class="qv_carousel d_inline_middle">
                            @foreach($product_images as $image)
                              @if($image->product_id==$product->id)
                                <li data-src={{$image->url}}>{!!Html::image($image->url, '')!!}</li>
                              @endif
                            @endforeach
          								</ul>
          								<button class="button_type_11 t_align_c f_size_ex_large bg_cs_hover r_corners d_inline_middle bg_tr tr_all_hover qv_btn_next">
          									<i class="fa fa-angle-right "></i>
          								</button>
          							</div>
          						</div>
          						<!--right popup column-->
          						<div class="f_right half_column">
          							<!--description-->
                        <h2 class="m_bottom_10"><a href="#" class="color_dark fw_medium">{!!Html::linkAction('ProductController@actionView', $product->name, array($product->id))!!}</a></h2>
                        <hr class="m_bottom_10 divider_type_3">
                        <table class="description_table m_bottom_10">
                          <tr>
                            <td>Category:</td>
                            <td>
                              @foreach($categories as $category)
                                @if($category->id==$product->category_id)
                                  {!!Html::linkAction('ProductController@categoryView', $category->name, array($category->id))!!}
                                @endif
                              @endforeach
                            </td>
                          </tr>
                          <tr>
                            <td>Product Code:</td>
                            <td>{{$product->code}}</td>
                          </tr>
                          <tr>
                            <td>Product Weight:</td>
                            <td>{{$product->weight}} gram</td>
                          </tr>
                        </table>
          							<p class="m_bottom_10">{{$product->specification}}</p>
          							<hr class="divider_type_3 m_bottom_10">
          							<p class="m_bottom_10">{{$product->description}}</p>
          							<hr class="divider_type_3 m_bottom_15">
          							<div class="m_bottom_15">
                          <?php $cost = number_format ($product->price, 0, ',', '.'); ?>
                          <span class="v_align_b f_size_big m_left_5 scheme_color fw_medium">Rp.{{$cost}},-</span>
          							</div>
                        {!!Form::open(array('action' => 'WishlistController@actionStore', 'class' => 'validator-form', 'id' => 'formQtyNew'.$product->id))!!}
                          <table class="description_table type_2 m_bottom_15">
                            <tr>
                              <td class="v_align_m">Quantity:</td>
                              <td class="v_align_m">
                                <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark">
                                  <button type="button" class="bg_tr d_block f_left" data-direction="down">-</button>
                                  <input type="text" name="quantity" readonly value="1" class="f_left">
                                  <button type="button" class="bg_tr d_block f_left" data-direction="up">+</button>
                                </div>
                              </td>
                            </tr>
                          </table>
                          <div class="clearfix m_bottom_15">
                            <script>
                              function submitForm(action, target){
                                  document.getElementById(target).action = action;
                                  document.getElementById(target).submit();
                              }
                            </script>
                            <input type="hidden" name="id_prod" value="{{$product->id}}">
                            @if (Auth::check())
                              <button onclick="submitForm('product/view/store/order', 'formQtyNew{{$product->id}}')" class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">Add to Cart</button>
                              <button onclick="submitForm('product/view/store/wishlist' 'formQtyNew{{$product->id}}')" class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-heart-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                            @else
                              <button type="button" data-popup="#login_popup" class="button_type_12 r_corners bg_scheme_color color_light tr_delay_hover f_left f_size_large">Add to Cart</button>
                              <button type="button" data-popup="#login_popup" class="button_type_12 bg_light_color_2 tr_delay_hover f_left r_corners color_dark m_left_5 p_hr_0"><i class="fa fa-heart-o f_size_big"></i><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span></button>
                            @endif
                          </div>
                        {!!Form::close()!!}
          						</div>
          					</div>
          				</div>
          			</section>
          		</div>
            @endforeach
          @endif

				</section>
				<!--banners-->
				<div class="row clearfix m_bottom_45">
					<div class="col-lg-6 col-md-6 col-sm-6">
						<a class="d_block animate_ftb h_md_auto m_xs_bottom_15 banner_type_2 r_corners red t_align_c tt_uppercase vc_child n_sm_vc_child">
							<span class="d_inline_middle">
								<img class="d_inline_middle m_md_bottom_5" src="assets/images/banner_img_3.png" alt="">
								<span class="d_inline_middle m_left_10 t_align_l d_md_block t_md_align_c">
									<b>100% Satisfaction</b><br><span class="color_dark">Guaranteed</span>
								</span>
							</span>
						</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<a class="d_block animate_ftb h_md_auto m_xs_bottom_15 banner_type_2 r_corners green t_align_c tt_uppercase vc_child n_sm_vc_child">
							<span class="d_inline_middle">
								<img class="d_inline_middle m_md_bottom_5" src="assets/images/banner_img_4.png" alt="">
								<span class="d_inline_middle m_left_10 t_align_l d_md_block t_md_align_c">
									<b>Wide<br class="d_none d_sm_block"> Shipping</b><br><span class="color_dark">On All Items</span>
								</span>
							</span>
						</a>
					</div>
				</div>
				<div class="clearfix">
					<h2 class="color_dark tt_uppercase f_left m_bottom_15 f_mxs_none heading5 animate_ftr">New Collection</h2>
					<div class="f_right clearfix nav_buttons_wrap animate_fade f_mxs_none m_mxs_bottom_5">
						<button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left tr_delay_hover r_corners nc_prev"><i class="fa fa-angle-left"></i></button>
						<button class="button_type_7 bg_cs_hover box_s_none f_size_ex_large t_align_c bg_light_color_1 f_left m_left_5 tr_delay_hover r_corners nc_next"><i class="fa fa-angle-right"></i></button>
					</div>
				</div>
				<!--bestsellers carousel-->
				<div class="nc_carousel m_bottom_30 m_sm_bottom_20">
          @if(count($new_products))
            @foreach($new_products as $product)
              @if ($product->deleted != '1' && $product->seller->deleted != '1')
      					<figure class="r_corners photoframe animate_ftb long tr_all_hover type_2 t_align_c shadow relative">
      						<!--product preview-->
      						<a href="#" class="d_block relative pp_wrap m_bottom_15">
                    @foreach($product_images as $image)
                      @if($image->type=='main' && $image->product_id==$product->id)
                        {!!Html::image($image->url, '', array('class' => 'tr_all_hover'))!!}
                      @endif
                    @endforeach
      							<span role="button" data-popup="#quick_view_product-{{$product->id}}" class="button_type_5 box_s_none color_light r_corners tr_all_hover d_xs_none">Quick View</span>
      						</a>
      						<!--description and price of product-->
      						<figcaption class="p_vr_0">
      							<h5 class="m_bottom_10"><a href="#" class="color_dark">{{$product->name}}</a></h5>
      							<div class="clearfix">
                      <?php $cost = number_format ($product->price, 0, ',', '.'); ?>
      								<p class="scheme_color f_size_large m_bottom_15">Rp. {{$cost}},-</p>
      							</div>
                    @if (Auth::check())
                      <a href="{{url('product/store/order/'.$product->id)}}"><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15">Add to Cart</button></a>
                      <a href="{{url('product/store/wishlist/'.$product->id)}}"><button class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0"><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span><i class="fa fa-heart-o f_size_big"></i></button></a>
                    @else
                      <button data-popup="#login_popup" class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15">Add to Cart</button>
                      <button data-popup="#login_popup" class="button_type_12 bg_light_color_2 tr_delay_hover d_inline_b r_corners color_dark m_left_5 p_hr_0"><span class="tooltip tr_all_hover r_corners color_dark f_size_small">Wishlist</span><i class="fa fa-heart-o f_size_big"></i></button>
                    @endif
      						</figcaption>
      					</figure>
              @endif
            @endforeach
          @endif
				</div>
			</section>
		</div>
	</div>
</div>

@stop
