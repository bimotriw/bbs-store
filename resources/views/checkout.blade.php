@extends('master')

@section('content')

@if (count(Auth::user()->buyer->temp_order_details))

<?php
  //$destination = Auth::user()->buyer->city_id;
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "key: ee3a51ee0f3fec412c0d7745ce963b87"
  ),
));
  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);
  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
   $k = json_decode($response, true);
   //echo json_encode($k['rajaongkir']['results']);
  }
?>

@foreach (Auth::user()->buyer->temp_order_details as $order_detail)
  @if($order_detail->product->deleted != '1')
    @foreach ($order_detail->product->product_images as $image)
      @if($image->type == 'main')
        <?php $url = $image->url; ?>
      @endif
    @endforeach
    <?php
      $temp_order[$order_detail->product->seller->id][] = array(
        'id' => $order_detail->product->seller->id,
        'id_temp' => $order_detail->id,
        'seller_name' => $order_detail->product->seller->name,
        'prod_id' => $order_detail->product_id,
        'prod_code' => $order_detail->product->code,
        'prod_name' => $order_detail->product->name,
        'prod_price' => $order_detail->product->price,
        'buy_id' => Auth::user()->buyer->id,
        'quantity' => $order_detail->quantity,
        'sub_tot' => $order_detail->sub_total,
        'sub_weig' => $order_detail->sub_weight,
        'image_prod' => $url,
      );
    ?>
  @endif
@endforeach

<div class="page_content_offset">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
        <h2 class="tt_uppercase color_dark m_bottom_25">Cart</h2>
        <?php $payment = 0; ?>
        @foreach ($temp_order as $order)
          <hr class="m_bottom_20">
          <!--cart table-->
          <h4 class="tt_uppercase color_dark m_bottom_25">#Seller : {{$order[0]['seller_name']}}</h4>
          <table class="table_type_4 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c m_bottom_30">
            <thead>
              <tr class="f_size_large">
                <!--titles for td-->
                <th>Product Image &amp; Name</th>
                <th>Product Code</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
              <?php $sub_total = 0; ?>
              <?php $sub_weight[$order[0]['id']] = 0; ?>
              @foreach ($temp_order[$order[0]['id']] as $order_detail)
                <?php $sub_weight[$order[0]['id']] = $sub_weight[$order[0]['id']] + $order_detail['sub_weig']; ?>
                <tr>
                  <!--Product name and image-->
                  <td data-title="Product Image &amp; name" class="t_md_align_c">
                    {!!Html::image($order_detail['image_prod'], '...', array('class' => 'm_md_bottom_5 d_xs_block d_xs_centered other_product_image'))!!}
                    {!!Html::linkAction('ProductController@actionView', $order_detail['prod_name'], array($order_detail['prod_id']), array('class' => 'd_inline_b m_left_5 color_dark'))!!}
                  </td>
                  <!--product key-->
                  <td data-title="Product Code">{!!Html::linkAction('ProductController@actionView', $order_detail['prod_code'], array($order_detail['prod_id']), array('class' => ''))!!}</td>
                  <!--product price-->
                  <?php $prod_price = number_format ($order_detail['prod_price'], 0, ',', '.'); ?>
                  <td data-title="Price">Rp. {{$prod_price}},-</td>
                  <!--quanity-->
                  <td data-title="Quantity">
                    {!!Form::open(array('action' => 'CheckOutController@actionUpdateQuantity', 'class' => 'validator-form', 'id' => 'formUpdateQty'))!!}
                      <div class="clearfix quantity r_corners d_inline_middle f_size_medium color_dark m_bottom_10">
                        <button type="button" class="bg_tr d_block f_left" data-direction="down">-</button>
                        <input type="text" name="quantity" readonly value="{{$order_detail['quantity']}}" class="f_left">
                        <button type="button" class="bg_tr d_block f_left" data-direction="up">+</button>
                      </div>
                      <div>
                        <script>
                          function submitForm(action){
                              document.getElementById('formUpdateQty').action = action;
                              document.getElementById('formUpdateQty').submit();
                          }
                        </script>
                        <input type="hidden" name="id_temp" value="{{$order_detail['id_temp']}}">
                        <a href="#" onclick="submitForm('checkout/update')" class="color_dark"><i class="fa fa-check f_size_medium m_right_5"></i>Update</a><br>
                        <i class="fa fa-times f_size_medium m_right_5"></i>{!!Html::linkAction('CheckOutController@actionDelete', 'Remove', array($order_detail['id_temp']), array('class' => 'color_dark'))!!}
                      </div>
                    {!!Form::close()!!}
                  </td>
                  <!--subtotal-->
                  <td data-title="Subtotal">
                    <?php $sub_tot = number_format ($order_detail['sub_tot'], 0, ',', '.'); ?>
                    <?php $sub_total = $sub_total + $order_detail['sub_tot']; ?>
                    <?php $payment = $payment + $order_detail['sub_tot']; ?>
                    <p class="f_size_large fw_medium scheme_color">Rp. {{$sub_tot}},- </p>
                  </td>
                </tr>
              @endforeach
              <?php $sub_weight[$order[0]['id']] = ceil($sub_weight[$order[0]['id']] / 1000); ?>
              <!--prices-->
              <tr>
                <td colspan="4">
                  <p class="fw_medium f_size_large t_align_r t_xs_align_c">Subtotal:</p>
                </td>
                <td colspan="1">
                  <?php $sub_total = number_format ($sub_total, 0, ',', '.'); ?>
                  <p class="fw_medium f_size_large color_dark">Rp. {{$sub_total}},-</p>
                </td>
              </tr>
              <!--total-->
            </tbody>
          </table>
        @endforeach

        <h2 class="tt_uppercase color_dark m_bottom_25">Payment Of Bills To</h2>
        <!--order info tables-->
        <table class="table_type_6 responsive_table full_width r_corners shadow m_bottom_45 t_align_l">
          <tr>
            <td class="f_size_large d_xs_none">BCA</td>
            <td data-title="BCA">0115309167</td>
            <td>an. <span class="scheme_color">Bimo Tri Widodo</span></td>
          </tr>
          <tr>
            <td class="f_size_large d_xs_none">BRI</td>
            <td data-title="BCA">2066 - 01 - 001223 - 53 - 0</td>
            <td>an. <span class="scheme_color">Bimo Tri Widodo</span></td>
          </tr>
          <tr>
            <td class="f_size_large d_xs_none">Mandiri</td>
            <td data-title="BCA">900 - 00-2162135 - 5</td>
            <td>an. <span class="scheme_color">Witri Handayani</span></td>
          </tr>
        </table>

        <?php $final_weight = 0; ?>
        @foreach ($sub_weight as $weight)
          <?php $final_weight = $final_weight + $weight; ?>
        @endforeach
        <?php $final_weight = $final_weight * 1000; ?>
        <?php
          $destination = Auth::user()->buyer->city_id;
          $curl = curl_init();
          curl_setopt_array($curl, array(
             CURLOPT_URL => "http://rajaongkir.com/api/starter/cost",
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => "",
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 30,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "POST",
             CURLOPT_POSTFIELDS => "origin=256&destination=$destination&weight=$final_weight",
             CURLOPT_HTTPHEADER => array(
               "key: ee3a51ee0f3fec412c0d7745ce963b87"
             ),
          ));
          $response = curl_exec($curl);
          $err = curl_error($curl);
          curl_close($curl);
          if ($err) {
            echo "cURL Error #:" . $err;
          } else {
           $data = json_decode($response, true);

          }
        ?>

        <h2 class="color_dark tt_uppercase m_bottom_25">Shipment Information</h2>
        <table class="table_type_4 responsive_table full_width r_corners wraper shadow t_align_l t_xs_align_c m_bottom_30">
          <tr>
            <td colspan="2">
              <form>
								<ul>
									<li class="m_bottom_15">
										<label for="a_name_1" class="d_inline_b m_bottom_5">From</label>
										<input type="text" value="Malang" name="city_from" class="r_corners full_width" readonly>
									</li>
									<li class="m_bottom_15">
										<label for="c_name_2" class="d_inline_b m_bottom_5">Destination</label>
                    @foreach ($k['rajaongkir']['results'] as $wa)
                      @if ($wa['city_id'] == Auth::user()->buyer->city_id)
                        <input type="text" value="{{$wa['city_name']}}" name="city_from" class="r_corners full_width" readonly>
                      @endif
                    @endforeach
									</li>
								</ul>
							</form>
            </td>
          </tr>
          <?php $para = 0; ?>
          @if (count($orderReal))
            @foreach ($orderReal as $order)
              <?php $last_id = $order->id + 1; ?>
            @endforeach
          @else
            <?php $last_id = 1; ?>
          @endif
          @foreach ($data['rajaongkir']['results'] as $we)
            <tr>
              <td colspan="2">
                <h4 class="fw_medium m_bottom_15 m_sm_bottom_5">{{$we['name']}}</h4>
              </td>
            </tr>
            @foreach ($we['costs'] as $wa)
              <?php $para++; ?>
              <tr id="shipment{{$para}}">
                <td>
                  @foreach ($wa['cost'] as $wi)
                    <input type="radio" checked onclick="functionShipment('{{$wi['value']}}','{{$payment}}', '{{$last_id}}')" name="shippment">
                  @endforeach
                </td>
                <td>
                  <a><h5 class="m_bottom_15 m_sm_bottom_5 f_size_large fw_medium">{{$wa['service']}}</h5></a> ({{$wa['description']}})
                </td>
                <td>
                  @foreach ($wa['cost'] as $wi)
                    <?php $cost_shipment = number_format ($wi['value'], 0, ',', '.'); ?>
                    <p class="f_size_large fw_medium color_dark">Rp. {{$cost_shipment}},-</p>
                    <?php $cost_shipment_chosen = $wi['value']; ?>
                  @endforeach
                </td>
              </tr>
            @endforeach
          @endforeach
          <tr>
            <td colspan="3">
            </td>
          </tr>
          <tr>
            <td colspan="3">
            </td>
          </tr>
        </table>

        <script>
          function convertToRupiah(angka){
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return rupiah.split('',rupiah.length-1).reverse().join('');
          }
          function functionShipment(val, pay, last) {
            var value = val;
            var payment = pay;
            var last_id = last;
            var total = eval(value) + eval(payment) + eval(last_id);
            var value1 = convertToRupiah(value);
            var total1 = convertToRupiah(total);
            document.getElementById("shipment_final").innerHTML = value1;
            document.getElementById("total_final").innerHTML = total1;
            document.getElementById("shipment_input").value = value;
          }
          function functionConfirmation() {
            document.getElementById("confimation_purchase").removeAttribute("disabled");
          }
        </script>

        <br><br>
        <h2 class="color_dark tt_uppercase m_bottom_25">Payment Fee &amp; Shipment Fee</h2>
        <table class="table_type_5 full_width r_corners wraper shadow t_align_l">
          <tr>
            <td class="t_align_r">
              <p class="f_size_large fw_medium">Shipment Fee:</p>
            </td>
            <td>
              <p class="f_size_large fw_medium color_dark">Rp. <span id="shipment_final">{{$cost_shipment}}</span>,-</p>
            </td>
          </tr>
          <tr>
            <td class="t_align_r">
              <p class="f_size_large fw_medium">Payment Fee:</p>
            </td>
            <td>
              <?php $payment1 = number_format ($payment, 0, ',', '.'); ?>
              <p class="f_size_large fw_medium color_dark">Rp. {{$payment1}},-</p>
            </td>
          </tr>
          <tr>
            <td class="t_align_r">
              <p class="f_size_large fw_medium">Code Transaction:</p>
            </td>
            <td>
              <?php $last_id1 = number_format ($last_id, 0, ',', '.'); ?>
              <p class="f_size_large fw_medium color_dark">Rp. {{$last_id1}},-</p>
            </td>
          </tr>
          {!!Form::open(array('action' => 'CheckOutController@actionStore'))!!}
            <tr>
              <td class="t_align_r">
                <?php $total_input = $cost_shipment_chosen + $payment + $last_id1; ?>
                {!!Form::hidden('shipment_input', $cost_shipment_chosen, array('id' => 'shipment_input'))!!}
                {!!Form::hidden('total_input', $payment, array('id' => 'total_input'))!!}
                <p class="f_size_large fw_medium scheme_color">Total:</p>
              </td>
              <td>
                <?php $total_input1 = number_format ($total_input, 0, ',', '.'); ?>
                <p class="f_size_large fw_medium scheme_color">Rp. <span id="total_final">{{$total_input1}}</span>,-</p>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <h2 class="tt_uppercase color_dark m_bottom_30">Terms of service</h2>
                <div class="bs_inner_offsets bg_light_color_3 shadow r_corners m_bottom_45">
                  <p class="m_bottom_10"><span class="scheme_color">1.</span> Pemesanan barang akan di proses setelah anda (pembeli) mentransfer uang sebesar total pemesanan ke salah satu rekening yang tercantum.</p>
                  <p class="m_bottom_10"><span class="scheme_color">2.</span> Setelah anda (pembeli) mentransfer uang, anda (pembeli) wajib <span class="scheme_color">menverifikasi</spam> pembayaran pada halaman <span class="scheme_color">Order Detail</span>.</p>
                  <p><span class="scheme_color">3.</span> Setelah anda (pembeli) <span class="scheme_color">menverifikasi</span> pembayaran pada halaman <span class="scheme_color">Order Detail</span>, admin akan memproses pemesanan anda (pembeli).</p>
                  <p class="m_bottom_10"><span class="scheme_color">*Jika verifikasi anda (pembeli) tidak sesuai, maka pemesanan di anggap tidak valid.</span></p>
                  <p class="m_bottom_10"><span class="scheme_color">4.</span> Jika dalam 1 minggu anda (pembeli) belum menerima barang yang dipesan, anda (pembeli) bisa mengisi <span class="scheme_color">complaint form</span> yang selanjutnya akan di proses oleh admin untuk pengembalian uang sebesar total harga barang yang bermasalah.</p>
                </div>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <input type="checkbox" onclick="functionConfirmation()" name="checkbox_8" id="checkbox_8" class="d_none"><label for="checkbox_8">I agree to the Terms of Service (Terms of service)</label>
              </td>
            </tr>
            <tr>
              <td colspan="2">
                <button id="confimation_purchase" disabled class="button_type_6 bg_scheme_color f_size_large r_corners tr_all_hover color_light m_bottom_20">Confirm Purchase</button>
              </td>
            </tr>
          {!!Form::close()!!}
        </table>
      </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>

@else

<div class="page_content_offset" id="container_content">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9 m_xs_bottom_30">
        <h2 class="tt_uppercase color_dark m_bottom_25">Cart</h2>
        <p>No Products In The <span class="scheme_color">Cart</span>.</p>
      </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>

@endif



@stop
