@extends('master')

@section('content')

<div class="page_content_offset">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9">
        <h2 class="tt_uppercase color_dark m_bottom_25">{{$category->name}}</h2>

        <!--categories nav-->

        <!--sort-->

        <hr class="m_bottom_10 divider_type_3">

        <!--products list type-->
        <section class="products_container list_type clearfix m_bottom_5 m_left_0 m_right_0">
          @foreach ($category->products as $product)
            <!--product item-->
            <div class="product_item full_width list_type hit m_left_0 m_right_0">
              <figure class="r_corners photoframe tr_all_hover type_2 shadow relative clearfix">
                <!--product preview-->
                @foreach ($product->product_images as $product_image)
                  @if ($product_image->type == 'main')
                    <a href="#" class="d_block f_left relative pp_wrap m_right_30 m_xs_right_25">
                      {!!Html::image($product_image->url, '...', array('class' => 'tr_all_hover product_image_category'))!!}
                    </a>
                  @endif
                @endforeach
                <!--description and price of product-->
                <figcaption>
                  <div class="clearfix">
                    <div class="f_left p_list_description f_sm_none w_sm_full m_xs_bottom_10">
                      <h4 class="fw_medium"><a href="{{url('product/view/'.$product->id)}}">{{$product->name}}</a></h4>
                      <hr class="m_bottom_10">
                      <p class="d_sm_none d_xs_block">{{$string = substr($product->description,0,200).'...'}}</p>
                    </div>
                    <div class="f_right f_sm_none t_align_r t_sm_align_l">
                      <?php $cost = number_format ($product->price, 0, ',', '.'); ?>
                      <p class="scheme_color f_size_large m_bottom_15"><span class="fw_medium">Rp. {{$cost}},-</span></p>
                      @if (Auth::check())
                        <a href="{{url('product/store/order/'.$product->id)}}"><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15 m_sm_bottom_0 d_sm_inline_middle">Add to Cart</button><br class="d_sm_none"></a>
                        <a href="{{url('product/store/wishlist/'.$product->id)}}"><button class="button_type_4 bg_light_color_2 tr_all_hover f_right m_sm_left_5 r_corners color_dark mw_0 p_hr_0 d_sm_inline_middle f_sm_none"><i class="fa fa-heart-o"></i></button></a>
                      @else
                        <button data-popup="#login_popup" class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15 m_sm_bottom_0 d_sm_inline_middle">Add to Cart</button><br class="d_sm_none">
                        <button data-popup="#login_popup" class="button_type_4 bg_light_color_2 tr_all_hover f_right m_sm_left_5 r_corners color_dark mw_0 p_hr_0 d_sm_inline_middle f_sm_none"><i class="fa fa-heart-o"></i></button>
                      @endif
                    </div>
                  </div>
                </figcaption>
              </figure>
            </div>
          @endforeach
        </section>
        <hr class="m_bottom_10 divider_type_3">
      </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>

@stop
