@extends('master')

@section('content')

<div class="page_content_offset">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9">
        <h2 class="tt_uppercase color_dark m_bottom_25">Order Details</h2>
        <!--order info tables-->
        <table class="table_type_6 responsive_table full_width r_corners shadow m_bottom_45 t_align_l">
          <tr>
            <td class="f_size_large d_xs_none">Order Number</td>
            <td data-title="Order Number">{{$order->code}}</td>
          </tr>
          <tr>
            <td class="f_size_large d_xs_none">Order Date</td>
            <td data-title="Order Date">{{date_format($order->created_at, 'd F Y')}}</td>
          </tr>
          <tr>
            <td class="f_size_large d_xs_none">Order Status</td>
            <td data-title="Order Status">
              @if ($order->status == '2')
                <a>Unpaid</a>
              @elseif ($order->status == '0')
                Verification
              @elseif ($order->status == '1')
                Confirmed by admin
              @elseif ($order->status == '3')
                Invalid
              @endif
            </td>
          </tr>
          <tr>
            <td class="f_size_large d_xs_none">Last Updated</td>
            <td data-title="Last Update">{{date_format($order->updated_at, 'd F Y')}}</td>
          </tr>
          <tr>
            <?php $shipping_cost = number_format ($order->shipping_cost, 0, ',', '.'); ?>
            <td class="f_size_large d_xs_none">Shipment</td>
            <td data-title="Shipment">Rp. {{$shipping_cost}},-</td>
          </tr>
          <tr>
            <?php $total = number_format ($order->total, 0, ',', '.'); ?>
            <td class="f_size_large d_xs_none">Payment</td>
            <td data-title="Payment">Rp. {{$total}},-</td>
          </tr>
          <tr>
            <?php $id = number_format ($order->id, 0, ',', '.'); ?>
            <td class="f_size_large d_xs_none">Code Transaction</td>
            <td data-title="Payment">Rp. {{$id}},-</td>
          </tr>
          <tr>
            <?php $cost = $order->shipping_cost + $order->total + $order->id; ?>
            <?php $cost = number_format ($cost, 0, ',', '.'); ?>
            <td class="f_size_large d_xs_none">Total</td>
            <td data-title="Total"><p class="fw_medium f_size_large scheme_color">Rp. {{$cost}},-</p></td>
          </tr>
        </table>

        <h2 class="tt_uppercase color_dark m_bottom_25">Payment Of Bills To</h2>
        <!--order info tables-->
        <table class="table_type_6 responsive_table full_width r_corners shadow m_bottom_45 t_align_l">
          <tr>
            <td class="f_size_large d_xs_none">BCA</td>
            <td data-title="BCA">0115309167</td>
            <td>an. <span class="scheme_color">Bimo Tri Widodo</span></td>
          </tr>
          <tr>
            <td class="f_size_large d_xs_none">BRI</td>
            <td data-title="BCA">2066 - 01 - 001223 - 53 - 0</td>
            <td>an. <span class="scheme_color">Bimo Tri Widodo</span></td>
          </tr>
          <tr>
            <td class="f_size_large d_xs_none">Mandiri</td>
            <td data-title="BCA">900 - 00-2162135 - 5</td>
            <td>an. <span class="scheme_color">Witri Handayani</span></td>
          </tr>
        </table>

        <div class="tabs m_bottom_45">
          <!--tabs navigation-->
          <nav>
            <ul class="tabs_nav horizontal_list clearfix">
              <li class="f_xs_none"><a href="#tab-1" class="bg_light_color_1 color_dark tr_delay_hover r_corners d_block">Order Items</a></li>
            </ul>
          </nav>
          <section class="tabs_content shadow r_corners p_hr_0 p_vr_0 wrapper">
            <div id="tab-1">
              <table class="table_type_7 responsive_table full_width t_align_l">
                <thead>
                  <tr class="f_size_large">
                    <th>Product Code</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Qty</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($order->order_details as $order_detail)
                    <tr>
                      <td data-title="Code Product">
                        {!!Html::linkAction('ProductController@actionView', $order_detail->product->code, array($order_detail->product->id), array('class' => ''))!!}
                      </td>
                      <td data-title="Product Name">
                        {!!Html::linkAction('ProductController@actionView', $order_detail->product->name, array($order_detail->product->id), array('class' => 'color_dark d_inline_b m_bottom_5'))!!}
                      </td>
                      <td data-title="Price">
                        <?php $price = number_format ($order_detail->product->price, 0, ',', '.'); ?>
                        <p class="f_size_large color_dark">Rp. {{$price}},-</p>
                      </td>
                      <td data-title="Qty">{{$order_detail->quantity}}</td>
                      <?php $sub_total = number_format ($order_detail->sub_total, 0, ',', '.'); ?>
                      <td data-title="Total"><p class="color_dark f_size_large">Rp. {{$sub_total}},-</p></td>
                    </tr>
                  @endforeach
                  <tr>
                    <td colspan="4">
                      <p class="fw_medium f_size_large t_xs_align_c t_align_r">Shipment Fee:</p>
                    </td>
                    <td colspan="1" class="color_dark"><p class="fw_medium f_size_large">Rp. {{$shipping_cost}},-</p></td>
                  </tr>
                  <tr>
                    <td colspan="4">
                      <p class="fw_medium f_size_large t_align_r t_xs_align_c">Payment Fee:</p>
                    </td>
                    <td colspan="1" class="color_dark"><p class="fw_medium f_size_large">Rp. {{$total}},-</p></td>
                  </tr>
                  <tr>
                    <td colspan="4">
                      <p class="fw_medium f_size_large t_align_r t_xs_align_c">Code Transaction:</p>
                    </td>
                    <td colspan="1" class="color_dark"><p class="fw_medium f_size_large">Rp. {{$id}},-</p></td>
                  </tr>
                  <tr>
                    <td colspan="4">
                      <p class="fw_medium f_size_large t_align_r t_xs_align_c scheme_color">Total:</p>
                    </td>
                    <td colspan="1" class="color_dark"><p class="fw_medium f_size_large scheme_color">Rp. {{$cost}},-</p></td>
                  </tr>
                  @if ($order->status == '2')
                    <tr>
                      <td colspan="4"></td>
                      <td colspan="1" class="color_dark"><a href="{{url('order/verification/'.$order->id)}}"><button class="button_type_4 bg_scheme_color r_corners tr_all_hover color_light mw_0 m_bottom_15">Verication Purchase</button></a></td>
                    </tr>
                  @endif
                </tbody>
              </table>
            </div>
          </section>
        </div>
      </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>

@stop
