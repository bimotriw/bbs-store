@extends('master')

@section('content')

@if (Session::get('warning'))
  <div class="message_container d_none m_top_20" style="display: block;" id="notif">
    <div class="alert_box r_corners error">
      <i class="fa fa-exclamation-triangle"></i>
      <p>{{Session::get('warning')}}</p>
    </div>
  </div>
@endif
@if (Session::get('danger'))
  <div class="message_container d_none m_top_20" style="display: block;" id="notif">
    <div class="alert_box r_corners error">
      <i class="fa fa-exclamation-triangle"></i>
      <p>{{Session::get('danger')}}</p>
    </div>
  </div>
@endif

<?php
  //$destination = Auth::user()->buyer->city_id;
  $curl = curl_init();
  curl_setopt_array($curl, array(
  CURLOPT_URL => "http://api.rajaongkir.com/starter/city",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "key: ee3a51ee0f3fec412c0d7745ce963b87"
  ),
));
  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);
  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
   $k = json_decode($response, true);
   //echo json_encode($k['rajaongkir']['results']);
  }
?>

<div class="page_content_offset">
  <div class="container">
    <div class="row clearfix">
      <!--left content column-->
      <section class="col-lg-9 col-md-9 col-sm-9">
        <h2 class="tt_uppercase color_dark m_bottom_25">My Account</h2>
        <div class="row clearfix">
          <div class="col-lg-4 col-md-4 col-sm-4 m_xs_bottom_30">
            <h2 class="tt_uppercase color_dark m_bottom_25">Contact Info <a>BBStore</a></h2>
            <ul class="c_info_list">
              <li class="m_bottom_10">
                <div class="clearfix m_bottom_15">
                  <i class="fa fa-map-marker f_left color_dark"></i>
                  <p class="contact_e">Jln Ter D Maninjau Barat B3A6,<br> Sawojajar, Malang.</p>
                </div>
              </li>
              <li class="m_bottom_10">
                <div class="clearfix m_bottom_10">
                  <i class="fa fa-phone f_left color_dark"></i>
                  <p class="contact_e">(0341)713857</p>
                </div>
              </li>
              <li class="m_bottom_10">
                <div class="clearfix m_bottom_10">
                  <i class="fa fa-envelope f_left color_dark"></i>
                  <a class="contact_e default_t_color" href="mailto:#">info@bbstore.com</a>
                </div>
              </li>
            </ul>
          </div>
          <div class="col-lg-8 col-md-8 col-sm-8 m_xs_bottom_30">
            <h2 class="tt_uppercase color_dark m_bottom_25">My Account Form</h2>
            <p class="m_bottom_10">Create An Account. All fields with an <span class="scheme_color">*</span> are required.</p>
            {!!Form::open(array('action' => 'HomeController@editAccount', 'class' => 'validator-form'))!!}
              <ul>
                <li class="m_bottom_15">
                  <label for="full_name" class="d_inline_b m_bottom_5 required">Full Name</label>
                  <input type="text" name="full_name" value="{{Auth::user()->buyer->name}}" class="full_width r_corners">
                </li>
                <li class="m_bottom_15">
                  <label for="company" class="d_inline_b m_bottom_5">Company</label>
                  <input type="text" name="company" value="{{Auth::user()->buyer->company}}" class="full_width r_corners">
                </li>
                <li class="m_bottom_15">
                  <label for="address" class="d_inline_b m_bottom_5 required">Address</label>
                  <textarea id="cf_message" name="address" class="full_width r_corners">{{Auth::user()->buyer->address}}</textarea>
                </li>
                <li class="m_bottom_15">
                  <label for="city" class="d_inline_b m_bottom_5 required">City</label>
                  <div class="select_title">
										<select name="city">
                      @foreach ($k['rajaongkir']['results'] as $wa)
                        @if ($wa['city_id'] == Auth::user()->buyer->city_id)
                          <option selected value="{{$wa['city_id']}}">{{$wa['city_name']}}</option>
                        @else
                          <option value="{{$wa['city_id']}}">{{$wa['city_name']}}</option>
                        @endif
                      @endforeach
										</select>
									</div>
                </li>
                <li class="m_bottom_15">
                  <label for="phone" class="d_inline_b m_bottom_5 required">Phone</label>
                  <input type="text" name="phone" value="{{Auth::user()->buyer->phone}}" class="full_width r_corners">
                </li>
                <hr>
                <br>
                <li class="m_bottom_15">
                  <label for="email" class="d_inline_b m_bottom_5 required">Email</label>
                  <input type="text" name="email" disabled value="{{Auth::user()->email}}" class="full_width r_corners">
                </li>
                <li class="m_bottom_15">
                  <label for="password" class="d_inline_b m_bottom_5">New Password</label>
                  <input type="password" name="password" class="full_width r_corners">
                </li>
                <li>
                  <button class="button_type_4 bg_light_color_2 r_corners mw_0 tr_all_hover color_dark">Edit Account</button>
                </li>
              </ul>
            {!!Form::close()!!}
          </div>
        </div>
      </section>
      <!--right column-->
      <aside class="col-lg-3 col-md-3 col-sm-3">
        <!--widgets-->
        <figure class="widget animate_ftr shadow r_corners wrapper m_bottom_30">
					<figcaption>
						<h3 class="color_light">Categories</h3>
					</figcaption>
					<div class="widget_content">
						<!--Categories list-->
						<ul class="categories_list">
              @if (count($categories))
                @foreach ($categories as $category)
                  @if ($category->parent_id == 0)
                    <li>
                      <a href="{{url('category/view/'.$category->id)}}" class="f_size_large color_dark d_block relative">
                        <b>{{$category->name}}</b>
                        <span class="bg_light_color_1 r_corners f_right color_dark talign_c"></span>
                      </a>
                      <ul class="d_none">
                        @foreach ($categories as $category1)
                          @if ($category1->parent_id == $category->id)
          									<li>
          										<a href="{{url('category/view/'.$category->id)}}" class="d_block f_size_large color_dark relative">
          											{{$category1->name}}
          										</a>
          									</li>
                          @endif
                        @endforeach
      								</ul>
                    </li>
                  @endif
                @endforeach
              @endif
						</ul>
					</div>
				</figure>
      </aside>
    </div>
  </div>
</div>


@stop
